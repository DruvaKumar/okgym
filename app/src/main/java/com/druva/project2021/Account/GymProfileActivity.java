package com.druva.project2021.Account;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.druva.project2021.Activity.BatchActivity;
import com.druva.project2021.Adapters.AdapterBatch;
import com.druva.project2021.BottomNavigation;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.druva.project2021.pojo.Get_Gym_profileDAO;
import com.druva.project2021.pojo.GymprofileDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GymProfileActivity extends AppCompatActivity {

    TextView tvsave;
    ImageView back;
    CircleImageView imgGymLogo;
    TextInputEditText etgymname,etaddress,etcontact,etemail,etwebsite;
    String stgynmame,staddress,stcontact,stemail,stwebsite;
    LinearLayout lladdphoto;
    private static final int PICK_IMAGE = 1;
    PreferenceManager preferenceManager;
    ArrayList<List<GymprofileDAO>>gymprofile;
    Session session;
    File profilepicture;
    String profilepath;

    Uri picUri;
    Bitmap myBitmap;
    private static final int GALLERY = 1;
    private static Bitmap Image = null;
    private static Bitmap rotateImage = null;
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_create_gym_profile);

        preferenceManager=new PreferenceManager(getApplicationContext());
        session=new Session(this);

        tvsave=findViewById(R.id.tv_save);
        back=findViewById(R.id.btn_back);
        etgymname=findViewById(R.id.et_gymname);
        etaddress=findViewById(R.id.et_address);
        etcontact=findViewById(R.id.et_phone);
        etemail=findViewById(R.id.et_email);
        etwebsite=findViewById(R.id.et_website);
        lladdphoto=findViewById(R.id.ll_add_logo);
        imgGymLogo=findViewById(R.id.img_gym_logo);

        tvsave.setVisibility(View.VISIBLE);
        lladdphoto.setVisibility(View.GONE);


      /*  etgymname.setText(preferenceManager.getGymName("gymname"));
        etaddress.setText(preferenceManager.getGymAddress("gymaddress"));
        etcontact.setText(preferenceManager.getGymContact("gymcontact"));
        etemail.setText(preferenceManager.getGymEmail("gymemail"));
        etwebsite.setText(preferenceManager.getGymWebsite("gymwebsite"));*/
        
        get_gym_profile();

       // etgymname.setText(preferenceManager.getGymName("gymname"));



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tvsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendGymProfiletoServer();

            }
        });

        lladdphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {

                    ActivityCompat.requestPermissions(GymProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 200);

                }

                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent.createChooser(intent,"Select File"),GALLERY);

            }


        });


    }

    private void get_gym_profile() {

        PublicHelper.showProgressDialog(GymProfileActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));

        Call<Get_Gym_profileDAO> call = RetrofitInstance.getInstance().getMyApi().get_gym_profile(listDetails);
        call.enqueue(new Callback<Get_Gym_profileDAO>() {

            @Override
            public void onResponse(Call<Get_Gym_profileDAO> call, Response<Get_Gym_profileDAO> response) {
                Get_Gym_profileDAO get_gym_profileDAO=response.body();
                etgymname.setText(get_gym_profileDAO.getGym_name());
                etaddress.setText(get_gym_profileDAO.getAddress());
                etcontact.setText(get_gym_profileDAO.getContact());
                etemail.setText(get_gym_profileDAO.getEmail());
                etwebsite.setText(get_gym_profileDAO.getWebsite());

                Glide.with(GymProfileActivity.this).load(get_gym_profileDAO.getProfile_image())
                        .thumbnail(0.5f)
                        .into(imgGymLogo);
                PublicHelper.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<Get_Gym_profileDAO> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

            }
        });
    }

    private void SendGymProfiletoServer() {
        stgynmame=etgymname.getEditableText().toString().trim();
        staddress=etaddress.getEditableText().toString().trim();
        stcontact=etcontact.getEditableText().toString().trim();
        stemail=etemail.getEditableText().toString().trim();
        stwebsite=etwebsite.getEditableText().toString().trim();

        if(stgynmame.isEmpty()|| stgynmame.length()<2){
            etgymname.setError("Please enter valid name for your Gym");
        }
        else if(stcontact.length()<10){
            etcontact.setError("Please enter valid mobile number");
        }
        else if(!stemail.contains("@")){
            etemail.setError("Please enter valid Email id");
        }
        else if(PublicHelper.isConnectedToInternet(getApplicationContext())) {


         /*   File file1 = new File(String.valueOf(profilepicture));

            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
            MultipartBody.Part imageBody1 =
                    MultipartBody.Part.createFormData("profile", file1.getName(), requestFile1);*/

            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), stgynmame);
            RequestBody address = RequestBody.create(MediaType.parse("text/plain"), staddress);
            RequestBody contact = RequestBody.create(MediaType.parse("text/plain"), stcontact);
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), stemail);
            RequestBody website = RequestBody.create(MediaType.parse("text/plain"), stwebsite);
            RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getUserid(Config.USERID));


            Call<GymprofileDAO> call = RetrofitInstance.getInstance().getMyApi().update_gymprofile(name,address,contact,email,website,userid);
            call.enqueue(new Callback<GymprofileDAO>() {
                @Override
                public void onResponse(Call<GymprofileDAO> call, Response<GymprofileDAO> response) {
                    Log.v("TAG", response.toString());
                    GymprofileDAO gymprofileDAO = (GymprofileDAO) response.body();

                    String status = gymprofileDAO.getStatus();
                    if (status.equalsIgnoreCase("Success")) {
                        Toast.makeText(getApplicationContext(), "Profile Updated Successfully", Toast.LENGTH_SHORT).show();

                        preferenceManager.setGymName(gymprofileDAO.getGym_name());
                        preferenceManager.setGymContact(gymprofileDAO.getContact());
                        preferenceManager.setGymEmail(gymprofileDAO.getEmail());
                        preferenceManager.setGymWebsite(gymprofileDAO.getWebsite());
                        preferenceManager.setGymAddress(gymprofileDAO.getAddress());

                        session.setprofile(true);
                        session.setLoggedin(true);
                        startActivity(new Intent(getApplicationContext(), BottomNavigation.class));
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to update profile", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<GymprofileDAO> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
            });



/*

            HashMap<String, Object> listDetails = new HashMap<>();
            listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
            listDetails.put("gym_name", stgynmame);
            listDetails.put("address", staddress);
            listDetails.put("contact", stcontact);
            listDetails.put("email", stemail);
            listDetails.put("website", stwebsite);


            //  LocaleHelper.showProgressDialog(getActivity(), getString(R.string.please));
            Call<GymprofileDAO> call = RetrofitInstance.getInstance().getMyApi().addgymdetails(listDetails);
            call.enqueue(new Callback<GymprofileDAO>() {
                @Override
                public void onResponse(Call<GymprofileDAO> call, Response<GymprofileDAO> response) {
                    GymprofileDAO gymprofileDAO = (GymprofileDAO) response.body();

                    String status = gymprofileDAO.getStatus();


                    if (status.equalsIgnoreCase("Success")) {
                        Toast.makeText(getApplicationContext(), "Profile Updated Successfully", Toast.LENGTH_SHORT).show();

                        preferenceManager.setGymName(gymprofileDAO.getGym_name());
                        preferenceManager.setGymContact(gymprofileDAO.getContact());
                        preferenceManager.setGymEmail(gymprofileDAO.getEmail());
                        preferenceManager.setGymWebsite(gymprofileDAO.getWebsite());
                        preferenceManager.setGymAddress(gymprofileDAO.getAddress());

                        session.setprofile(true);
                        session.setLoggedin(true);
                        startActivity(new Intent(getApplicationContext(), BottomNavigation.class));
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to update profile", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<GymprofileDAO> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
            });

*/



        }
        else {
            Toast.makeText(getApplicationContext(), "Please Connect to Internet" , Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,requestCode,data);

        if(resultCode== Activity.RESULT_OK){
            Uri seletecdImageUri = data.getData();

            profilepath=getPath(seletecdImageUri);
            Log.v("TAG","path "+ profilepath);
            profilepicture=new File(profilepath);
            imgGymLogo.setImageURI(seletecdImageUri);
        }
    }
    public String getPath(Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(GymProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(GymProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(GymProfileActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(GymProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity =
                (ConnectivityManager) context.getSystemService(
                        Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }


}