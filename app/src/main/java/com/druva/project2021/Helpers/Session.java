package com.druva.project2021.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Druva on 18-Apr-17.
 */
public class Session {
    SharedPreferences prefe;
    SharedPreferences.Editor editor;
    Context ctx;

    public Session(Context ctx){
        this.ctx=ctx;
        prefe=ctx.getSharedPreferences("MYDB", Context.MODE_PRIVATE);
        editor= prefe.edit();
    }

    public void setLoggedin(boolean logggedin){
        editor.putBoolean("LoggedInmode",logggedin);
        editor.commit();
    }

    public boolean logggedin()
    {

        return prefe.getBoolean("LoggedInmode",false);
    }

    //
    public void setprofile(boolean profile){
        editor.putBoolean("profileupdated",profile);
        editor.commit();
    }

    public boolean isprofile()
    {

        return prefe.getBoolean("profileupdated",false);
    }

    //plan
    //
    public void setplanadded(boolean planadded){
        editor.putBoolean("planadded",planadded);
        editor.commit();
    }

    public boolean isplanadded()
    {

        return prefe.getBoolean("planadded",false);
    }

    //Batch
    public void setbatchadded(boolean batchadded){
        editor.putBoolean("batchadded",batchadded);
        editor.commit();
    }

    public boolean isbatchadded()
    {

        return prefe.getBoolean("batchadded",false);
    }
}
