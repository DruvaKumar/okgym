package com.druva.project2021.Helpers;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spannable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;


import com.druva.project2021.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by abdalla on 10/2/17.
 */

public class PublicHelper {

    private static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";
    private static Dialog progressDialog;
    private static final long DURATION_IN_FADE_ID = 250;
    private static final long DURATION_IN_RIGHT_LEFT = 150;

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivity =
                (ConnectivityManager) context.getSystemService(
                        Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {

                        return true;

                    }
        }
        return false;
    }

    public static void showProgressDialog(Context mContext, String titleMsg) {

        progressDialog = new Dialog(mContext, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.progress_dialog_layout);
        progressDialog.setCancelable(true);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }

//        }
    }
    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

/*

    public static void showProgressDialog(Context mContext, String titleMsg) {
//        if (progressDialog == null || !progressDialog.isShowing()) {
        progressDialog = new Dialog(mContext, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.layout_custom_progress_dialog);
        progressDialog.setCancelable(true);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        ImageView ivProgressBar=progressDialog.findViewById(R.id.iv_progress_bar);
//        TextView msg = (TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
//        msg.setText(titleMsg);
//        msg.setVisibility(View.GONE);
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }

//        }
    }

    public static void showSadEmoji(Context mContext) {
        progressDialog = new Dialog(mContext, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.layout_custom_progress_dialog_sad_emoji);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivEmojiSuccess = progressDialog.findViewById(R.id.iv_bg_success);
        ImageView ivEmojiWrong = progressDialog.findViewById(R.id.iv_bg_wrong);
        ivEmojiWrong.setVisibility(View.VISIBLE);
        ivEmojiSuccess.setVisibility(View.GONE);
        */
/* ImageView ivEmoji = progressDialog.findViewById(R.id.iv_emoji);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .error(R.drawable.ic_no_record_found)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .priority(Priority.HIGH);

        Glide.with(mContext).applyDefaultRequestOptions(options)
                .load(R.drawable.wrong_ans)
                .into(ivEmoji);*//*

        progressDialog.show();
    }

    public static void showHappyEmoji(Context mContext) {
        progressDialog = new Dialog(mContext, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.layout_custom_progress_dialog_sad_emoji);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivEmojiSuccess = progressDialog.findViewById(R.id.iv_bg_success);
        ImageView ivEmojiWrong = progressDialog.findViewById(R.id.iv_bg_wrong);
        ivEmojiWrong.setVisibility(View.GONE);
        ivEmojiSuccess.setVisibility(View.VISIBLE);
        */
/*RequestOptions options = new RequestOptions()
                .centerCrop()
                .error(R.drawable.ic_no_record_found)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .priority(Priority.HIGH);

        Glide.with(mContext).applyDefaultRequestOptions(options)
                .load(R.drawable.correct_answer)
                .into(ivEmoji);*//*

        progressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public static boolean checkForSubscription(Context mContext, String selectedSubjectId) {
        com.visl.helper.PreferenceManager manager = new com.visl.helper.PreferenceManager(mContext);
        String currentClassId = manager.getClassName("selected");
        String jsonSubjectIdArray = manager.getSubscriptionJsonArry("JsonArray");
        String subscribedClassStatus = manager.getFullClass("full_class");
        String DEMOUSER = manager.getDemoUser("demouser");
        String DEM_expires = manager.getDemoExpityDate("demoexpirydate");
        boolean selectedSubjectSubscribed = false;


        if(DEMOUSER.matches("1")){
           return  true;
        }

        else if (subscribedClassStatus != null && !subscribedClassStatus.equalsIgnoreCase("")
                && subscribedClassStatus.equalsIgnoreCase("YES")) {
            Log.d("LocalHelper", "Class Subscribed" + currentClassId);
            return true;
        } else if (jsonSubjectIdArray != null && !jsonSubjectIdArray.equalsIgnoreCase("")) {
            try {
                JSONArray jsonArray = new JSONArray(jsonSubjectIdArray);
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String subjectId = jsonObject.getString("subject_id");
                        if (selectedSubjectId.equalsIgnoreCase(subjectId)) {
                            selectedSubjectSubscribed = true;
                        }
                    }

                    if (!selectedSubjectSubscribed) {
                        AppCompatActivity appCompatActivity = (AppCompatActivity) mContext;
                        FragmentManager fragmentManager = appCompatActivity.getSupportFragmentManager();
                        SubscribeNowDialogBottomSheet addPhotoBottomDialogFragment =
                                SubscribeNowDialogBottomSheet.newInstance();

                        addPhotoBottomDialogFragment.show(fragmentManager,
                                SubscribeNowDialogBottomSheet.TAG);
                    } else {
                        return true;
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            AppCompatActivity appCompatActivity = (AppCompatActivity) mContext;
            FragmentManager fragmentManager = appCompatActivity.getSupportFragmentManager();
            SubscribeNowDialogBottomSheet addPhotoBottomDialogFragment =
                    SubscribeNowDialogBottomSheet.newInstance();

            addPhotoBottomDialogFragment.show(fragmentManager,
                    SubscribeNowDialogBottomSheet.TAG);
        }
        return false;
    }

    public static boolean isAlreadySubscribed(Context mContext, String selectedSubjectId) {
        com.visl.helper.PreferenceManager manager = new com.visl.helper.PreferenceManager(mContext);
        String currentClassId = manager.getClassName("selected");
        String jsonSubjectIdArray = manager.getSubscriptionJsonArry("JsonArray");
        String subscribedClassStatus = manager.getFullClass("full_class");

        String Cansubscribenow = manager.getAdvancePayment("advancePayment");
        boolean selectedSubjectSubscribed = false;

        if(Cansubscribenow.matches("1")){
            return  false;
        }

        else if (subscribedClassStatus != null && !subscribedClassStatus.equalsIgnoreCase("")
                && subscribedClassStatus.equalsIgnoreCase("YES")) {
            Log.d("LocalHelper", "Class Subscribed" + currentClassId);
            return true;
        } else if (jsonSubjectIdArray != null && !jsonSubjectIdArray.equalsIgnoreCase("")) {
            try {
                JSONArray jsonArray = new JSONArray(jsonSubjectIdArray);
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String subjectId = jsonObject.getString("subject_id");
                        if (selectedSubjectId.equalsIgnoreCase(subjectId)) {
                            selectedSubjectSubscribed = true;
                        }
                    }

                    if (!selectedSubjectSubscribed) {
                    } else {
                        return true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
        return false;
    }

    public static void hideAnimation(View v, int duration) {
        v.animate().alpha(0f).setDuration(duration);
    }

    public static void showAnimation(View v, int duration) {
        v.animate().alpha(1f).setDuration(duration);
    }

    public static Context onAttach(Context context) {
        String lang = getPersistedData(context, Locale.getDefault().getLanguage());
        return setLocale(context, lang);
    }

    public static Context onAttach(Context context, String defaultLanguage) {
        String lang = getPersistedData(context, defaultLanguage);
        return setLocale(context, lang);
    }

    public static String getLanguage(Context context) {
        return getPersistedData(context, Locale.getDefault().getLanguage());
    }

    public static Context setLocale(Context context, String language) {
        persist(context, language);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResources(context, language);
        }

        return updateResourcesLegacy(context, language);
    }

    private static String getPersistedData(Context context, String defaultLanguage) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    private static void persist(Context context, String language) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SELECTED_LANGUAGE, language);
        editor.apply();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);

        return context.createConfigurationContext(configuration);
    }

    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale);
        }

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }
*/


/*

    public static void showToastMessage(Context mContext, String toastMessage) {
        if (mContext != null) {
            Toast.makeText(mContext, toastMessage, Toast.LENGTH_LONG).show();
//            View parent_view = ((AppCompatActivity) mContext).findViewById(android.R.id.content);
//            Snackbar snackbar = Snackbar.make(parent_view, toastMessage, Snackbar.LENGTH_SHORT);
//            snackbar.getView().setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
//            snackbar.show();
        }
    }

    public static String convertDate(String date) throws ParseException {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Date dateConverted = inputFormat.parse(date);
        String outputDateStr = outputFormat.format(dateConverted);
        return outputDateStr;
    }


    public static String convertDateAndTime(String date) throws ParseException {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy hh:mm", Locale.US);
        Date dateConverted = inputFormat.parse(date);
        String outputDateStr = outputFormat.format(dateConverted);
        return outputDateStr;
    }


    public static boolean isEmailAddressValid(String email) {
        boolean isEmailValid = false;
        String strExpression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        //  String strExpression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern objPattern = Pattern.compile(strExpression, Pattern.CASE_INSENSITIVE);
        Matcher objMatcher = objPattern.matcher(inputStr);
        if (objMatcher.matches()) {
            isEmailValid = true;
        }
        return isEmailValid;
    }


    public static void startAnimationBlink(View textView) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textView.startAnimation(anim);
    }

    public static Animation getAnimationSet(AppCompatActivity mContext) {
        AnimationSet set = new AnimationSet(true);

        // Fade in animation
        Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(400);
        fadeIn.setFillAfter(true);
        set.addAnimation(fadeIn);

        // Slide up animation from bottom of screen
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int height = 100;
        if (mContext != null) {
            mContext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
        }
        Animation slideUp = new TranslateAnimation(0, 0, height, 0);
        slideUp.setInterpolator(new DecelerateInterpolator(4.f));
        slideUp.setDuration(400);
        set.addAnimation(slideUp);


        return set;
    }


    public static String getDateDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        long totalSeconds = 0;
        StringBuilder builder = new StringBuilder();

        if (elapsedHours > 0) {
//            builder.append(elapsedHours);
            totalSeconds = elapsedHours * 60 * 60;
        }

        if (elapsedMinutes > 0) {
//            builder.append(elapsedMinutes).append(":");
            totalSeconds = totalSeconds + (elapsedMinutes * 60);
        }

        if (elapsedSeconds > 0) {
            totalSeconds = totalSeconds + elapsedSeconds;
        }

        builder.append(totalSeconds);

        return builder.toString();
    }

    public static Spannable getSpannableTxt(TextView textView, String value, Context mContext) {

        PicassoImageGetter imageGetterOptionA = new PicassoImageGetter(textView, mContext);

        Spannable htmlReturnValue;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            htmlReturnValue = (Spannable) Html.fromHtml(value, Html.FROM_HTML_MODE_LEGACY, imageGetterOptionA, null);
        } else {
            htmlReturnValue = (Spannable) Html.fromHtml(value, imageGetterOptionA, null);
        }
        return htmlReturnValue;
    }

    public static void animateFadeIn(View view, int position) {
        boolean not_first_item = position == -1;
        position = position + 1;
        view.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(view, "alpha", 0.f, 0.5f, 1.f);
        ObjectAnimator.ofFloat(view, "alpha", 0.f).start();
        animatorAlpha.setStartDelay(not_first_item ? DURATION_IN_FADE_ID / 2 : (position * DURATION_IN_FADE_ID / 3));
        animatorAlpha.setDuration(DURATION_IN_FADE_ID);
        animatorSet.play(animatorAlpha);
        animatorSet.start();
    }

    private static void animateRightLeft(View view, int position) {
        boolean not_first_item = position == -1;
        position = position + 1;
        view.setTranslationX(view.getX() + 400);
        view.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(view, "translationX", view.getX() + 400, 0);
        ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(view, "alpha", 1.f);
        ObjectAnimator.ofFloat(view, "alpha", 0.f).start();
        animatorTranslateY.setStartDelay(not_first_item ? DURATION_IN_RIGHT_LEFT : (position * DURATION_IN_RIGHT_LEFT));
        animatorTranslateY.setDuration((not_first_item ? 2 : 1) * DURATION_IN_RIGHT_LEFT);
        animatorSet.playTogether(animatorTranslateY, animatorAlpha);
        animatorSet.start();
    }
*/

}