package com.druva.project2021.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {
    SharedPreferences prefrencemanager;
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "Gym";
    // All Shared Preferences Keys
    private static final String USERID = "userid";
    private static final String MOBILE = "mobile";
    private static final String GYM_NAME = "gym_name";
    private static final String GYM_PROFILE = "gym_profile";
    private static final String GYM_CONTACT = "gym_contact";
    private static final String GYM_WEBSITE = "gym_website";
    private static final String GYM_EMAIL = "gym_email";
    private static final String GYM_ADDRESS = "gym_address";
    private static final String GYM_MEMBER_COUNT = "gym_count";

    private static final String MEMBER_ID = "memberid";
    private static final String TRIAL_EXPIRY = "trialexpiry";

    private static final String IS_PLAN_ADDED = "plan";
    private static final String IS_BATCH_ADDED = "batch";

    private static final String NEWJOIN = "newjoin";
    private static final String DISCOUNT = "discount";
    private static final String SUBSCRIPTION = "subscription";
    private static final String EXPIRYDATE = "expirydate";
    private static final String NEW_FEATURES = "features";
    private static final String VERSION_NAME = "version_name";

    public PreferenceManager(Context context) {
        this._context = context;
        prefrencemanager = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = prefrencemanager.edit();
    }

    //userid
    public String getUserid(String userid) {
        return prefrencemanager.getString(USERID, null);
    }

    public void setUserid(String userid) {
        editor.putString(USERID, userid);
        editor.commit();
    }

    //profile
    public String getGymProfile(String profile) {
        return prefrencemanager.getString(GYM_PROFILE, null);
    }

    public void setGymProfile(String profile) {
        editor.putString(GYM_PROFILE, profile);
        editor.commit();
    }
    //mobile
    public String getMobile(String mobile) {
        return prefrencemanager.getString(MOBILE, null);
    }

    public void setMobile(String mobile) {
        editor.putString(MOBILE, mobile);
        editor.commit();
    }
    // gym name
    public String getGymName(String gymname) {
        return prefrencemanager.getString(GYM_NAME, null);
    }

    public void setGymName(String gymname) {
        editor.putString(GYM_NAME, gymname);
        editor.commit();
    }

    // gym contact
    public String getGymContact(String gymcontact) {
        return prefrencemanager.getString(GYM_CONTACT, null);
    }

    public void setGymContact(String gymcontact) {
        editor.putString(GYM_CONTACT, gymcontact);
        editor.commit();
    }

    // gym website
    public String getGymWebsite(String gymwebsite) {
        return prefrencemanager.getString(GYM_WEBSITE, null);
    }

    public void setGymWebsite(String gymwebsite) {
        editor.putString(GYM_WEBSITE, gymwebsite);
        editor.commit();
    }

    // gym email
    public String getGymEmail(String gymemail) {
        return prefrencemanager.getString(GYM_EMAIL, null);
    }

    public void setGymEmail(String gymemail) {
        editor.putString(GYM_EMAIL, gymemail);
        editor.commit();
    }

    // gym address
    public String getGymAddress(String gymaddress) {
        return prefrencemanager.getString(GYM_ADDRESS, null);
    }

    public void setGymAddress(String gymaddress) {
        editor.putString(GYM_ADDRESS, gymaddress);
        editor.commit();
    }

    // member id
    public String getMemberId(String memberid) {
        return prefrencemanager.getString(MEMBER_ID, null);
    }

    public void setMemberId(String memberid) {
        editor.putString(MEMBER_ID, memberid);
        editor.commit();
    }


    // member ID
    public int getGymCount(String count) {
        return prefrencemanager.getInt(GYM_MEMBER_COUNT, 0);
    }

    public void setGymCount(int count) {
        editor.putInt(GYM_MEMBER_COUNT, count);
        editor.commit();
    }

    // trialexpiry
    public String getTrialExpiry(String trialexpiry) {
        return prefrencemanager.getString(TRIAL_EXPIRY, null);
    }

    public void setTrialExpiry(String trialexpiry) {
        editor.putString(TRIAL_EXPIRY, trialexpiry);
        editor.commit();
    }

    // new join
    public String getNewjoin(String newjoin) {
        return prefrencemanager.getString(NEWJOIN, null);
    }

    public void setNewjoin(String newjoin) {
        editor.putString(NEWJOIN, newjoin);
        editor.commit();
    }

    // discount
    public String getDiscount(String discount) {
        return prefrencemanager.getString(DISCOUNT, null);
    }

    public void setDiscount(String discount) {
        editor.putString(DISCOUNT, discount);
        editor.commit();
    }

    // SUBSCRIP
    public String getSubscription(String subscription) {
        return prefrencemanager.getString(SUBSCRIPTION, null);
    }

    public void setSubscription(String subscription) {
        editor.putString(SUBSCRIPTION, subscription);
        editor.commit();
    }

    // EXpiry date
    public String getExpirydate(String expirydate) {
        return prefrencemanager.getString(EXPIRYDATE, null);
    }

    public void setExpirydate(String expirydate) {
        editor.putString(EXPIRYDATE, expirydate);
        editor.commit();
    }

    // new Features
    public String getNewFeatures(String features) {
        return prefrencemanager.getString(NEW_FEATURES, null);
    }

    public void setNewFeatures(String features) {
        editor.putString(NEW_FEATURES, features);
        editor.commit();
    }

    // version name
    public String getVersionName(String version_name) {
        return prefrencemanager.getString(VERSION_NAME, null);
    }

    public void setVersionName(String version_name) {
        editor.putString(VERSION_NAME, version_name);
        editor.commit();
    }

}

