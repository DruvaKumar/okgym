package com.druva.project2021.PaymentGateway;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Activity.MembersPlan;
import com.druva.project2021.Activity.SubsvriptionwithFAQActivity;
import com.druva.project2021.BottomNavigation;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.SignInACtivity;
import com.druva.project2021.pojo.MemebrPlanDAO;
import com.druva.project2021.pojo.Subscription_Status_DAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.paytm.pgsdk.TransactionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayTM extends AppCompatActivity {
    private String  TAG ="MainActivity";
    private ProgressBar progressBar;
    private EditText txnAmount;
    private String midString ="noSgqP05171793785678", txnAmountString="1.0", orderIdString="", txnTokenString="";
    private Button btnPayNow;
    private Integer ActivityRequestCode = 2;
    TextView planname,status,planduration,planexpirydate,loading;
    Button done;
    LinearLayout responelayout;
    String amount,duration,planName,planid;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.paytm_activity);

        preferenceManager=new PreferenceManager(getApplicationContext());
        planname=findViewById(R.id.planname);
        planduration=findViewById(R.id.plandurartion);
        planexpirydate=findViewById(R.id.planexpiry);
        status=findViewById(R.id.status);
        done=findViewById(R.id.done);
        responelayout=findViewById(R.id.statuslayout);
        loading=findViewById(R.id.loading);

        Intent intent=getIntent();
         amount=intent.getStringExtra("amount");
         duration=intent.getStringExtra("duration");
         planName=intent.getStringExtra("name");
                planid=intent.getStringExtra("plan_id");

         done.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 String result=status.getText().toString().trim();

                 if(result.equalsIgnoreCase("Success")){
                     startActivity(new Intent(PayTM.this, SignInACtivity.class));
                     finish();
                 }
                 else {
                     onBackPressed();
                 }
             }
         });


        txnAmountString=amount;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        String date = df.format(c.getTime());
        Random rand = new Random();
        int min =1000, max= 9999;
// nextInt as provided by Random is exclusive of the top value so you need to add 1
        int randomNum = rand.nextInt((max - min) + 1) + min;
        orderIdString =  date+String.valueOf(randomNum);

        getToken(amount);


    }

    private void getToken(String amount) {
        PublicHelper.showProgressDialog(PayTM.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        RequestBody code = RequestBody.create(MediaType.parse("text/plain"), "12345");
        RequestBody MID = RequestBody.create(MediaType.parse("text/plain"), midString);
        RequestBody ORDER_ID = RequestBody.create(MediaType.parse("text/plain"), orderIdString);
        RequestBody AMOUNT = RequestBody.create(MediaType.parse("text/plain"), txnAmountString);
        RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getUserid(Config.USERID));


        Call<Token_Res> call = RetrofitInstance.getInstance().getMyApi().generateTokenCall(code,MID,ORDER_ID,AMOUNT,userid);
        call.enqueue(new Callback<Token_Res>() {

            @Override
            public void onResponse(Call<Token_Res> call, Response<Token_Res> response) {
                Log.e(TAG, " respo "+ response.isSuccessful() );
                PublicHelper.dismissProgressDialog();
                try {

                    if (response.isSuccessful() && response.body()!=null){
                        if (response.body().getBody().getTxnToken()!="") {
                            Log.e(TAG, " transaction token : "+response.body().getBody().getTxnToken());
                            startPaytmPayment(response.body().getBody().getTxnToken());
                        }else {
                            Log.e(TAG, " Token status false");
                        }
                    }
                }catch (Exception e){
                    Log.e(TAG, " error in Token Res "+e.toString());
                }

            }

            @Override
            public void onFailure(Call<Token_Res> call, Throwable t) {
                PublicHelper.dismissProgressDialog();
                // Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();
                Log.e(TAG, " response error "+t.toString());

            }
        });

    }

   /* private  void getToken(String amount){
        Log.e(TAG, " get token start");
        txnAmountString=amount;
        PublicHelper.showProgressDialog(PayTM.this,"");
        ServiceWrapper serviceWrapper = new ServiceWrapper(null);
        Call<Token_Res> call = serviceWrapper.getTokenCall("12345", midString, orderIdString, txnAmountString);
        call.enqueue(new Callback<Token_Res>() {
            @Override
            public void onResponse(Call<Token_Res> call, Response<Token_Res> response) {
                  Log.e(TAG, " respo "+ response.isSuccessful() );
               PublicHelper.dismissProgressDialog();
                try {

                    if (response.isSuccessful() && response.body()!=null){
                        if (response.body().getBody().getTxnToken()!="") {
                            Log.e(TAG, " transaction token : "+response.body().getBody().getTxnToken());
                            startPaytmPayment(response.body().getBody().getTxnToken());
                        }else {
                            Log.e(TAG, " Token status false");
                        }
                    }
                }catch (Exception e){
                    Log.e(TAG, " error in Token Res "+e.toString());
                }
            }

            @Override
            public void onFailure(Call<Token_Res> call, Throwable t) {
               PublicHelper.dismissProgressDialog();
                Log.e(TAG, " response error "+t.toString());
            }
        });

    }*/

    public void startPaytmPayment (String token){

        txnTokenString = token;
	   // for test mode use it
        String host = "https://securegw-stage.paytm.in/";
        // for production mode use it
       // String host = "https://securegw.paytm.in/";
        String orderDetails = "MID: " + midString + ", OrderId: " + orderIdString + ", TxnToken: " + txnTokenString
                + ", Amount: " + txnAmountString;
        //Log.e(TAG, "order details "+ orderDetails);

        String callBackUrl = host + "theia/paytmCallback?ORDER_ID="+orderIdString;
        Log.e(TAG, " callback URL "+callBackUrl);
        PaytmOrder paytmOrder = new PaytmOrder(orderIdString, midString, txnTokenString, txnAmountString, callBackUrl);
        TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback(){
            @Override
            public void onTransactionResponse(Bundle bundle) {
                Log.e("TAG", "Response (onTransactionResponse) : "+bundle.toString());

               String Response=bundle.toString();
               loading.setVisibility(View.GONE);
               if(Response.contains("TXN_SUCCESS")){
                   add_subscription();

                  /* status.setText("Success");
                   status.setVisibility(View.VISIBLE);
                   status.setBackgroundColor(getResources().getColor(R.color.green));
                   responelayout.setVisibility(View.VISIBLE);*/

               }
               else {
                   status.setText("Failure");
                   status.setVisibility(View.VISIBLE);
                   status.setBackgroundColor(getResources().getColor(R.color.red));
               }

            }

            @Override
            public void networkNotAvailable() {
                Log.e(TAG, "network not available ");
                Toast.makeText(PayTM.this,"network not available ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }

            @Override
            public void onErrorProceed(String s) {
                Log.e(TAG, " onErrorProcess "+s.toString());
                Toast.makeText(PayTM.this,"Something went wrong ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                Log.e(TAG, "Clientauth "+s);
                Toast.makeText(PayTM.this,"Something went wrong ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }

            @Override
            public void someUIErrorOccurred(String s) {
                Log.e(TAG, " UI error "+s);
                Toast.makeText(PayTM.this," UI error ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
                Log.e(TAG, " error loading web "+s+"--"+s1);
                Toast.makeText(PayTM.this," error loading web ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }

            @Override
            public void onBackPressedCancelTransaction() {
                Log.e(TAG, "backPress ");
                Toast.makeText(PayTM.this," transaction canceled ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }

            @Override
            public void onTransactionCancel(String s, Bundle bundle) {
                Log.e(TAG, " transaction cancel "+s);
                Toast.makeText(PayTM.this," transaction cancel ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
                finish();
            }
        });

        transactionManager.setAppInvokeEnabled(false);
        transactionManager.setShowPaymentUrl(host + "theia/api/v1/showPaymentPage");
        transactionManager.startTransaction(this, ActivityRequestCode);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG ," result code "+resultCode);
        // -1 means successful  // 0 means failed
        // one error is - nativeSdkForMerchantMessage : networkError
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode && data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Log.e(TAG, key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
                }
            }
             Log.e(TAG, " data "+  data.getStringExtra("nativeSdkForMerchantMessage"));
             Log.e(TAG, " data response - "+data.getStringExtra("response"));


/*
 data response - {"BANKNAME":"WALLET","BANKTXNID":"1395841115",
 "CHECKSUMHASH":"7jRCFIk6eRmrep+IhnmQrlrL43KSCSXrmMP5pH0hekXaaxjt3MEgd1N9mLtWyu4VwpWexHOILCTAhybOo5EVDmAEV33rg2VAS/p0PXdk\u003d",
 "CURRENCY":"INR","GATEWAYNAME":"WALLET","MID":"EAcR4116","ORDERID":"100620202152",
 "PAYMENTMODE":"PPI","RESPCODE":"01","RESPMSG":"Txn Success","STATUS":"TXN_SUCCESS",
 "TXNAMOUNT":"2.00","TXNDATE":"2020-06-10 16:57:45.0","TXNID":"202006101112128001101683631290118"}
  */
            Toast.makeText(this, data.getStringExtra("nativeSdkForMerchantMessage")
                    + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
        }else{
            Log.e(TAG, " payment failed");
        }
    }

    private void add_subscription() {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("plan_name", planName);
        listDetails.put("plan_duration", duration);
        listDetails.put("amount", amount);
        listDetails.put("plan_id", planid);

        Call<Subscription_Status_DAO> call = RetrofitInstance.getInstance().getMyApi().add_subscription(listDetails);
        call.enqueue(new Callback<Subscription_Status_DAO>() {

            @Override
            public void onResponse(Call<Subscription_Status_DAO> call, Response<Subscription_Status_DAO> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(PayTM.this,"Trasaction Successfull", Toast.LENGTH_SHORT).show();

                Subscription_Status_DAO subscription_status_dao=response.body();

                status.setText("Success");
                status.setVisibility(View.VISIBLE);
                status.setBackgroundColor(getResources().getColor(R.color.green));

                planname.setText("Plan Name : "+planName);
                planduration.setText("Plan Duration : "+duration);
                responelayout.setVisibility(View.VISIBLE);



            }

            @Override
            public void onFailure(Call<Subscription_Status_DAO> call, Throwable t) {
                Toast.makeText(PayTM.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(PayTM.this, SubsvriptionwithFAQActivity.class));
        finish();
    }
}