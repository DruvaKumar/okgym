package com.druva.project2021.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Activity.UpdateBatchesActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Adapter_total_collection extends RecyclerView.Adapter<Adapter_total_collection.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<MembersOverallDAO> listcollections;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public Adapter_total_collection(ArrayList<MembersOverallDAO> listcollections, Context mContext) {
        this.listcollections = listcollections;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,mobile,fees,paidamount,unpaidamount,purchasedate;




        public MyViewHolder(View vi) {
            super(vi);

            name=vi.findViewById(R.id.name);
            mobile=vi.findViewById(R.id.mobile);
            fees=vi.findViewById(R.id.fees);
            paidamount=vi.findViewById(R.id.paidamount);
            unpaidamount=vi.findViewById(R.id.unpaidamount);
            purchasedate=vi.findViewById(R.id.purchasedate);


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public Adapter_total_collection(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_collection, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        preferenceManager=new PreferenceManager(mContext);
        MembersOverallDAO model = listcollections.get(position);
      holder.name.setText(model.getName());
      holder.mobile.setText(model.getMobile());
      holder.fees.setText(model.getFees());
      holder.paidamount.setText(model.getPaid_amount());
      holder.unpaidamount.setText(model.getUnpaid_amount());
      holder.purchasedate.setText(model.getPlan_start_date());




    }

    @Override
    public int getItemCount() {
        return listcollections.size();
    }


}

