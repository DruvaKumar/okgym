package com.druva.project2021.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Activity.AddEnquiryActivity;
import com.druva.project2021.Activity.AddmemberActivity;
import com.druva.project2021.Activity.MembersAttendance;
import com.druva.project2021.Activity.MembersIdCard;
import com.druva.project2021.Activity.MembersPlan;
import com.druva.project2021.Activity.MembersProfile;
import com.druva.project2021.Activity.UpdateEnquiryActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.pojo.MembersDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterEnquiry extends RecyclerView.Adapter<AdapterEnquiry.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<EnquiryDAO> listenquiry;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public AdapterEnquiry(ArrayList<EnquiryDAO> listenquiry, Context mContext) {
        this.listenquiry = listenquiry;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,mobile,enquirydate,folloupdate,addrerss;
        ImageView moredetails,profile;
        TextView ivcall,ivsms,ivwatsapp,ivadd;



        public MyViewHolder(View vi) {
            super(vi);

            name=vi.findViewById(R.id.tvmembername);
            mobile=vi.findViewById(R.id.tvmembermobile);
            enquirydate=vi.findViewById(R.id.tvenquirydate);
            folloupdate=vi.findViewById(R.id.tvfollowupdate);
            addrerss=vi.findViewById(R.id.tvmembereaddress);
            ivcall=vi.findViewById(R.id.imgcall);
            ivsms=vi.findViewById(R.id.imgsms);
            ivwatsapp=vi.findViewById(R.id.imgwatsapp);
            ivadd=vi.findViewById(R.id.imgadd);
            moredetails=vi.findViewById(R.id.ivmemdetails);



        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterEnquiry(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_enquiry, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        preferenceManager=new PreferenceManager(mContext);

        EnquiryDAO model = listenquiry.get(position);
        holder.name.setText(model.getMember_name());
        holder.mobile.setText(model.getMobile());
        holder.addrerss.setText(model.getAddress());
        holder.enquirydate.setText(model.getEnquiry_date());
        holder.folloupdate.setText(model.getFollowup_date());

        holder.ivadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EnquiryDAO user=listenquiry.get(position);

                Intent intent=new Intent(mContext, AddmemberActivity.class);

                Bundle b=new Bundle();
                b.putSerializable("user", user);
                intent.putExtras(b);
                mContext.startActivity(intent);
            }
        });

        holder.ivcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:"+model.getMobile()));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions((Activity) mContext,
                            new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                }else {     //have got permission
                    try{
                        mContext.startActivity(callIntent);  //call activity and make phone call
                    }
                    catch (android.content.ActivityNotFoundException ex){
                        Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


        holder.moredetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                PopupMenu popup = new PopupMenu(mContext, holder.moredetails);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_memberedit_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                   if(item.getItemId()==R.id.edit){
                       EnquiryDAO user=listenquiry.get(position);

                       Intent intent=new Intent(mContext, UpdateEnquiryActivity.class);

                       Bundle b=new Bundle();
                       b.putSerializable("user", user);
                       intent.putExtras(b);
                       mContext.startActivity(intent);
                        }

                        //delete
                        else if(item.getItemId()==R.id.delete){
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Are yoy sure to delete ")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String action="block";

                                            deletemember(model.getEnquiry_id());
                                            listenquiry.remove(position);
                                            notifyDataSetChanged();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                        //Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

    }


    private void deletemember(String memeber_id) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("enquiry_id", memeber_id);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().delete_enquiry(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return listenquiry.size();
    }
}

