package com.druva.project2021.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Activity.UpdateBatchesActivity;
import com.druva.project2021.Activity.UpdatePlansActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.BatchDAO;
import com.druva.project2021.pojo.PlansDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterPlans extends RecyclerView.Adapter<AdapterPlans.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<PlansDAO> listplans;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public AdapterPlans(ArrayList<PlansDAO> listplans, Context mContext) {
        this.listplans = listplans;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,fees,duration,details;
        CardView card;




        public MyViewHolder(View vi) {
            super(vi);

            name=vi.findViewById(R.id.tvplanname);
            fees=vi.findViewById(R.id.tvplanfees);
            duration=vi.findViewById(R.id.tvplanduration);
            details=vi.findViewById(R.id.tvplandetails);
           // card=vi.findViewById(R.id.cardview);


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterPlans(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plans, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        PlansDAO model = listplans.get(position);
        holder.name.setText(model.getPlan_name());
        holder.fees.setText(model.getFeese());
        holder.duration.setText(model.getDuration());
        holder.details.setText(model.getPlan_details());
        preferenceManager=new PreferenceManager(mContext);

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(mContext, holder.name);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_memberedit_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.edit){
                            PlansDAO user=listplans.get(position);

                            Intent intent=new Intent(mContext, UpdatePlansActivity.class);

                            Bundle b=new Bundle();
                            b.putSerializable("user", user);
                            intent.putExtras(b);
                            mContext.startActivity(intent);

                        }

                        //delete
                        else if(item.getItemId()==R.id.delete){
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Do you want to delete?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String action="block";

                                            delete_plan(model.getPlan_id());
                                            listplans.remove(position);
                                            notifyDataSetChanged();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                        //Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

    }

    private void delete_plan(String plan_id) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("plan_id", plan_id);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().delete_plan(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();



            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public int getItemCount() {
        return listplans.size();
    }
}

