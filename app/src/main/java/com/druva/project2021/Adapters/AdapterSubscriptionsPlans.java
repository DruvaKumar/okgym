package com.druva.project2021.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Activity.UpdateBatchesActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;

import com.druva.project2021.PaymentGateway.PayTM;
import com.druva.project2021.R;
import com.druva.project2021.pojo.Subscription_plansDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterSubscriptionsPlans extends RecyclerView.Adapter<AdapterSubscriptionsPlans.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<Subscription_plansDAO> listsubscriptionplans;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public AdapterSubscriptionsPlans(ArrayList<Subscription_plansDAO> listsubscriptionplans, Context mContext) {
        this.listsubscriptionplans = listsubscriptionplans;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView price,planname,duration,expirydate,plandetails,actual_price;
        RadioButton trial;
        Button proceed;



        public MyViewHolder(View vi) {
            super(vi);

            price=vi.findViewById(R.id.plan_price);
            planname=vi.findViewById(R.id.plan_name);
            duration=vi.findViewById(R.id.plan_duration);
            expirydate=vi.findViewById(R.id.planexpirydate);
            plandetails=vi.findViewById(R.id.htmlplandescription);
            trial=vi.findViewById(R.id.trial);
            proceed=vi.findViewById(R.id.btn_continue);
            actual_price=vi.findViewById(R.id.actual_price);


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterSubscriptionsPlans(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_subscription_plans, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        preferenceManager=new PreferenceManager(mContext);

        Subscription_plansDAO model = listsubscriptionplans.get(position);

        String actualprice=  model.getActual_price();
        SpannableString strikedstring=new SpannableString(actualprice);
        strikedstring.setSpan(new StrikethroughSpan(),0,strikedstring.length(),0);
    //  holder.actual_price.setText("\u20B9"+strikedstring);
      String hh="\u20B9"+model.getActual_price();

        holder.actual_price.setText(Html.fromHtml("<del>"+hh+"</del>"));

      holder.price.setText("\u20B9"+model.getPrice());
      holder.planname.setText(model.getPlan_name());
      holder.duration.setText(model.getDuration());
     // holder.expirydate.setText("Expires on : "+preferenceManager.getTrialExpiry("trialexpiry"));

        holder.plandetails.setText(model.getFeatures());
        String trialdate=preferenceManager.getTrialExpiry("trialexpiry");
        String subs=preferenceManager.getSubscription("subscription");


        if(subs==null){
            if(model.getId().equalsIgnoreCase("1")) {
                holder.trial.setChecked(true);
                holder.expirydate.setText(preferenceManager.getTrialExpiry("trialexpiry"));
                holder.trial.setVisibility(View.VISIBLE);
                holder.expirydate.setVisibility(View.VISIBLE);
                holder.proceed.setVisibility(View.GONE);
            }
            else {
                holder.expirydate.setVisibility(View.GONE);
            }
        }
        else {
            if(model.getId().equalsIgnoreCase(subs)) {
                holder.trial.setChecked(true);
                holder.trial.setVisibility(View.VISIBLE);
                holder.expirydate.setText("Expires on : "+preferenceManager.getExpirydate("expirydate"));
                holder.proceed.setVisibility(View.GONE);
            }
            else if(model.getId().equalsIgnoreCase("1")) {
                holder.proceed.setVisibility(View.GONE);
                holder.expirydate.setVisibility(View.GONE);

            }
            else {
                holder.expirydate.setVisibility(View.GONE);
            }
        }

       /* if(subs!=null | !subs.equalsIgnoreCase("null"))
        {
            if(model.getId().equalsIgnoreCase(subs)) {
                holder.trial.setChecked(true);
                holder.trial.setVisibility(View.VISIBLE);
                holder.expirydate.setText("Expires on : "+preferenceManager.getExpirydate("expirydate"));
                holder.proceed.setVisibility(View.GONE);
            }
            else {
                holder.expirydate.setVisibility(View.GONE);
               // holder.proceed.setVisibility(View.GONE);
            }

        }
        else
        {
            holder.expirydate.setVisibility(View.GONE);
           // holder.trial.setChecked(true);
            holder.proceed.setVisibility(View.GONE);
        }*/


        holder.proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, PayTM.class);
                intent.putExtra("amount",listsubscriptionplans.get(position).getPrice());
                intent.putExtra("duration",listsubscriptionplans.get(position).getDuration());
                intent.putExtra("name",listsubscriptionplans.get(position).getPlan_name());
                intent.putExtra("plan_id",listsubscriptionplans.get(position).getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listsubscriptionplans.size();
    }


}

