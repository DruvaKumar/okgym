package com.druva.project2021.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.druva.project2021.Activity.MembersAttendance;
import com.druva.project2021.Activity.MembersIdCard;
import com.druva.project2021.Activity.MembersPlan;
import com.druva.project2021.Activity.MembersProfile;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterLiveMembersAdapter extends RecyclerView.Adapter<AdapterLiveMembersAdapter.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<MembersOverallDAO> listmembers;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public AdapterLiveMembersAdapter(ArrayList<MembersOverallDAO> listmembers, Context mContext) {
        this.listmembers = listmembers;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,id,joindate,expirydate,unpaidamount;
        ImageView moredetails,profile;
        TextView ivcall,ivsms,ivwatsapp,ivattendence;

        public MyViewHolder(View vi) {
            super(vi);

            name=vi.findViewById(R.id.tvmembername);
            id=vi.findViewById(R.id.tvmemberid);
            joindate=vi.findViewById(R.id.tvmemberejoindate);
            expirydate=vi.findViewById(R.id.tvmemberexpiry);
            ivcall=vi.findViewById(R.id.imgcall);
            ivsms=vi.findViewById(R.id.imgsms);
            ivwatsapp=vi.findViewById(R.id.imgwatsapp);
            ivattendence=vi.findViewById(R.id.imgattendence);
            moredetails=vi.findViewById(R.id.ivmemdetails);
            profile=vi.findViewById(R.id.ivprofile);
            unpaidamount=vi.findViewById(R.id.tvunpaidamount);

        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterLiveMembersAdapter(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_members, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        preferenceManager=new PreferenceManager(mContext);
        MembersOverallDAO model = listmembers.get(position);
        holder.name.setText(model.getName());
        holder.id.setText("Member Id : " +model.getMember_id());
        holder.joindate.setText("Joined : "+model.getJoindate());
        holder.expirydate.setText("Expiry Date : "+model.getPlan_expiry_date());
        holder.unpaidamount.setText("Unpaid Amount : "+model.getUnpaid_amount());
        holder.unpaidamount.setVisibility(View.VISIBLE);

        String thumb=model.getProfilepic();
        Glide.with(mContext).load(thumb)
                .thumbnail(0.5f)
                .into(holder.profile);

        String memeberid=holder.id.getText().toString().trim();
        String name=holder.name.getText().toString();
        String mobile=model.getMobile();
        String dob=model.getJoindate();


        holder.ivcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:"+mobile));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions((Activity) mContext,
                            new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                }else {     //have got permission
                    try{
                        mContext.startActivity(callIntent);  //call activity and make phone call
                    }
                    catch (android.content.ActivityNotFoundException ex){
                        Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        holder.ivsms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.ivattendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name=holder.name.getText().toString();

                Dialog dialog=new Dialog(mContext);
                dialog.setContentView(R.layout.checkin_chckout_layout);


               TextView checkin=dialog.findViewById(R.id.checkin);
               TextView checkout=dialog.findViewById(R.id.checkout);

               checkin.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       Add_Attendance_checkin(model.getMember_id(),name);
                       dialog.cancel();

                   }
               });

                checkout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Add_Attendance_checkout(model.getMember_id(),name);
                        dialog.cancel();
                    }
                });
              dialog.show();

            }
        });

        holder.moredetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String memeber_id=model.getMember_id();
                String name=model.getName();

                PopupMenu popup = new PopupMenu(mContext, holder.moredetails);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_memberdetails_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.profile){
                            MembersOverallDAO user=listmembers.get(position);

                          Intent intent=new Intent(mContext, MembersProfile.class);

                            Bundle b=new Bundle();
                            b.putSerializable("user", user);
                            intent.putExtras(b);
                          mContext.startActivity(intent);
                        }

    //plans
                        else if(item.getItemId()==R.id.plan){
                            MembersOverallDAO user=listmembers.get(position);
                            Intent intent=new Intent(mContext, MembersPlan.class);
                            Bundle b=new Bundle();
                            b.putSerializable("user", user);
                            intent.putExtras(b);
                            mContext.startActivity(intent);
                        }

   //idcard
                        else if(item.getItemId()==R.id.idcard){
                            MembersOverallDAO user=listmembers.get(position);
                            Intent intent=new Intent(mContext, MembersIdCard.class);
                            Bundle b=new Bundle();
                            b.putSerializable("user", user);
                            intent.putExtras(b);
                            mContext.startActivity(intent);
                        }

    //attendace

                        else if(item.getItemId()==R.id.attendance){

                            MembersOverallDAO user=listmembers.get(position);
                            Intent intent=new Intent(mContext, MembersAttendance.class);
                            Bundle b=new Bundle();
                            b.putSerializable("user", user);
                            intent.putExtras(b);
                            mContext.startActivity(intent);
                        }

   //block
                        else if(item.getItemId()==R.id.block){
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Do you want to Block "+model.getName() +"?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                           String action="block";

                                            blockmember(memeber_id);
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }

   //extra days
                        else if(item.getItemId()==R.id.extradays){

                            Dialog dialog=new Dialog(mContext);
                            dialog.setContentView(R.layout.extra_days_layout);

                            TextInputEditText days=dialog.findViewById(R.id.et_extradays);
                            Button procced=dialog.findViewById(R.id.proceed);

                            procced.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String extra=days.getEditableText().toString().trim()+" days";

                                    update_extra_days_to_memeber(memeber_id,extra);
                                    dialog.cancel();
                                }
                            });
                            dialog.show();


                           /* String extra="1 month";
                           */
                        }

   //delete
                        else if(item.getItemId()==R.id.delete){
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Are yoy sure to delete "+model.getName() +"?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String action="block";

                                            deletemember(memeber_id);
                                            listmembers.remove(position);
                                            notifyDataSetChanged();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                        //Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

    }

    private void Add_Attendance_checkin(String member_id, String name) {

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", member_id);
        listDetails.put("name",name);


        Call<Void> call = RetrofitInstance.getInstance().getMyApi().checkin(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void Add_Attendance_checkout(String member_id, String name) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", member_id);
        listDetails.put("name",name);


        Call<Void> call = RetrofitInstance.getInstance().getMyApi().checkout(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
// extra days
    private void update_extra_days_to_memeber(String memeberid,String extradays) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", memeberid);
        listDetails.put("extra", extradays);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().update_extra_days_to_memeber(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void deletemember(String memeberid) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", memeberid);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().deletemember(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void blockmember(String memeberid) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", memeberid);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().blockmember(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }




    @Override
    public int getItemCount() {
        return listmembers.size();
    }
}

