package com.druva.project2021.Adapters;

import android.content.Context;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.Member_AttendanceDAO;

import java.util.ArrayList;


public class AdapterGymAttendance extends RecyclerView.Adapter<AdapterGymAttendance.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<Member_AttendanceDAO> membersattedancelist;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public AdapterGymAttendance(ArrayList<Member_AttendanceDAO> membersattedancelist, Context mContext) {
        this.membersattedancelist = membersattedancelist;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,checkin,checkout;
        public MyViewHolder(View vi) {
            super(vi);
            name=vi.findViewById(R.id.name);
            checkin=vi.findViewById(R.id.checkin);
            checkout=vi.findViewById(R.id.checkout);


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterGymAttendance(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attandance, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Member_AttendanceDAO model = membersattedancelist.get(position);
        holder.name.setText(model.getMember_name());
        holder.checkin.setText(model.getCheck_in());
        holder.checkout.setText(model.getCheck_out());

       

    }




    @Override
    public int getItemCount() {
        return membersattedancelist.size();
    }
}

