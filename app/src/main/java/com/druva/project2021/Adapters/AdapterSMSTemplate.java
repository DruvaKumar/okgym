package com.druva.project2021.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersDAO;
import com.druva.project2021.pojo.SMStemplatesDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterSMSTemplate extends RecyclerView.Adapter<AdapterSMSTemplate.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<SMStemplatesDAO> listsms;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager pre;


    public AdapterSMSTemplate(ArrayList<SMStemplatesDAO> listsms, Context mContext) {
        this.listsms = listsms;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title , sms;
        ImageView ivdelete;



        public MyViewHolder(View vi) {
            super(vi);

            title=vi.findViewById(R.id.tvtittle);
            sms=vi.findViewById(R.id.smsdescription);
            ivdelete=vi.findViewById(R.id.ivdelete);


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterSMSTemplate(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sms, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        pre=new PreferenceManager(mContext);
        SMStemplatesDAO model = listsms.get(position);
        holder.title.setText(model.getTitle());
        holder.sms.setText(model.getDescription());


        holder.ivdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String smsid=model.getSms_id();
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Are You sure you want to delete")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                delete_sms_template(pre.getUserid(Config.USERID),smsid);
                                listsms.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();



            }
        });


    }

    private void delete_sms_template(String userid, String smsid) {

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", userid);
        listDetails.put("sms_id", smsid);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().delete_sms_template(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return listsms.size();
    }
}

