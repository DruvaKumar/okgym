package com.druva.project2021.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.druva.project2021.Activity.UpdateBatchesActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.BatchDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterBatch extends RecyclerView.Adapter<AdapterBatch.MyViewHolder>
        implements RecyclerView.OnItemTouchListener {

    private ArrayList<BatchDAO> listbatches;
    private Context mContext;
    private OnItemClickListener mListener;
    PreferenceManager preferenceManager;


    public AdapterBatch(ArrayList<BatchDAO> listbatches, Context mContext) {
        this.listbatches = listbatches;
        this.mContext = mContext;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,starttime,endtime,batchcount;
        ImageView ivmore;



        public MyViewHolder(View vi) {
            super(vi);

            name=vi.findViewById(R.id.tvbatchname);
            starttime=vi.findViewById(R.id.tvstart);
            endtime=vi.findViewById(R.id.tvend);
            ivmore=vi.findViewById(R.id.imgmore);
            batchcount=vi.findViewById(R.id.batchcount);


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public AdapterBatch(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;

    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_batch, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        preferenceManager=new PreferenceManager(mContext);
        BatchDAO model = listbatches.get(position);
      holder.name.setText(model.getBatch_name());
      holder.starttime.setText("Start Time : "+model.getStarttime());
      holder.endtime.setText("End Time : "+model.getEndtime());

      String tot_members=model.getTotal_members();
      if(tot_members==null){
          holder.batchcount.setText("0" + "/" + model.getBatch_limit());
      }
      else {
          holder.batchcount.setText(model.getTotal_members() + "/" + model.getBatch_limit());
      }

        holder.ivmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(mContext, holder.ivmore);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_memberedit_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.edit){
                            BatchDAO user=listbatches.get(position);

                            Intent intent=new Intent(mContext, UpdateBatchesActivity.class);

                            Bundle b=new Bundle();
                            b.putSerializable("user", user);
                            intent.putExtras(b);
                            mContext.startActivity(intent);
                        }

                        //delete
                        else if(item.getItemId()==R.id.delete){
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Do you want to delete?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String action="block";

                                            delete_batch(model.getBatch_id());
                                            listbatches.remove(position);
                                            notifyDataSetChanged();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                        //Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });


    }

    @Override
    public int getItemCount() {
        return listbatches.size();
    }

    private void delete_batch(String memeber_id) {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("batch_id", memeber_id);

        Call<Void> call = RetrofitInstance.getInstance().getMyApi().delete_batch(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(mContext,"Success", Toast.LENGTH_SHORT).show();



            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}

