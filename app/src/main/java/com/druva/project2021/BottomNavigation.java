package com.druva.project2021;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.databinding.ActivityMainBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class BottomNavigation extends AppCompatActivity {
    private ActivityMainBinding binding;

    TextView tvGymname;
    ImageView search,menuoptions;
    PreferenceManager preferenceManager;
    private GoogleSignInClient googleSignInClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        preferenceManager=new PreferenceManager(getApplicationContext());

        String Gymname=preferenceManager.getGymName(Config.GYMNAME);

        GoogleSignInOptions gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient= GoogleSignIn.getClient(this,gso);

        tvGymname=findViewById(R.id.header);
        search=findViewById(R.id.img_search);
        menuoptions=findViewById(R.id.img_optionsmenu);

        tvGymname.setText(Gymname);

        menuoptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(getApplicationContext(), "Show Bottom", Toast.LENGTH_SHORT).show();
                showbottomsheetlayout();

            }
        });

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
    }

    private void showbottomsheetlayout() {
        BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(BottomNavigation.this);
        bottomSheetDialog.setContentView(R.layout.bottomsheet_layout);

        TextView share=bottomSheetDialog.findViewById(R.id.share);
        TextView rate=bottomSheetDialog.findViewById(R.id.rate);
        TextView contact=bottomSheetDialog.findViewById(R.id.contactus);
        TextView terms=bottomSheetDialog.findViewById(R.id.terms);
        TextView privacy=bottomSheetDialog.findViewById(R.id.privacy);
        TextView logout=bottomSheetDialog.findViewById(R.id.logout);
        ImageView close=bottomSheetDialog.findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Download Gym App from " + "");
                sendIntent.setType("text/plain");
                PackageManager packageManager = getApplicationContext().getPackageManager();
                if (sendIntent.resolveActivity(packageManager) != null) {
                    startActivity(sendIntent);
                } else {
                    Log.d("TAG", "No Intent available to handle action");
                }
            }
        });

        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent(getApplicationContext(),WebviewActivity.class);
               intent.putExtra("heading","Terms & Conditions");
               intent.putExtra("url","google.com");
               startActivity(intent);
            }
        });
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),WebviewActivity.class);
                intent.putExtra("heading","Privacy Policy");
                intent.putExtra("url","youtube.com");
                startActivity(intent);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BottomNavigation.this);
                builder.setMessage("Are you sure you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Session session=new Session(getApplicationContext());
                                session.setLoggedin(false);
                                signout_from_google();
                                startActivity(new Intent(BottomNavigation.this, SignInACtivity.class));
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        bottomSheetDialog.show();
    }
    private void signout_from_google() {
        googleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BottomNavigation.this);
        builder.setMessage("Do you want to Exit ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                      Intent intent=new Intent(Intent.ACTION_MAIN);
                      intent.addCategory(Intent.CATEGORY_HOME);
                      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                      startActivity(intent);
                      finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
