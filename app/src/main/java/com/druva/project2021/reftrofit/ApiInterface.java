package com.druva.project2021.reftrofit;

import com.druva.project2021.PaymentGateway.Token_Res;
import com.druva.project2021.pojo.AttendanceDAO;
import com.druva.project2021.pojo.BatchnamesDAO;
import com.druva.project2021.pojo.DashbaordDAO;
import com.druva.project2021.pojo.FAQ;
import com.druva.project2021.pojo.First_open_DAO;
import com.druva.project2021.pojo.Get_Gym_profileDAO;
import com.druva.project2021.pojo.Member_AttendanceDAO;
import com.druva.project2021.pojo.BatchDAO;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.pojo.GymprofileDAO;
import com.druva.project2021.pojo.MembersDAO;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.pojo.MemebrPlanDAO;
import com.druva.project2021.pojo.Plan_detailsDAO;
import com.druva.project2021.pojo.Plan_nameDAO;
import com.druva.project2021.pojo.Plan_purchase_DAO;
import com.druva.project2021.pojo.PlansDAO;
import com.druva.project2021.pojo.SMStemplatesDAO;
import com.druva.project2021.pojo.Scannned_attendnce_DAO;
import com.druva.project2021.pojo.Subscription_Status_DAO;
import com.druva.project2021.pojo.Subscription_plansDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {


    //create gym profile
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("Create_gym_profile.php")
    Call<GymprofileDAO> addgymdetails(@Body HashMap<String, Object> jsonObject);


    //add members
    @Multipart
    @POST("add_member.php")
    Call<Void> addnewmembers(@Part MultipartBody.Part file1 ,
                               @Part("member_name") RequestBody member_name,
                               @Part("member_id") RequestBody member_id,
                               @Part("member_details") RequestBody member_details,
                               @Part("member_mobile") RequestBody member_mobile,
                               @Part("member_address") RequestBody member_address,
                               @Part("member_email") RequestBody member_email,
                               @Part("member_gender") RequestBody member_gender,
                               @Part("member_joindate") RequestBody member_joindate,
                               @Part("member_dob") RequestBody member_dob,
                               @Part("member_batch") RequestBody member_batch,
                               @Part("userid") RequestBody userid,
                               @Part("enquiryid") RequestBody enquiryid);


//add batches
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("add_plans.php")
    Call<Object>addplans(@Body HashMap<String, Object> jsonObject);



//add plans
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("add_batch.php")
    Call<Object>addbatches(@Body HashMap<String, Object> jsonObject);



    //add enquiry
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("add_enquiry.php")
    Call<Object> addenquiry(@Body HashMap<String, Object> jsonObject);


    // create gym profile with image
    @Multipart
    @POST("Create_gym_profile.php")
    Call<GymprofileDAO> creategynprofile
            (@Part MultipartBody.Part imageBody1,
             @Part("gym_name")RequestBody name,
             @Part("address")RequestBody address,
             @Part("contact")RequestBody contact,
             @Part("email")RequestBody email,
             @Part("website")RequestBody website,
             @Part("userid")RequestBody userid,
             @Part("mobile")RequestBody mobile);

    // update gym profile
    @Multipart
    @POST("Update_gym_profile.php")
    Call<GymprofileDAO> update_gymprofile
    (
     @Part("gym_name")RequestBody name,
     @Part("address")RequestBody address,
     @Part("contact")RequestBody contact,
     @Part("email")RequestBody email,
     @Part("website")RequestBody website,
     @Part("userid")RequestBody userid);

    //upload member documents

    @Multipart
    @POST("add_member_document.php")
    Call<Void> uploadmemberdocuments (@Part MultipartBody.Part imageBody1,
                                               @Part MultipartBody.Part imageBody2,
                                               @Part ("userid") RequestBody userid,
                                               @Part ("memberid") RequestBody memberid);

    // get members
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_all_members.php")
    Call<ArrayList<MembersOverallDAO>>get_all_members(@Body HashMap<String, Object> jsonObject);


    // add sms template
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("add_sms_template.php")
    Call<Object> addtemplates(@Body HashMap<String, Object> listDetails);

    // get plans
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_all_plans.php")
    Call<ArrayList<PlansDAO>> get_all_plans(@Body HashMap<String, Object> listDetails);

    // get templates
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_all_sms_templates.php")
    Call<ArrayList<SMStemplatesDAO>> get_all_templates(@Body HashMap<String, Object> listDetails);

    // get batches
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_all_batch.php")
    Call<ArrayList<BatchDAO>> get_all_batch(@Body HashMap<String, Object> listDetails);

    // get enqury
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_all_enquiry.php")
    Call<ArrayList<EnquiryDAO>> get_all_enquiry(@Body HashMap<String, Object> listDetails);

    //update members

    @Multipart
    @POST("update_member_details.php")
    Call<Void> update_member_details(
                                                      @Part("member_name") RequestBody member_name,
                                                      @Part("member_id") RequestBody member_id,
                                                      @Part("member_mobile") RequestBody member_mobile,
                                                      @Part("member_address") RequestBody member_address,
                                                      @Part("member_email") RequestBody member_email,
                                                      @Part("member_gender") RequestBody member_gender,
                                                      @Part("member_joindate") RequestBody member_joindate,
                                                      @Part("member_dob") RequestBody member_dob,
                                                      @Part("member_batch") RequestBody member_batch,
                                                      @Part("userid") RequestBody userid);
    /*@Part MultipartBody.Part file1 ,
                                                      @Part MultipartBody.Part file2 ,
                                                      @Part MultipartBody.Part file3 ,*/


    //extra_days_to_member
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("extra_days_to_member.php")
    Call<Void> update_extra_days_to_memeber(@Body HashMap<String, Object> listDetails);

    //block a member
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("block_member.php")
    Call<Void>blockmember(@Body HashMap<String, Object> listDetails);

    //delete a members
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("delete_member.php")
    Call<Void> deletemember(@Body HashMap<String, Object> listDetails);

//add_plan_to_member
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("add_plan_to_member.php")
    Call<Plan_purchase_DAO> add_plan_to_member(@Body HashMap<String, Object> listDetails);


    // get member attendance
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_member_attendance.php")
    Call<ArrayList<Member_AttendanceDAO>>getmemberattendance(@Body HashMap<String, Object> listDetails);


    //get all gym attendance
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_all_members_attendance.php")
    Call<ArrayList<Member_AttendanceDAO>> get_gym_attendance(@Body HashMap<String, Object> listDetails);


    //checkin
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("member_checkin.php")
    Call<Void> checkin(@Body HashMap<String, Object> listDetails);

    //checkout
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("member_checkout.php")
    Call<Void> checkout(@Body HashMap<String, Object> listDetails);


    //delete sms template
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("delete_sms_template.php")
    Call<Void> delete_sms_template(@Body HashMap<String, Object> listDetails);

// delete enquiry
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("delete_enquiry.php")
    Call<Void> delete_enquiry(@Body HashMap<String, Object> listDetails);


    // update enquiry
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("Update_enquiry.php")
    Call<Object> updateenquiry(@Body HashMap<String, Object> listDetails);

    // delete batch
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("delete_batch.php")
    Call<Void> delete_batch(@Body HashMap<String, Object> listDetails);

    // update_batch
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("Update_batch.php")
    Call<Void> update_batch(@Body HashMap<String, Object> listDetails);


    // Dashbaord
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("dashboard.php")
    Call<DashbaordDAO> get_dashboard_data(@Body HashMap<String, Object> listDetails);


    // collection details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_totalcollection.php")
    Call<ArrayList<MembersOverallDAO>> get_collection_data(@Body HashMap<String, Object> listDetails);

    // live members details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_livemembers.php")
    Call<ArrayList<MembersOverallDAO>> get_live_members(@Body HashMap<String, Object> listDetails);

    // live expired details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_expiredmembers.php")
    Call<ArrayList<MembersOverallDAO>> get_expired_members(@Body HashMap<String, Object> jsonObject);


    // live expiring today details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_expiretoday.php")
    Call<ArrayList<MembersOverallDAO>> get_expireding_today_members(@Body HashMap<String, Object> listDetails);


    // live expiring in 1-5 days details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_expired1to5.php")
    Call<ArrayList<MembersOverallDAO>> get_expireding_in_1_5_members(@Body HashMap<String, Object> listDetails);

    // live expiring in 6-10 days details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_expired6to10.php")
    Call<ArrayList<MembersOverallDAO>> get_expireding_in_6_10_members(@Body HashMap<String, Object> listDetails);

    // live expiring in 11-15 days details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_expired_15.php")
    Call<ArrayList<MembersOverallDAO>> get_expireding_in_11_15_members(@Body HashMap<String, Object> listDetails);

    // lblocked members details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_blocked.php")
    Call<ArrayList<MembersOverallDAO>> blocked_members(@Body HashMap<String, Object> listDetails);

    // unpaid members details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_unpaid.php")
    Call<ArrayList<MembersOverallDAO>> unpaid_members(@Body HashMap<String, Object> listDetails);

    // Todays Birthday members details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_dashboard_bday.php")
    Call<ArrayList<MembersOverallDAO>> get_birthday_members(@Body HashMap<String, Object> listDetails);

    // get individual member plan
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_member_plans.php")
    Call<MemebrPlanDAO> get_member_plan(@Body HashMap<String, Object> listDetails);

    // get  plan array
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("PlansDAO.php")
    Call<ArrayList<Plan_nameDAO>> get_plan_array(@Body HashMap<String, Object> listDetails);

    //plan details fron plan name
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_plan_details_from_id.php")
    Call<ArrayList<Plan_detailsDAO>> get_plan_details(@Body HashMap<String, Object> listDetails);

    //Batch names
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("BatchDAO.php")
    Call<ArrayList<BatchnamesDAO>> get_batch_array(@Body HashMap<String, Object> listDetails);


    //Update plans
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("Update_plan.php")
    Call<Object> updateplans(@Body HashMap<String, Object> listDetails);

    //delete plans
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("delete_plan.php")
    Call<Void> delete_plan(@Body HashMap<String, Object> listDetails);


    //subscription plans
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_gym_subscription_plans.php")
    Call<ArrayList<Subscription_plansDAO>>getsubscriptionplans(@Body HashMap<String, Object> listDetails);


    //FAQ
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("FAQ.php")
    Call<ArrayList<FAQ>> getfaq(@Body HashMap<String, Object> listDetails);


    //scanner attendance
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("Scanned_attendance.php")
    Call<Scannned_attendnce_DAO> scanned_attendance(@Body HashMap<String, Object> listDetails);


    //Add subsription
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("add_subscriptions.php")
    Call<Subscription_Status_DAO> add_subscription(@Body HashMap<String, Object> listDetails);

    //get_Member_subscription
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_Member_subscription.php")
    Call<Subscription_Status_DAO> get_Member_subscription(@Body HashMap<String, Object> listDetails);

    //PAYTM
    @Multipart
    @POST("Paytm/init_Transaction.php")
    Call<Token_Res> generateTokenCall(
            @Part("code") RequestBody language,
            @Part("MID") RequestBody mid,
            @Part("ORDER_ID") RequestBody order_id,
            @Part("AMOUNT") RequestBody amount,
            @Part("userid") RequestBody customerid
    );

    //get_gym_details
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("Firstopen.php")
    Call<First_open_DAO> getgymdetails(@Body HashMap<String, Object> listDetails);

    //get_gym_profile
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("get_gym_profile.php")
    Call<Get_Gym_profileDAO> get_gym_profile(@Body HashMap<String, Object> listDetails);

    //Google Sign In
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
    @POST("google_signin.php")
    Call<Void> signin_with_google(@Body HashMap<String, Object> listDetails);


}


