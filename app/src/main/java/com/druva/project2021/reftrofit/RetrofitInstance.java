package com.druva.project2021.reftrofit;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.pojo.signPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitInstance {
    private static RetrofitInstance instance = null;
    private  static Retrofit retrofit;
    private ApiInterface myApi;
    private  static  final  String BASEURL= Config.BASE_URL;

   private RetrofitInstance() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit=new Retrofit.Builder().
                baseUrl(BASEURL).
                addConverterFactory(ScalarsConverterFactory.create()).
                addConverterFactory(GsonConverterFactory.create(gson)).
                build();
        myApi = retrofit.create(ApiInterface.class);


    }
    public static synchronized RetrofitInstance getInstance() {
        if (instance == null) {
            instance = new RetrofitInstance();
        }
        return instance;
    }

    public ApiInterface getMyApi() {
        return myApi;
    }
}
