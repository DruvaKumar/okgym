package com.druva.project2021.pojo;

import java.io.Serializable;

public class MembersOverallDAO implements Serializable {
    private String member_id;

    private String document_1;

    private String Qr_code;

    private String joindate;

    private String address;

    private String document_2;

    private String gender;

    private String profilepic;

    private String plan_expiry_date;

    private String mobile;

    private String batch;

    private String plan_duration;

    private String dob;

    private String name;

    private String plan_details;

    private String plan;

    private String plan_start_date;

    private String email;
    private String fees;
    private String paid_amount;
    private String unpaid_amount;

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(String paid_amount) {
        this.paid_amount = paid_amount;
    }

    public String getUnpaid_amount() {
        return unpaid_amount;
    }

    public void setUnpaid_amount(String unpaid_amount) {
        this.unpaid_amount = unpaid_amount;
    }

    private String status;

    public String getMember_id ()
    {
        return member_id;
    }

    public void setMember_id (String member_id)
    {
        this.member_id = member_id;
    }

    public String getDocument_1 ()
    {
        return document_1;
    }

    public void setDocument_1 (String document_1)
    {
        this.document_1 = document_1;
    }

    public String getQr_code ()
    {
        return Qr_code;
    }

    public void setQr_code (String Qr_code)
    {
        this.Qr_code = Qr_code;
    }

    public String getJoindate ()
    {
        return joindate;
    }

    public void setJoindate (String joindate)
    {
        this.joindate = joindate;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getDocument_2 ()
    {
        return document_2;
    }

    public void setDocument_2 (String document_2)
    {
        this.document_2 = document_2;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getProfilepic ()
    {
        return profilepic;
    }

    public void setProfilepic (String profilepic)
    {
        this.profilepic = profilepic;
    }

    public String getPlan_expiry_date ()
    {
        return plan_expiry_date;
    }

    public void setPlan_expiry_date (String plan_expiry_date)
    {
        this.plan_expiry_date = plan_expiry_date;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getBatch ()
    {
        return batch;
    }

    public void setBatch (String batch)
    {
        this.batch = batch;
    }

    public String getPlan_duration ()
    {
        return plan_duration;
    }

    public void setPlan_duration (String plan_duration)
    {
        this.plan_duration = plan_duration;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getPlan_details ()
    {
        return plan_details;
    }

    public void setPlan_details (String plan_details)
    {
        this.plan_details = plan_details;
    }

    public String getPlan ()
    {
        return plan;
    }

    public void setPlan (String plan)
    {
        this.plan = plan;
    }

    public String getPlan_start_date ()
    {
        return plan_start_date;
    }

    public void setPlan_start_date (String plan_start_date)
    {
        this.plan_start_date = plan_start_date;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member_id = "+member_id+", document_1 = "+document_1+", Qr_code = "+Qr_code+", joindate = "+joindate+", address = "+address+", document_2 = "+document_2+", gender = "+gender+", profilepic = "+profilepic+", plan_expiry_date = "+plan_expiry_date+", mobile = "+mobile+", batch = "+batch+", plan_duration = "+plan_duration+", dob = "+dob+", name = "+name+", plan_details = "+plan_details+", plan = "+plan+", plan_start_date = "+plan_start_date+", email = "+email+", status = "+status+" ,fees = "+fees+" ,paid_amount ="+paid_amount+" ,unpaid_amount ="+unpaid_amount+"]";
    }


    /*private String member_id;

    private String document_1;

    private String Qr_code;

    private String joindate;

    private String address;

    private String document_2;

    private String gender;

    private String expiry_date;

    private String mobile;

    private String batch;

    private String profilepic;

    private String dob;

    private String name;

    private String email;

    private String status;

    public String getProfilepic() {
        return profilepic;
    }

    public String getMember_id ()
    {
        return member_id;
    }

    public void setMember_id (String member_id)
    {
        this.member_id = member_id;
    }

    public String getDocument_1 ()
    {
        return document_1;
    }

    public void setDocument_1 (String document_1)
    {
        this.document_1 = document_1;
    }

    public String getQr_code ()
    {
        return Qr_code;
    }

    public void setQr_code (String Qr_code)
    {
        this.Qr_code = Qr_code;
    }

    public String getJoindate ()
    {
        return joindate;
    }

    public void setJoindate (String joindate)
    {
        this.joindate = joindate;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getDocument_2 ()
    {
        return document_2;
    }

    public void setDocument_2 (String document_2)
    {
        this.document_2 = document_2;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getExpiry_date ()
    {
        return expiry_date;
    }

    public void setExpiry_date (String expiry_date)
    {
        this.expiry_date = expiry_date;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getBatch ()
    {
        return batch;
    }

    public void setBatch (String batch)
    {
        this.batch = batch;
    }



    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member_id = "+member_id+", document_1 = "+document_1+", Qr_code = "+Qr_code+", joindate = "+joindate+", address = "+address+", document_2 = "+document_2+", gender = "+gender+", expiry_date = "+expiry_date+", mobile = "+mobile+", batch = "+batch+", profilepic = "+profilepic+", dob = "+dob+", name = "+name+", email = "+email+", status = "+status+"]";
    }*/
}



