package com.druva.project2021.pojo;

public class First_open_DAO {
    String plans;
    String batch;
    String newjoin;
    String lastday;
    String new_features;
    String versioncode;
    String version_name;
    String update;
    String discount;
    String subscription;
    String expirydate;
    String Status;

    public String getPlans() {
        return plans;
    }

    public void setPlans(String plans) {
        this.plans = plans;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getNewjoin() {
        return newjoin;
    }

    public void setNewjoin(String newjoin) {
        this.newjoin = newjoin;
    }

    public String getLastday() {
        return lastday;
    }

    public void setLastday(String lastday) {
        this.lastday = lastday;
    }

    public String getNew_features() {
        return new_features;
    }

    public void setNew_features(String new_features) {
        this.new_features = new_features;
    }

    public String getVersioncode() {
        return versioncode;
    }

    public void setVersioncode(String versioncode) {
        this.versioncode = versioncode;
    }

    public String getVersion_name() {
        return version_name;
    }

    public void setVersion_name(String version_name) {
        this.version_name = version_name;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "First_open_DAO{" +
                "plans='" + plans + '\'' +
                ", batch='" + batch + '\'' +
                ", newjoin='" + newjoin + '\'' +
                ", lastday='" + lastday + '\'' +
                ", new_features='" + new_features + '\'' +
                ", versioncode='" + versioncode + '\'' +
                ", version_name='" + version_name + '\'' +
                ", update='" + update + '\'' +
                ", discount='" + discount + '\'' +
                ", subscription='" + subscription + '\'' +
                ", expirydate='" + expirydate + '\'' +
                ", Status='" + Status + '\'' +
                '}';
    }
}
