package com.druva.project2021.pojo;

public class DashbaordDAO {

    private String birthday;

    private String expire_in_15;

    private String expired;

    private String blocked;

    private String expire_in_5;

    private String expire_in_10;

    private String lastday;

    private String Live;

    private String collection;

    private String unpaid;

    public String getBirthday ()
    {
        return birthday;
    }

    public void setBirthday (String birthday)
    {
        this.birthday = birthday;
    }

    public String getExpire_in_15 ()
    {
        return expire_in_15;
    }

    public void setExpire_in_15 (String expire_in_15)
    {
        this.expire_in_15 = expire_in_15;
    }

    public String getExpired ()
    {
        return expired;
    }

    public void setExpired (String expired)
    {
        this.expired = expired;
    }

    public String getBlocked ()
    {
        return blocked;
    }

    public void setBlocked (String blocked)
    {
        this.blocked = blocked;
    }

    public String getExpire_in_5 ()
    {
        return expire_in_5;
    }

    public void setExpire_in_5 (String expire_in_5)
    {
        this.expire_in_5 = expire_in_5;
    }

    public String getExpire_in_10 ()
    {
        return expire_in_10;
    }

    public void setExpire_in_10 (String expire_in_10)
    {
        this.expire_in_10 = expire_in_10;
    }

    public String getLastday ()
    {
        return lastday;
    }

    public void setLastday (String lastday)
    {
        this.lastday = lastday;
    }

    public String getLive ()
    {
        return Live;
    }

    public void setLive (String Live)
    {
        this.Live = Live;
    }

    public String getCollection ()
    {
        return collection;
    }

    public void setCollection (String collection)
    {
        this.collection = collection;
    }

    public String getUnpaid ()
    {
        return unpaid;
    }

    public void setUnpaid (String unpaid)
    {
        this.unpaid = unpaid;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [birthday = "+birthday+", expire_in_15 = "+expire_in_15+", expired = "+expired+", blocked = "+blocked+", expire_in_5 = "+expire_in_5+", expire_in_10 = "+expire_in_10+", lastday = "+lastday+", Live = "+Live+", collection = "+collection+", unpaid = "+unpaid+"]";
    }
}
