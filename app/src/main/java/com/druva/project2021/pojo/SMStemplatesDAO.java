package com.druva.project2021.pojo;

public class SMStemplatesDAO {
    private String sms_id;

    private String description;

    private String title;

    private String status;

    public String getSms_id ()
    {
        return sms_id;
    }

    public void setSms_id (String sms_id)
    {
        this.sms_id = sms_id;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sms_id = "+sms_id+", description = "+description+", title = "+title+", status = "+status+"]";
    }
}
