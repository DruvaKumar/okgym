package com.druva.project2021.pojo;

public class Subscription_plansDAO {
    String id;
    String duration;
    String price;
    String trail;
    String plan_name;
    String actual_price;
    String features;
    String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTrail() {
        return trail;
    }

    public void setTrail(String trail) {
        this.trail = trail;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getActual_price() {
        return actual_price;
    }

    public void setActual_price(String actual_price) {
        this.actual_price = actual_price;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "classpojo[" +
                "id='" + id + '\'' +
                ", duration='" + duration + '\'' +
                ", price='" + price + '\'' +
                ", trail='" + trail + '\'' +
                ", plan_name='" + plan_name + '\'' +
                ", actual_price='" + actual_price + '\'' +
                ", features='" + features + '\'' +
                ", status='" + status + '\'' +
                ']';
    }
}
