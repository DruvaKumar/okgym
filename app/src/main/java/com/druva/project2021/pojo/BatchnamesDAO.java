package com.druva.project2021.pojo;

public class BatchnamesDAO {
    private String batch_name;

    private String batch_limit;

    private String total_members;

    private String[] details;

    private String id;

    private String remainder;

    public String getBatch_name ()
    {
        return batch_name;
    }

    public void setBatch_name (String batch_name)
    {
        this.batch_name = batch_name;
    }

    public String getBatch_limit ()
    {
        return batch_limit;
    }

    public void setBatch_limit (String batch_limit)
    {
        this.batch_limit = batch_limit;
    }

    public String getTotal_members ()
    {
        return total_members;
    }

    public void setTotal_members (String total_members)
    {
        this.total_members = total_members;
    }

    public String[] getDetails ()
    {
        return details;
    }

    public void setDetails (String[] details)
    {
        this.details = details;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRemainder ()
    {
        return remainder;
    }

    public void setRemainder (String remainder)
    {
        this.remainder = remainder;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [batch_name = "+batch_name+", batch_limit = "+batch_limit+", total_members = "+total_members+", details = "+details+", id = "+id+", remainder = "+remainder+"]";
    }
}
