package com.druva.project2021.pojo;

public class FAQ {
    String question;
    String answer;
    String id;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FAQ[" +
                "question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", id='" + id +'\''+
                ']';
    }
}
