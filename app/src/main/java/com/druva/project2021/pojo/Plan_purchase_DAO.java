package com.druva.project2021.pojo;

public class Plan_purchase_DAO {
    String status;
    String plan_name;
    String start_date;
    String expiry_date;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    @Override
    public String toString() {
        return "Plan_purchase_DAO{" +
                "status='" + status + '\'' +
                ", plan_name='" + plan_name + '\'' +
                ", start_date='" + start_date + '\'' +
                ", expiry_date='" + expiry_date + '\'' +
                '}';
    }
}
