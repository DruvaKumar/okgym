package com.druva.project2021.pojo;

import java.io.Serializable;

public class BatchDAO implements Serializable {
    private String batch_name;

    private String batch_id;

    private String endtime;

    private String starttime;

    private String batch_limit;

    private String total_members;

    private String status;

    public String getTotal_members() {
        return total_members;
    }

    public void setTotal_members(String total_members) {
        this.total_members = total_members;
    }

    public String getBatch_limit() {
        return batch_limit;
    }

    public void setBatch_limit(String batch_limit) {
        this.batch_limit = batch_limit;
    }

    public String getBatch_name ()
    {
        return batch_name;
    }

    public void setBatch_name (String batch_name)
    {
        this.batch_name = batch_name;
    }

    public String getBatch_id ()
    {
        return batch_id;
    }

    public void setBatch_id (String batch_id)
    {
        this.batch_id = batch_id;
    }

    public String getEndtime ()
    {
        return endtime;
    }

    public void setEndtime (String endtime)
    {
        this.endtime = endtime;
    }

    public String getStarttime ()
    {
        return starttime;
    }

    public void setStarttime (String starttime)
    {
        this.starttime = starttime;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [batch_name = "+batch_name+", batch_id = "+batch_id+", endtime = "+endtime+", starttime = "+starttime+", batch_limit ="+batch_limit+",total_members = "+total_members+",status = "+status+"]";
    }
}
