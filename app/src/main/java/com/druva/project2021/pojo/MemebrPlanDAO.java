package com.druva.project2021.pojo;

public class MemebrPlanDAO {
    String plan;
    String plan_duration    ;
    String plan_start_date;
    String plan_expiry_date;
    String plan_details;
    String status;

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getPlan_duration() {
        return plan_duration;
    }

    public void setPlan_duration(String plan_duration) {
        this.plan_duration = plan_duration;
    }

    public String getPlan_start_date() {
        return plan_start_date;
    }

    public void setPlan_start_date(String plan_start_date) {
        this.plan_start_date = plan_start_date;
    }

    public String getPlan_expiry_date() {
        return plan_expiry_date;
    }

    public void setPlan_expiry_date(String plan_expiry_date) {
        this.plan_expiry_date = plan_expiry_date;
    }

    public String getPlan_details() {
        return plan_details;
    }

    public void setPlan_details(String plan_details) {
        this.plan_details = plan_details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [plan = "+plan+", plan_duration = "+plan_duration+", plan_start_date = "+plan_start_date+", plan_expiry_date = "+plan_expiry_date+", plan_details = "+plan_details+", status = "+status+"]";
    }
}
