package com.druva.project2021.pojo;

import java.io.Serializable;

public class EnquiryDAO implements Serializable {
    private String amount;

    private String address;

    private String mobile;

    private String enquiry_date;

    private String description;

    private String followup_date;

    private String member_name;

    private String enquiry_id;

    private String plan;

    private String email;

    private String status;

    private String newjoining;

    public String getNewjoining() {
        return newjoining;
    }

    public void setNewjoining(String newjoining) {
        this.newjoining = newjoining;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getEnquiry_date ()
    {
        return enquiry_date;
    }

    public void setEnquiry_date (String enquiry_date)
    {
        this.enquiry_date = enquiry_date;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getFollowup_date ()
    {
        return followup_date;
    }

    public void setFollowup_date (String followup_date)
    {
        this.followup_date = followup_date;
    }

    public String getMember_name ()
    {
        return member_name;
    }

    public void setMember_name (String member_name)
    {
        this.member_name = member_name;
    }

    public String getEnquiry_id ()
    {
        return enquiry_id;
    }

    public void setEnquiry_id (String enquiry_id)
    {
        this.enquiry_id = enquiry_id;
    }

    public String getPlan ()
    {
        return plan;
    }

    public void setPlan (String plan)
    {
        this.plan = plan;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", address = "+address+", mobile = "+mobile+", enquiry_date = "+enquiry_date+", description = "+description+", followup_date = "+followup_date+", member_name = "+member_name+", enquiry_id = "+enquiry_id+", plan = "+plan+", newjoining = "+newjoining+" ,email = "+email+", status = "+status+"]";
    }
}
