package com.druva.project2021.pojo;

import java.io.Serializable;

public class PlansDAO implements Serializable {
    private String duration;

    private String feese;

    private String plan_details;

    private String plan_name;

    private String plan_id;

    private String status;

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getFeese ()
    {
        return feese;
    }

    public void setFeese (String feese)
    {
        this.feese = feese;
    }

    public String getPlan_details ()
    {
        return plan_details;
    }

    public void setPlan_details (String plan_details)
    {
        this.plan_details = plan_details;
    }

    public String getPlan_name ()
    {
        return plan_name;
    }

    public void setPlan_name (String plan_name)
    {
        this.plan_name = plan_name;
    }

    public String getPlan_id ()
    {
        return plan_id;
    }

    public void setPlan_id (String plan_id)
    {
        this.plan_id = plan_id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [duration = "+duration+", feese = "+feese+", plan_details = "+plan_details+", plan_name = "+plan_name+", plan_id = "+plan_id+", status = "+status+"]";
    }
}
