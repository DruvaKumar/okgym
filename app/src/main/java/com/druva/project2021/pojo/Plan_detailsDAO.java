package com.druva.project2021.pojo;

public class Plan_detailsDAO {
    private String duration;

    private String plan_details;

    private String plan_name;

    private String feese;

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getPlan_details ()
    {
        return plan_details;
    }

    public void setPlan_details (String plan_details)
    {
        this.plan_details = plan_details;
    }

    public String getPlan_name ()
    {
        return plan_name;
    }

    public void setPlan_name (String plan_name)
    {
        this.plan_name = plan_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [duration = "+duration+", plan_details = "+plan_details+", plan_name = "+plan_name+" ,feese = "+feese+"]";
    }
}
