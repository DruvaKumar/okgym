package com.druva.project2021.pojo;

import java.io.Serializable;

public class Member_AttendanceDAO {
    private String member_id;

    private String date;

    private String check_out;

    private String check_in;

    private String member_name;

    private String status;

    public String getMember_id ()
    {
        return member_id;
    }

    public void setMember_id (String member_id)
    {
        this.member_id = member_id;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getCheck_out ()
    {
        return check_out;
    }

    public void setCheck_out (String check_out)
    {
        this.check_out = check_out;
    }

    public String getCheck_in ()
    {
        return check_in;
    }

    public void setCheck_in (String check_in)
    {
        this.check_in = check_in;
    }

    public String getMember_name ()
    {
        return member_name;
    }

    public void setMember_name (String member_name)
    {
        this.member_name = member_name;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member_id = "+member_id+", date = "+date+", check_out = "+check_out+", check_in = "+check_in+", member_name = "+member_name+", status = "+status+"]";
    }

}
