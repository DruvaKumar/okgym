package com.druva.project2021.pojo;

public class Subscription_Status_DAO {
    String status;
    String plan_name;
    String duration;
    String expiry_date;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    @Override
    public String toString() {
        return "Subscription_Status_DAO{" +
                "status='" + status + '\'' +
                ", plan_name='" + plan_name + '\'' +
                ", duration='" + duration + '\'' +
                ", expiry_date='" + expiry_date + '\'' +
                '}';
    }
}
