package com.druva.project2021.pojo;
/*
public class Plan_nameDAO
{
    private String plan_name;

    public String getPlan_name ()
    {
        return plan_name;
    }

    public void setPlan_name (String plan_name)
    {
        this.plan_name = plan_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [plan_name = "+plan_name+"]";
    }
}*/

public class Plan_nameDAO {
    private Details[] details;

    private String id;

    private String plan_name;

    public Details[] getDetails ()
    {
        return details;
    }

    public void setDetails (Details[] details)
    {
        this.details = details;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getPlan_name ()
    {
        return plan_name;
    }

    public void setPlan_name (String plan_name)
    {
        this.plan_name = plan_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [details = "+details+", id = "+id+", plan_name = "+plan_name+"]";
    }

    public class Details
    {
        private String plan_name;

        private String duration;

        private String feese;

        private String plan_details;

        public String getPlan_name() {
            return plan_name;
        }

        public void setPlan_name(String plan_name) {
            this.plan_name = plan_name;
        }

        public String getDuration ()
        {
            return duration;
        }

        public void setDuration (String duration)
        {
            this.duration = duration;
        }

        public String getFeese ()
        {
            return feese;
        }

        public void setFeese (String feese)
        {
            this.feese = feese;
        }

        public String getPlan_details ()
        {
            return plan_details;
        }

        public void setPlan_details (String plan_details)
        {
            this.plan_details = plan_details;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [plan_name = "+plan_name+", duration = "+duration+", feese = "+feese+", plan_details = "+plan_details+"]";
        }
    }

}

