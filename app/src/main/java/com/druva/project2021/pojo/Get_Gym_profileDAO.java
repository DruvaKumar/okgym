package com.druva.project2021.pojo;

public class Get_Gym_profileDAO {
    String gym_name;
    String address;
    String contact;
    String website;
    String email;
    String status;

    String mobile;
    String profile_image;




    @Override
    public String toString() {
        return "GymprofileDAO{" +
                "gym_name='" + gym_name + '\'' +
                ", address='" + address + '\'' +
                ", contact='" + contact + '\'' +
                ", website='" + website + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                ", mobile='" + mobile + '\'' +
                ", profile_image='" + profile_image + '\'' +
                '}';
    }

    public String getGym_name() {
        return gym_name;
    }

    public void setGym_name(String gym_name) {
        this.gym_name = gym_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }
}
