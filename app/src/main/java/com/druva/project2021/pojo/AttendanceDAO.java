package com.druva.project2021.pojo;

public class AttendanceDAO {
    private String dates;

    private Check_in_out[] check_in_out;

    public String getDates ()
    {
        return dates;
    }

    public void setDates (String dates)
    {
        this.dates = dates;
    }

    public Check_in_out[] getCheck_in_out ()
    {
        return check_in_out;
    }

    public void setCheck_in_out (Check_in_out[] check_in_out)
    {
        this.check_in_out = check_in_out;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [dates = "+dates+", check_in_out = "+check_in_out+"]";
    }

    public class Check_in_out
    {
        private String member_id;

        private String date;

        private String check_out;

        private String check_in;

        private String member_name;

        public String getMember_id ()
        {
            return member_id;
        }

        public void setMember_id (String member_id)
        {
            this.member_id = member_id;
        }

        public String getDate ()
        {
            return date;
        }

        public void setDate (String date)
        {
            this.date = date;
        }

        public String getCheck_out ()
        {
            return check_out;
        }

        public void setCheck_out (String check_out)
        {
            this.check_out = check_out;
        }

        public String getCheck_in ()
        {
            return check_in;
        }

        public void setCheck_in (String check_in)
        {
            this.check_in = check_in;
        }

        public String getMember_name ()
        {
            return member_name;
        }

        public void setMember_name (String member_name)
        {
            this.member_name = member_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [member_id = "+member_id+", date = "+date+", check_out = "+check_out+", check_in = "+check_in+", member_name = "+member_name+"]";
        }
    }
}
