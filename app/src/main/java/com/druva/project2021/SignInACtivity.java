package com.druva.project2021;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.druva.project2021.Account.GymProfileActivity;
import com.druva.project2021.Activity.CreateGymProfileActivity;
import com.druva.project2021.Activity.OTPActivity;

import com.druva.project2021.Activity.PhonenumberActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.PaymentGateway.Token_Res;
import com.druva.project2021.pojo.First_open_DAO;

import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;

public class SignInACtivity extends AppCompatActivity  implements GoogleApiClient.OnConnectionFailedListener {

    TextView tvTerms,tvPolicy;
    LinearLayout llGoogle,llPhone;
    Session session;
    private static final int PERMISSION_REQUEST_CODE = 200;
    PreferenceManager preferenceManager;
    int APP_VERSION;
    String updation="0";
    ProgressBar progressBar;
    RelativeLayout relativeLayout;

    SignInButton signInButton;
    private GoogleSignInClient googleSignInClient;
    private static final int RC_SIGN_IN = 1;
    private GoogleApiClient googleApiClient;

    private GoogleSignInOptions gso;
    String g_name,g_email,g_profile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_signin);

        preferenceManager=new PreferenceManager(getApplicationContext());

        session=new Session(this);

        APP_VERSION=BuildConfig.VERSION_CODE;

        tvTerms=findViewById(R.id.tv_terms);
        tvPolicy=findViewById(R.id.tv_policy);
       // llGoogle=findViewById(R.id.ll_google);
        llPhone=findViewById(R.id.ll_phone);
        progressBar=findViewById(R.id.progress);
        relativeLayout=findViewById(R.id.llrelative);


        if (!checkPermission()) {

            requestPermission();

        } else {

           // Snackbar.make(view, "Permission already granted.", Snackbar.LENGTH_LONG).show();

        }

        if(session.logggedin()){
                Get_gym_details();
        }

        else {
            progressBar.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
        }

        GoogleSignInOptions gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient= GoogleSignIn.getClient(this,gso);

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,  this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        signInButton=(SignInButton)findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = googleSignInClient.getSignInIntent();
                startActivityForResult(intent,RC_SIGN_IN);
            }
        });

        llPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(SignInACtivity.this, PhonenumberActivity.class);
                startActivity(intent);
            }
        });


    }

    private void Get_gym_details() {

        HashMap<String, Object> listDetails = new HashMap<>();
       listDetails.put("userid",preferenceManager.getUserid(Config.USERID));


        Call<First_open_DAO> call = RetrofitInstance.getInstance().getMyApi().getgymdetails(listDetails);
        call.enqueue(new Callback<First_open_DAO>() {

            @Override
            public void onResponse(Call<First_open_DAO> call, Response<First_open_DAO> response) {
                Log.e("TAG", " respo "+ response.isSuccessful() );
                PublicHelper.dismissProgressDialog();

                First_open_DAO first_open_dao=response.body();
                String status=first_open_dao.getStatus();
               if(status.equalsIgnoreCase("notfound")) {
                   progressBar.setVisibility(View.GONE);
                   relativeLayout.setVisibility(View.VISIBLE);
                   session.setLoggedin(false);
                   session.setprofile(false);
                   session.setbatchadded(false);
                   session.setplanadded(false);
                  signout_from_google();
               }
               else if(status.equalsIgnoreCase("Success")) {

                   String plans = first_open_dao.getPlans();
                   String batch = first_open_dao.getBatch();
                   String newjoin = first_open_dao.getNewjoin();
                   String lastday = first_open_dao.getLastday();
                   String new_features = first_open_dao.getNew_features();
                   String version = first_open_dao.getVersioncode();
                   String version_name = first_open_dao.getVersion_name();
                   String update = first_open_dao.getUpdate();
                   String discount = first_open_dao.getDiscount();
                   String subscription = first_open_dao.getSubscription();
                   String expirydate = first_open_dao.getExpirydate();

                   if (subscription == null) {

                   } else {
                       preferenceManager.setExpirydate(expirydate);
                       preferenceManager.setSubscription(subscription);
                   }
                   preferenceManager.setDiscount(discount);
                   preferenceManager.setNewjoin(newjoin);
                   preferenceManager.setNewFeatures(new_features);
                   preferenceManager.setVersionName(version_name);

                   if (!plans.equalsIgnoreCase("0")) {
                       session.setplanadded(true);

                   }
                   if (!batch.equalsIgnoreCase("0")) {
                       session.setbatchadded(true);
                   }
                   int api_version = Integer.parseInt(version);

                   if (update.equalsIgnoreCase("1") && APP_VERSION < api_version) {
                       //  Toast.makeText(SignInACtivity.this,"Need to update application from Playstore",Toast.LENGTH_LONG).show();
                       updation = "1";
                   } else if (!session.isprofile()) {
                       Intent intent = new Intent(getApplicationContext(), CreateGymProfileActivity.class);
                       startActivity(intent);
                       finish();
                   } else {
                       Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
                       intent.putExtra("updation", updation);
                       startActivity(intent);
                       finish();
                   }
               }

                else {

                    progressBar.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.VISIBLE);
                    session.setLoggedin(false);
                    session.setprofile(false);
                    session.setbatchadded(false);
                    session.setplanadded(false);

                }

            }

            @Override
            public void onFailure(Call<First_open_DAO> call, Throwable t) {
                PublicHelper.dismissProgressDialog();
                // Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();
                Log.e("TAG", " response error "+t.toString());

            }
        });
    }

    private void signout_from_google() {
        googleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean storage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (storage && cameraAccepted) {
                    }
                       // Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    else{


                        //Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{READ_EXTERNAL_STORAGE, CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(SignInACtivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

   /* @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account=GoogleSignIn.getLastSignedInAccount(this);
       // Toast.makeText(getApplicationContext(),"already Signed in",Toast.LENGTH_LONG).show();



        OptionalPendingResult<GoogleSignInResult> opr= Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult result=opr.get();
            handleSignInResult(result);
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }*/

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("TAG","error"+connectionResult);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
           // Task<GoogleSignInAccount> task =GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(result);
        }
    }

   /* private void handleSignInResult(Task<GoogleSignInAccount>completedTask) {
        Toast.makeText(getApplicationContext(),"Sign in Success",Toast.LENGTH_LONG).show();
        GoogleSignInAccount account= null;
        try {
            account = completedTask.getResult(ApiException.class);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        *//*String name=account.getDisplayName();
        String userEmail=account.getEmail();
        String userId=account.getId();*//*
    }*/
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
           // gotoProfile();
            Toast.makeText(getApplicationContext(),"Sign in Success",Toast.LENGTH_LONG).show();

            GoogleSignInAccount account=result.getSignInAccount();
             g_name=account.getDisplayName();
             g_email=account.getEmail();
            // userId=account.getId();
            // g_profile= String.valueOf(account.getPhotoUrl());

          /*  Log.v("TAG","uesr name "+name);
            Log.v("TAG","userEmail "+userEmail);
            Log.v("TAG","userId "+userId);*/

            sign_in_with_google(g_name,g_email);



        }else{
            Toast.makeText(getApplicationContext(),"Sign in cancel",Toast.LENGTH_LONG).show();
        }
    }

    private void sign_in_with_google(String name, String userEmail) {

       /* PublicHelper.showProgressDialog(SignInACtivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("name", name);
        listDetails.put("email", userEmail);


        Call<Void> call = RetrofitInstance.getInstance().getMyApi().signin_with_google(listDetails);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                String Response= String.valueOf(response.body());


                if(Response.equalsIgnoreCase("null")) {


                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(Response));

                        String status = jsonObject.getString("status");

                        if (status.equalsIgnoreCase("Success")) {

                            String USERID = jsonObject.getString("user_id");
                            String message = jsonObject.getString("message");
                            String mobile = jsonObject.getString("mobile");

                            preferenceManager.setUserid(USERID);
                            preferenceManager.setMobile(mobile);

                            String name = jsonObject.getString("gym_name");
                            if (!name.equalsIgnoreCase("0")) {
                                String contact = jsonObject.getString("contact");
                                String email = jsonObject.getString("email");
                                String address = jsonObject.getString("address");
                                String website = jsonObject.getString("website");

                                preferenceManager.setGymName(name);
                                preferenceManager.setGymEmail(email);
                                preferenceManager.setGymContact(contact);
                                preferenceManager.setGymAddress(address);
                                preferenceManager.setGymWebsite(website);
                            }


                            if (message.equalsIgnoreCase("new_user") || message.equalsIgnoreCase("not_verified")) {
                                Intent intent = new Intent(SignInACtivity.this, OTPActivity.class);
                                startActivity(intent);
                            } else if (!name.equalsIgnoreCase("0")) {
                                Intent intent = new Intent(SignInACtivity.this, BottomNavigation.class);
                                session.setLoggedin(true);
                                session.setprofile(true);
                                startActivity(intent);
                                finish();
                            } else if (message.equalsIgnoreCase("old_user")) {
                                Intent intent = new Intent(SignInACtivity.this, CreateGymProfileActivity.class);
                                session.setLoggedin(true);
                                startActivity(intent);
                                finish();
                            } else {

                            }


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    PublicHelper.dismissProgressDialog();
                }




            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

            }
        });*/


        JSONObject postdetails = new JSONObject();
        try {

            postdetails.put("email",userEmail);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (postdetails.length() > 0) {
            new getJson().execute(String.valueOf(postdetails));
        }
    }

    class getJson extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PublicHelper.showProgressDialog(SignInACtivity.this,"");
        }

        @Override
        protected String doInBackground(String... params) {


            String JsonResponse;
            String JsonDATA = params[0];


            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(Config.BASE_URL+"google_signin.php");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);

                urlConnection.setRequestMethod(Config.METHOD_TYPE_POST);
                urlConnection.setRequestProperty(Config.CONTENT_TYPE, Config.CONTENT_JSON);
                urlConnection.setRequestProperty(Config.ACCEPT, Config.CONTENT_JSON);

                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                writer.write(JsonDATA);

                writer.close();

                InputStream inputStream = urlConnection.getInputStream();

                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputline;
                while ((inputline = reader.readLine()) != null)
                    buffer.append(inputline).append("\n");
                if (buffer.length() == 0) {
                    return null;
                }
                JsonResponse = buffer.toString();
                //Log.v("("otp response", JsonResponse);

                return JsonResponse;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        //Log.v("("TAG", "Error closing stream", e);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String JsonResponse) {
            super.onPostExecute(JsonResponse);
            if (JsonResponse == null) {
                Toast.makeText(getApplicationContext(),"Error ",Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

            }
            else if(JsonResponse.contains("Success")){
                PublicHelper.dismissProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(JsonResponse);

                    String USERID=jsonObject.getString("user_id");
                    String message=jsonObject.getString("message");
                    String mobile=jsonObject.getString("mobile");

                    preferenceManager.setUserid(USERID);
                    preferenceManager.setMobile(mobile);

                    String name=jsonObject.getString("gym_name");
                    if(!name.equalsIgnoreCase("0")) {
                        String contact = jsonObject.getString("contact");
                        String email = jsonObject.getString("email");
                        String address = jsonObject.getString("address");
                        String website = jsonObject.getString("website");

                        preferenceManager.setGymName(name);
                        preferenceManager.setGymEmail(email);
                        preferenceManager.setGymContact(contact);
                        preferenceManager.setGymAddress(address);
                        preferenceManager.setGymWebsite(website);
                    }

                    Date date = new Date();
                    SimpleDateFormat df  = new SimpleDateFormat("YYYY-MM-dd");
                    Calendar c1 = Calendar.getInstance();
                    String currentDate = df.format(date);// get current date here

                    // now add 30 day in Calendar instance
                    c1.add(Calendar.MONTH, 1);
                    df = new SimpleDateFormat("yyyy-MM-dd");
                    Date resultDate = c1.getTime();
                    String dueDate = df.format(resultDate);

                    preferenceManager.setTrialExpiry(dueDate);

                   /* if(message.equalsIgnoreCase("new_user")||message.equalsIgnoreCase("not_verified"))
                    {
                        Intent intent=new Intent(SignInACtivity.this,OTPActivity.class);
                        startActivity(intent);
                    }
                    else */

                    if(name.equalsIgnoreCase("0") ||message.equalsIgnoreCase("new_user"))
                    {
                        Intent intent=new Intent(SignInACtivity.this,CreateGymProfileActivity.class);
                        intent.putExtra("from", "google");
                        intent.putExtra("g_email", g_email);
                        session.setLoggedin(true);
                        startActivity(intent);
                        finish();

                    }
                    else  {

                        Intent intent=new Intent(SignInACtivity.this, BottomNavigation.class);
                        session.setLoggedin(true);
                        session.setprofile(true);
                        startActivity(intent);
                        finish();
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else {
                PublicHelper.dismissProgressDialog();
                Toast.makeText(getApplicationContext(),""+JsonResponse,Toast.LENGTH_SHORT).show();
            }

        }

    }
        
        


    private void gotoProfile() {

        gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

      /*  googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();*/

    }

}
