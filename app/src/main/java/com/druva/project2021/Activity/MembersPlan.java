package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.druva.project2021.pojo.DashbaordDAO;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.pojo.MemebrPlanDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembersPlan extends AppCompatActivity {

    TextView planname,startdate,enddate,duration,details;
    Button btn_addnewplan;
    ImageView back;
    TextView header;
    String memberid;

    MembersOverallDAO MembersOverallDAO;
    PreferenceManager preferenceManager;
    CardView cardView;
    TextView noplans;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_members_plan);

        preferenceManager=new PreferenceManager(getApplicationContext());
        session=new Session(this);

        Intent i=getIntent();
        Bundle b=i.getExtras();
        MembersOverallDAO= (MembersOverallDAO) b.getSerializable("user");

      // memberid=b.getString("memberid");
        memberid=preferenceManager.getMemberId("memberid");


        header=findViewById(R.id.header);
        back=findViewById(R.id.btn_back);
        btn_addnewplan=findViewById(R.id.btn_newplan);
        cardView=findViewById(R.id.cardplans);
        noplans=findViewById(R.id.noplans);

        planname=findViewById(R.id.name);
        startdate=findViewById(R.id.stratdate);
        enddate=findViewById(R.id.expirydate);
        duration=findViewById(R.id.duration);
        details=findViewById(R.id.details);

        header.setText("Member Plans");

        getmeberplan(memberid);

       /* Intent i=getIntent();
        Bundle b=i.getExtras();
        MembersOverallDAO= (MembersOverallDAO) b.getSerializable("user");
        if(b!=null) {

            planname.setText(MembersOverallDAO.getPlan());
            startdate.setText(MembersOverallDAO.getPlan_start_date());
            enddate.setText(MembersOverallDAO.getPlan_expiry_date());
            duration.setText(MembersOverallDAO.getPlan_duration());
            details.setText(MembersOverallDAO.getPlan_details());
        }
        else {
            Intent intent1=getIntent();
            String fromplan=intent1.getStringExtra("from");
            if(fromplan!=null) {
                String endd=intent1.getStringExtra("startdate");
                //endd=endd.replace("months","");
                int endingdate=endd.charAt(0);

                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");

                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                c.add(Calendar.MONTH, +endingdate);

                Date d = c.getTime();
                String res = format.format(d);

                planname.setText(intent1.getStringExtra("planname"));
                startdate.setText(intent1.getStringExtra("startdate"));
                enddate.setText(res);
                duration.setText(intent1.getStringExtra("duration"));
                details.setText(intent1.getStringExtra("details"));
            }

        }*/


        btn_addnewplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
/*
                Intent intent1 = new Intent(MembersPlan.this, Add_plan_to_member.class);
                //  intent1.putExtra("memberid",memberid);

                Bundle b = new Bundle();
                b.putSerializable("user", MembersOverallDAO);
                intent1.putExtras(b);

                startActivity(intent1);
                finish();*/

                if(session.isplanadded()) {
                    Intent intent1 = new Intent(MembersPlan.this, Add_plan_to_member.class);
                    //  intent1.putExtra("memberid",memberid);

                    Bundle b = new Bundle();
                    b.putSerializable("user", MembersOverallDAO);
                    intent1.putExtras(b);

                    startActivity(intent1);
                    finish();
                }
                else  {
                    Toast.makeText(MembersPlan.this,"Please add a Plan to continue ",Toast.LENGTH_LONG).show();

                }
            }
        });


    }

    private void getmeberplan(String member_id) {
        PublicHelper.showProgressDialog(MembersPlan.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("memberid", member_id);

        Call<MemebrPlanDAO> call = RetrofitInstance.getInstance().getMyApi().get_member_plan(listDetails);
        call.enqueue(new Callback<MemebrPlanDAO>() {

            @Override
            public void onResponse(Call<MemebrPlanDAO> call, Response<MemebrPlanDAO> response) {

                MemebrPlanDAO MemebrPlanDAO=response.body();

                if(MemebrPlanDAO.getPlan().equalsIgnoreCase("0")){
                    noplans.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
                else {
                    planname.setText(MemebrPlanDAO.getPlan());
                    startdate.setText(MemebrPlanDAO.getPlan_start_date());
                    enddate.setText(MemebrPlanDAO.getPlan_expiry_date());
                    duration.setText(MemebrPlanDAO.getPlan_duration());
                    details.setText(MemebrPlanDAO.getPlan_details());

                    cardView.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<MemebrPlanDAO> call, Throwable t) {
                PublicHelper.dismissProgressDialog();
               // Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();


            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* startActivity(new Intent(MembersPlan.this,Add_plan_to_member.class));
        finish();*/
    }
}