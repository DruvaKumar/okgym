package com.druva.project2021.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.Scannned_attendnce_DAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScannerActivity extends AppCompatActivity implements View.OnClickListener {

    Button scanBtn,scanagain,done;
    TextView messageText, messageFormat,status,name,message,header;
    PreferenceManager preferenceManager;
    ImageView back;
    LinearLayout attendanceStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_scanner);

        preferenceManager=new PreferenceManager(getApplicationContext());

        scanBtn = findViewById(R.id.scanBtn);
        header = findViewById(R.id.header);
        back = findViewById(R.id.btn_back);

        // adding listener to the button
        status=findViewById(R.id.status);
        name=findViewById(R.id.name);
        message=findViewById(R.id.message);

        scanagain=findViewById(R.id.scanagain);
        done=findViewById(R.id.done);
        attendanceStatus=findViewById(R.id.llattendance);

        header.setText("Scan & Capture attendance");

        scanBtn.setOnClickListener((View.OnClickListener) this);
    }


    @Override
    public void onClick(View v) {
        // we need to create the object
        // of IntentIntegrator class
        // which is the class of QR library
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setPrompt("Scan a barcode or QR Code");
        intentIntegrator.setOrientationLocked(true);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        // if the intentResult is null then
        // toast a message as "cancelled"
        if (intentResult != null) {
            if (intentResult.getContents() == null) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                // if the intentResult is not null we'll set
                // the content and format of scan message
               // messageText.setText(intentResult.getContents());
               // messageFormat.setText(intentResult.getFormatName());
                String scanned_member_id=intentResult.getContents();
                Add_attendnce(scanned_member_id);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void Add_attendnce(String memberid) {

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", memberid);

        Call<Scannned_attendnce_DAO> call = RetrofitInstance.getInstance().getMyApi().scanned_attendance(listDetails);
        call.enqueue(new Callback<Scannned_attendnce_DAO>() {

            @Override
            public void onResponse(Call<Scannned_attendnce_DAO> call, Response<Scannned_attendnce_DAO> response) {
                Log.v("TAG", response.toString());

                Scannned_attendnce_DAO scannned_attendnce_dao=(Scannned_attendnce_DAO)response.body();

               // new Gson().toJson(scannned_attendnce_dao);
                try {
                    JSONObject jsonObject=new JSONObject(new Gson().toJson(scannned_attendnce_dao));
                    String Status=jsonObject.getString("status");
                    String messag=jsonObject.getString("message");
                    String membername=jsonObject.getString("name");
                    String time=jsonObject.getString("time");

                    if(Status.equalsIgnoreCase("Success")){
                        status.setTextColor(getResources().getColor(R.color.c1));
                    }
                    else {
                        status.setTextColor(getResources().getColor(R.color.c9));
                    }

                    status.setText(Status);
                    message.setText(messag+"@ "+time);
                    name.setText(membername);
                    attendanceStatus.setVisibility(View.VISIBLE);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Scannned_attendnce_DAO> call, Throwable t) {
              //  Toast.makeText(ScannerActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


}