package com.druva.project2021.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterGymAttendance;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.Member_AttendanceDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendancActivity extends AppCompatActivity {
    TextView header,date,nodata;
    ImageView imgback,imhprevious,imgnext,imgedit;
    DatePickerDialog datePickerDialog;
    String currentdate,today,nextdate,previousdate;
    Calendar c;
    DateFormat df;
    int dayshift=0;
    PreferenceManager preferenceManager;
    RecyclerView rvattendance;

    ArrayList<Member_AttendanceDAO> membersattedancelist=new ArrayList<>();
    AdapterGymAttendance adapterAttendance;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_attendanc);
        
        preferenceManager=new PreferenceManager(getApplicationContext());

      c = Calendar.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            df=new SimpleDateFormat("yyyy-MM-dd");

        }
        header=findViewById(R.id.header);
        imgback=findViewById(R.id.btn_back);
        date=findViewById(R.id.tvdate);
        imgedit=findViewById(R.id.img_edit);
        imhprevious=findViewById(R.id.imgprevious);
        imgnext=findViewById(R.id.imgnext);
        imgedit=findViewById(R.id.imgpedit);
        header.setText(getResources().getString(R.string.app_name));
        rvattendance=findViewById(R.id.rvattenance);
        nodata=findViewById(R.id.nodata);
       // date.setText(attendancedate());
        //attendancedate();

         getcurrentdate();





        imgnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nectdate();


            }
        });
        imhprevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previousdate();

            }
        });

        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month

                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

                c.get(Calendar.DAY_OF_YEAR);
                // date picker dialog
                datePickerDialog = new DatePickerDialog(AttendancActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                Date date1 = null;
                                try {
                                    date1 = new SimpleDateFormat("dd-mm-yyyy").parse(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String mjoindate = new SimpleDateFormat("yyyy-mm-dd").format(date1);
                                date.setText(mjoindate);
                                getattendance(preferenceManager.getUserid(Config.USERID),date.getText().toString().trim());


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        rvattendance.setLayoutManager(new LinearLayoutManager(AttendancActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getattendance(preferenceManager.getUserid(Config.USERID),date.getText().toString().trim());
        rvattendance.setItemAnimator(new DefaultItemAnimator());


    }

    

    private void getcurrentdate() {
        Calendar cal=Calendar.getInstance();

        dayshift=0;
        if(dayshift!=0)
        {
            cal.add(Calendar.DAY_OF_YEAR,dayshift);

        }

        date.setText(df.format(cal.getTime()));
    }

    private String attendancedate() {

        // calender class's instance and get current date , month and year from calender

        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        SimpleDateFormat monthdate=new SimpleDateFormat("M");
        String monthName=monthdate.format(c.getTime());

        currentdate=mDay + " "
                + (monthName) + " " + mYear;
        return currentdate;
    }

    private void nectdate(){


        Calendar cal=Calendar.getInstance();
        dayshift=dayshift+1;
        if(dayshift!=0)
        {
            cal.add(Calendar.DAY_OF_YEAR,dayshift);
        }

        date.setText(df.format(cal.getTime()));
        getattendance(preferenceManager.getUserid(Config.USERID),date.getText().toString().trim());
    }

    private void previousdate(){

        dayshift=dayshift-1;
        Calendar cal=Calendar.getInstance();
        if(dayshift!=0)
        {
            cal.add(Calendar.DAY_OF_YEAR,dayshift);
        }

        date.setText(df.format(cal.getTime()));

        getattendance(preferenceManager.getUserid(Config.USERID),date.getText().toString().trim());
    }

    private void getattendance(String userid, String today) {
        PublicHelper.showProgressDialog(AttendancActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", userid);
        listDetails.put("todaysdate", today);


        Call<ArrayList<Member_AttendanceDAO>> call = RetrofitInstance.getInstance().getMyApi().get_gym_attendance(listDetails);
        call.enqueue(new Callback<ArrayList<Member_AttendanceDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<Member_AttendanceDAO>> call, Response<ArrayList<Member_AttendanceDAO>> response) {
                if (membersattedancelist.size() > 0) {
                    membersattedancelist.clear();
                }
                Log.v("TAG","attendance "+response.body());

                membersattedancelist = (ArrayList<Member_AttendanceDAO>) response.body();
                adapterAttendance = new AdapterGymAttendance(membersattedancelist, AttendancActivity.this);
                rvattendance.setAdapter(adapterAttendance);
                if(membersattedancelist.size() > 0) {
                    rvattendance.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
                else {
                    PublicHelper.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Member_AttendanceDAO>> call, Throwable t) {
                nodata.setVisibility(View.VISIBLE);
                PublicHelper.dismissProgressDialog();
              //  Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();


            }
        });
    }

    }
