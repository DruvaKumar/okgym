package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.pojo.Plan_detailsDAO;
import com.druva.project2021.pojo.Plan_nameDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEnquiryActivity extends AppCompatActivity {
    TextInputEditText etname,etaddress,etemail,etamount,etdescriptpion,etmobile;
    TextView tvenquirydate,tvplan,tvfollowupdate,header,save;
    ImageView imgenquirydate,imgfollowupdate,imgback;
    Spinner spinner;
    DatePickerDialog datePickerDialog;
    String[]planarray={"Select Plan","plan1","plan2","Batch3","Batch4"};
    String ename,enumber,eaddress,edate="",eplan,eamount,efolloupdate="",edescription,eemail;
    PreferenceManager preferenceManager;
    String selected;

    EnquiryDAO enquiryDAO;
    ArrayList<Plan_nameDAO> planslist=new ArrayList<>();
    ArrayList<Plan_detailsDAO> plandetails=new ArrayList<>();
    ArrayList<String>PlansArray=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_enquiry);
        preferenceManager=new PreferenceManager(getApplicationContext());

        etname=findViewById(R.id.et_name);
        etmobile=findViewById(R.id.et_mem_mobile);
        etemail=findViewById(R.id.et_mem_email);
        etaddress=findViewById(R.id.et_mem_address);
        etamount=findViewById(R.id.et_mem_amount);
        etdescriptpion=findViewById(R.id.et_description);

        tvenquirydate=findViewById(R.id.tv_enquirydate);
        tvplan=findViewById(R.id.tv_selcct_plan);
        tvfollowupdate=findViewById(R.id.tv_folloupdate);
        imgenquirydate=findViewById(R.id.imgenquirydate);
        imgfollowupdate=findViewById(R.id.imgfollowup);
        spinner=findViewById(R.id.imgplandropdown);

        header=findViewById(R.id.header);
        imgback=findViewById(R.id.btn_back);
        save=findViewById(R.id.tv_save);
        header.setText("Add Member Enquiry");

        save.setVisibility(View.VISIBLE);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivity(new Intent(getApplicationContext(),EnquiryActivity.class));
                finish();*/
                Add_enery_members_to_server();
            }
        });

       // final String[] enquiredplan = new String[1];




        imgenquirydate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(AddEnquiryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                               /* tvenquirydate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);*/
                                Date date1 = null;
                                try {
                                    date1 = new SimpleDateFormat("dd-mm-yyyy").parse(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                edate = new SimpleDateFormat("yyyy-mm-dd").format(date1);
                                tvenquirydate.setText(edate);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }

        });


        imgfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(AddEnquiryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                               /* joindate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);*/
                                Date date2 = null;
                                try {
                                    date2 = new SimpleDateFormat("dd-mm-yyyy").parse(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                efolloupdate = new SimpleDateFormat("yyyy-mm-dd").format(date2);
                                tvfollowupdate.setText(efolloupdate);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

              /*  // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(AddEnquiryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                              *//*  tvfollowupdate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);*//*

                                Date date1 = null;
                                try {
                                    date1 = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth+"-"+monthOfYear+1+"-"+year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                efolloupdate = new SimpleDateFormat("dd MMMM yyyy").format(date1);
                                tvfollowupdate.setText(efolloupdate);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();*/
            }

        });
        getallplans();

      /*  ArrayAdapter adapter=new ArrayAdapter(this, android.R.layout.simple_spinner_item,planarray);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(AddmemberActivity.this,batcharray[i],Toast.LENGTH_SHORT).show();
                eplan =planarray[i].toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(AddmemberActivity.this,batcharray[i],Toast.LENGTH_SHORT).show();
                String selectedCode=PlansArray.get(i);
                String selected = adapterView.getItemAtPosition(i).toString().trim();
                getplandetails(selected);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void getplandetails(String selected) {

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
        listDetails.put("planname", selected);

        Call<ArrayList<Plan_detailsDAO>> call = RetrofitInstance.getInstance().getMyApi().get_plan_details(listDetails);
        call.enqueue(new Callback<ArrayList<Plan_detailsDAO>>() {
            @Override
            public void onResponse(Call<ArrayList<Plan_detailsDAO>> call, Response<ArrayList<Plan_detailsDAO>> response) {

                // Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();

                plandetails=(ArrayList<Plan_detailsDAO>)response.body();
                String mmm= new Gson().toJson(plandetails);
                try {
                    JSONArray jsonArray=new JSONArray(mmm);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);

                        etamount.setText(jsonObject.getString("feese"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<Plan_detailsDAO>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
        });
    }

    private void getallplans() {
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));

        Call<ArrayList<Plan_nameDAO>> call = RetrofitInstance.getInstance().getMyApi().get_plan_array(listDetails);
        call.enqueue(new Callback<ArrayList<Plan_nameDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<Plan_nameDAO>> call, Response<ArrayList<Plan_nameDAO>> response) {
                if (planslist.size() > 0) {
                    planslist.clear();
                }
                planslist = (ArrayList<Plan_nameDAO>) response.body();


                String mmm= new Gson().toJson(planslist);
                try {
                    JSONArray jsonArray=new JSONArray(mmm);
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        String plannamess=jsonObject.getString("plan_name");
                        Log.v("TAG","plannamess "+plannamess);


                        PlansArray.add(plannamess);
                        spinner.setAdapter(new ArrayAdapter<String>(AddEnquiryActivity.this, android.R.layout.simple_spinner_dropdown_item,PlansArray));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<Plan_nameDAO>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();


            }
        });

    }


    private void Add_enery_members_to_server() {{
        ename = etname.getEditableText().toString().trim();
        enumber = etmobile.getEditableText().toString().trim();
        eaddress = etaddress.getEditableText().toString().trim();
        eamount = etamount.getEditableText().toString().trim();
        edescription= etdescriptpion.getEditableText().toString().trim();
        eemail=etemail.getEditableText().toString().trim();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (ename.isEmpty() ) {
            etname.setError("Please enter Name");
        }
        else if(ename.length()<3){
            etname.setError("Please add a valid name");
        }
        else if (enumber.isEmpty() ) {
            etmobile.setError("Please enter Mobile number");
        }
        else if (!enumber.isEmpty() && enumber.length()!=10 ) {
            etmobile.setError("Please enter 10 digit Mobile number");
        }
        else if (!eemail.matches(emailPattern) ){
            etemail.setError("Please enter valid Email Id");
        }
        else if(eaddress.isEmpty()){
            etaddress.setError("Please enter address");
        }
        else if (edate.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Please choose enquiry date", Toast.LENGTH_SHORT).show();
            //joindate.setError("Please select join date");
        }


        else if (eamount.isEmpty() ) {
            etamount.setError("Please enter amount");
        }
        else if (efolloupdate.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Please choose followup date", Toast.LENGTH_SHORT).show();
            //tvfollowupdate.setError("Please choose followup date");
        }
        else if (edescription.equalsIgnoreCase("")) {
            etdescriptpion.setError("Please enter details");
        }
        else if (PublicHelper.isConnectedToInternet(getApplicationContext())) {
            PublicHelper.showProgressDialog(AddEnquiryActivity.this,"");

            HashMap<String, Object> listDetails = new HashMap<>();
            listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
            listDetails.put("member_name", ename);
            listDetails.put("mobile", enumber);
            listDetails.put("email", eemail);
            listDetails.put("address", eaddress);
            listDetails.put("enquiry_date", edate);
            listDetails.put("plan", selected);
            listDetails.put("amount", eamount);
            listDetails.put("followup_date", efolloupdate);
            listDetails.put("description", edescription);

            Call<Object> call = RetrofitInstance.getInstance().getMyApi().addenquiry(listDetails);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddEnquiryActivity.this,EnquiryActivity.class));
                    finish();
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                    PublicHelper.dismissProgressDialog();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

    }

    }
}