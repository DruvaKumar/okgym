package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.pojo.Plan_detailsDAO;
import com.druva.project2021.pojo.Plan_nameDAO;
import com.druva.project2021.pojo.Plan_purchase_DAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.gkemon.XMLtoPDF.PdfGenerator;
import com.gkemon.XMLtoPDF.PdfGeneratorListener;
import com.gkemon.XMLtoPDF.model.FailureResponse;
import com.gkemon.XMLtoPDF.model.SuccessResponse;
import com.google.android.material.textfield.TextInputEditText;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_plan_to_member extends AppCompatActivity {
    TextInputEditText duration, details, fees, admissiofee, discount, tax, paidamount;
    Spinner plans, mode;
    TextView startdate, header, joindate, save;
    ImageView caleder, back;
    DatePickerDialog datePickerDialog;
    String memberid, st_startdate = "", plan, plan_duration, plan_details, plan_fee, plan_admissiofee, plan_discount, plan_tax, plan_paidamount, plan_mode;
    PreferenceManager preferenceManager;
    ArrayList<Plan_nameDAO> planslist = new ArrayList<>();
    ArrayList<Plan_detailsDAO> plandetails = new ArrayList<>();

    // String[] Plans = {"Plan 1","Plan 2","Plan 3","Plan 4"};

    String[] modes = {"Cash", "Cheque", "Card Payment", "Online Payment"};
    MembersOverallDAO MembersOverallDAO;

    ArrayList<String> PlansArray = new ArrayList<>();
    JSONArray detailarray;
    String slectedplanname;
    String selected = "", selectedmode;
    String unpaidamount;
    private View mRootView;


    String expiry;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_plan_to_member);

        preferenceManager = new PreferenceManager(getApplicationContext());

        //  memberid=getIntent().getStringExtra("memberid");
        Intent i = getIntent();
        Bundle b = i.getExtras();
        MembersOverallDAO = (MembersOverallDAO) b.getSerializable("user");

        getallplans();



        memberid = MembersOverallDAO.getMember_id();

        plans = findViewById(R.id.spinner);
        duration = findViewById(R.id.plan_duration);
        joindate = findViewById(R.id.tvjoindate);
        details = findViewById(R.id.plandeatil);
        fees = findViewById(R.id.et_planfees);
        admissiofee = findViewById(R.id.admissionfee);
        discount = findViewById(R.id.plandiscount);
        tax = findViewById(R.id.plantax);
        paidamount = findViewById(R.id.et_paid);
        mode = findViewById(R.id.spinnermode);
        startdate = findViewById(R.id.et_start);
        header = findViewById(R.id.header);
        caleder = findViewById(R.id.img_joindate);
        back = findViewById(R.id.btn_back);
        save = findViewById(R.id.tv_save);

        header.setText("Add New Plans");
        save.setVisibility(View.VISIBLE);


      /*  ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, PlanNames);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        plans.setAdapter(adapter);*/

      /*  ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Plans);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        plans.setAdapter(adapter);

        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, modes);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        mode.setAdapter(adapter2);*/

      /* plans.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View view, MotionEvent motionEvent) {
               plans.setPadding(0,20,0,20);
               plans.showDropDown();
               plans.requestFocus();
               return  false;
           }
       });

        mode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mode.setPadding(0,5,0,5);
                mode.showDropDown();
                mode.requestFocus();
                return  false;
            }
        });*/

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        caleder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(Add_plan_to_member.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                               /* joindate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);*/
                                Date date1 = null;
                                try {
                                    date1 = new SimpleDateFormat("dd-mm-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                st_startdate = new SimpleDateFormat("yyyy-mm-dd").format(date1);
                                joindate.setText(st_startdate);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }


        });
        mode.setAdapter(new ArrayAdapter<String>(Add_plan_to_member.this, android.R.layout.simple_spinner_dropdown_item, modes));


        mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(AddmemberActivity.this,batcharray[i],Toast.LENGTH_SHORT).show();

                selectedmode = adapterView.getItemAtPosition(i).toString().trim();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        plans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(AddmemberActivity.this,batcharray[i],Toast.LENGTH_SHORT).show();
                String selectedCode = PlansArray.get(i);
                selected = adapterView.getItemAtPosition(i).toString().trim();
                getplandetails(selected);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // plan=plans.getEditableText().toString().trim();
                plan_duration = duration.getEditableText().toString().trim();
                plan_details = details.getEditableText().toString().trim();
                plan_fee = fees.getEditableText().toString().trim();
                plan_admissiofee = admissiofee.getEditableText().toString().trim();
                plan_discount = discount.getEditableText().toString().trim();
                plan_tax = tax.getEditableText().toString().trim();
                plan_paidamount = paidamount.getEditableText().toString().trim();
                plan_mode = selectedmode;

               /* if(plan.isEmpty()){
                    plans.setError("Please Select Plan");
                }*/
                if (plan_duration.isEmpty()) {
                    duration.setError("Please enter duration");
                } else if (st_startdate.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Please Select plan start date", Toast.LENGTH_SHORT).show();

                } else if (plan_details.isEmpty()) {
                    details.setError("Please enter details");
                } else if (plan_fee.isEmpty()) {
                    fees.setError("Please enter fee");
                } else if (plan_admissiofee.isEmpty()) {
                    admissiofee.setError("Please enter admission fee");
                } else if (plan_discount.isEmpty()) {
                    discount.setError("Please enter discount amount");
                } else if (plan_tax.isEmpty()) {
                    tax.setError("Please enter tax amount");
                } else if (plan_paidamount.isEmpty()) {
                    paidamount.setError("Please enter paid amount");
                } else if (st_startdate.isEmpty()) {
                    startdate.setError("Please Select date");
                } else if (PublicHelper.isConnectedToInternet(Add_plan_to_member.this)) {
                    // add_plan_to_server();


                    showbillpreview();
                } else {
                    Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }



    private void getallplans() {
       // PublicHelper.showProgressDialog(Add_plan_to_member.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));

        Call<ArrayList<Plan_nameDAO>> call = RetrofitInstance.getInstance().getMyApi().get_plan_array(listDetails);
        call.enqueue(new Callback<ArrayList<Plan_nameDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<Plan_nameDAO>> call, Response<ArrayList<Plan_nameDAO>> response) {
                if (planslist.size() > 0) {
                    planslist.clear();
                }
                planslist = (ArrayList<Plan_nameDAO>) response.body();


                String mmm= new Gson().toJson(planslist);
                try {
                    JSONArray jsonArray=new JSONArray(mmm);
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        String plannamess=jsonObject.getString("plan_name");
                        Log.v("TAG","plannamess "+plannamess);


                        PlansArray.add(plannamess);
                        plans.setAdapter(new ArrayAdapter<String>(Add_plan_to_member.this, android.R.layout.simple_spinner_dropdown_item,PlansArray));

                      //  PublicHelper.dismissProgressDialog();
                     /*   String selctedplan="gold";
                        detailarray=jsonObject.getJSONArray("details");

                        for(int k=0;k<detailarray.length();k++){
                            JSONObject jsonObject2 = detailarray.getJSONObject(k);
                            String insideplanname=jsonObject2.getString("plan_name");

                            if(selctedplan.equalsIgnoreCase(insideplanname)){
                                duration.setText(jsonObject2.getString("duration"));
                                details.setText(jsonObject2.getString("plan_details"));
                                fees.setText(jsonObject2.getString("feese"));
                            }
                        }*/





                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<Plan_nameDAO>> call, Throwable t) {
               // Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();

               // PublicHelper.dismissProgressDialog();
            }
        });

    }

    private void getplandetails(String selected) {
       // PublicHelper.showProgressDialog(Add_plan_to_member.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
        listDetails.put("planname", selected);

        Call<ArrayList<Plan_detailsDAO>> call = RetrofitInstance.getInstance().getMyApi().get_plan_details(listDetails);
        call.enqueue(new Callback<ArrayList<Plan_detailsDAO>>() {
            @Override
            public void onResponse(Call<ArrayList<Plan_detailsDAO>> call, Response<ArrayList<Plan_detailsDAO>> response) {

               // Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();

                plandetails=(ArrayList<Plan_detailsDAO>)response.body();
                String mmm= new Gson().toJson(plandetails);
                try {
                    JSONArray jsonArray=new JSONArray(mmm);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        duration.setText(jsonObject.getString("duration"));
                        details.setText(jsonObject.getString("plan_details"));
                        fees.setText(jsonObject.getString("feese"));
                       // PublicHelper.dismissProgressDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<Plan_detailsDAO>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  //PublicHelper.dismissProgressDialog();
            }
        });
    }

    private void add_plan_to_server() {
        PublicHelper.showProgressDialog(Add_plan_to_member.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", memberid);
        listDetails.put("plan", selected);
        listDetails.put("plan_duration", plan_duration);
        listDetails.put("plan_start", st_startdate);

        listDetails.put("plan_details", plan_details);
        listDetails.put("plan_fees", plan_fee);
        listDetails.put("admission_fee", plan_admissiofee);
        listDetails.put("discount", plan_discount);
        listDetails.put("tax", plan_tax);
        listDetails.put("paid_amount", plan_paidamount);
        listDetails.put("unpaid_amount", unpaidamount);
        listDetails.put("payment_mode", plan_mode);


            Call<Plan_purchase_DAO> call = RetrofitInstance.getInstance().getMyApi().add_plan_to_member(listDetails);
            call.enqueue(new Callback<Plan_purchase_DAO>() {
                @Override
                public void onResponse(Call<Plan_purchase_DAO> call, Response<Plan_purchase_DAO> response) {
                    PublicHelper.dismissProgressDialog();
                    //Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();

                    Plan_purchase_DAO plan_purchase_dao=response.body();

                     expiry=plan_purchase_dao.getExpiry_date();

                    Generate_bill(expiry);
                   /* Intent intent=new Intent(Add_plan_to_member.this,MembersPlan.class);
                    intent.putExtra("memberid",memberid);
                    startActivity(intent);
                    finish();*/

                   // showbillpreview();
                }

                @Override
                public void onFailure(Call<Plan_purchase_DAO> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  PublicHelper.dismissProgressDialog();
                }
            });


    }

    private void showbillpreview() {

        Dialog dialog=new Dialog(Add_plan_to_member.this);
        dialog.setContentView(R.layout.bill_preview_layout);

       TextView tvadmissionfee=dialog.findViewById(R.id.admissionee);
        TextView tvfees=dialog.findViewById(R.id.fees);
        TextView tvdiscount=dialog.findViewById(R.id.discount);
        TextView tvtaxable=dialog.findViewById(R.id.taxable);
        TextView tvtax=dialog.findViewById(R.id.taxamount);
        TextView tvfinalamount=dialog.findViewById(R.id.finalamount);
        TextView tvpaid=dialog.findViewById(R.id.paid_amount);
        TextView tvunpaid=dialog.findViewById(R.id.unpaid_amount);
        TextView cancel=dialog.findViewById(R.id.cancel);
        TextView save=dialog.findViewById(R.id.save);

        String TaxableAmount= String.valueOf(Integer.parseInt(plan_admissiofee) + Integer.parseInt(plan_fee)- Integer.parseInt(plan_discount));

        String Final_Amount= String.valueOf(Integer.parseInt(TaxableAmount) + Integer.parseInt(plan_tax));

        unpaidamount= String.valueOf(Integer.parseInt(Final_Amount)-Integer.parseInt(plan_paidamount));

        tvadmissionfee.setText(plan_admissiofee);
        tvfees.setText(plan_fee);
        tvdiscount.setText(plan_discount);
        tvtaxable.setText(TaxableAmount);
        tvtax.setText(plan_tax);
        tvfinalamount.setText(Final_Amount);
        tvpaid.setText(plan_paidamount);
        tvunpaid.setText(unpaidamount);
        //tvadmissionfee.setText(plan_admissiofee);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                add_plan_to_server();
            }
        });


        dialog.show();

    }

    private void Generate_bill(String expiry) {


       /* Intent i=getIntent();
        Bundle b=i.getExtras();
        MembersOverallDAO= (MembersOverallDAO) b.getSerializable("user");
*/
        Dialog dialog=new Dialog(Add_plan_to_member.this);
        dialog.setContentView(R.layout.bill_generate_layout);

        TextView tvgymname=dialog.findViewById(R.id.gymname);
        TextView tvgymcontact=dialog.findViewById(R.id.gymcontact);
        TextView tvgymrecipt=dialog.findViewById(R.id.recipt_number);
        TextView tvmembername=dialog.findViewById(R.id.tvmembername);
        TextView tvmmembrid=dialog.findViewById(R.id.tvmemberid);
        TextView tvmemberphone=dialog.findViewById(R.id.tvmembermobile);
        TextView tvmemberjoindate=dialog.findViewById(R.id.tvjoinedate);
        CircleImageView profile=dialog.findViewById(R.id.ivprofile);

        TextView tvpurchasedate=dialog.findViewById(R.id.purchasedate);
        TextView tvplanname=dialog.findViewById(R.id.planname);
        TextView tvstartdate=dialog.findViewById(R.id.startdate);
        TextView tvexpirydate=dialog.findViewById(R.id.expiredate);

        TextView tvadmissionfee=dialog.findViewById(R.id.admissionee);
        TextView tvfees=dialog.findViewById(R.id.fees);
        TextView tvdiscount=dialog.findViewById(R.id.discount);
        TextView tvtaxable=dialog.findViewById(R.id.taxable);
        TextView tvtax=dialog.findViewById(R.id.taxamount);
        TextView tvfinalamount=dialog.findViewById(R.id.finalamount);
        TextView tvpaid=dialog.findViewById(R.id.paid_amount);
        TextView tvunpaid=dialog.findViewById(R.id.unpaid_amount);
        TextView share=dialog.findViewById(R.id.share);
        TextView download=dialog.findViewById(R.id.download);
        TextView watsapp=dialog.findViewById(R.id.watsapp);


        Random random=new Random();
        String receipt= String.valueOf(random.nextInt(10000));

        tvgymname.setText(preferenceManager.getGymName("gymname"));
        tvgymcontact.setText(preferenceManager.getGymContact("gymcontact"));
        tvgymrecipt.setText("Receipt No. "+receipt);

        tvmembername.setText(MembersOverallDAO.getName());
        tvmmembrid.setText("Member id : "+MembersOverallDAO.getMember_id());
        tvmemberphone.setText(MembersOverallDAO.getMobile());
        tvmemberjoindate.setText(MembersOverallDAO.getJoindate());

        Glide.with(Add_plan_to_member.this).load(MembersOverallDAO.getProfilepic())
                .thumbnail(0.5f)
                .into(profile);

        tvpurchasedate.setText(st_startdate);
        tvplanname.setText(selected);
        tvstartdate.setText(st_startdate);
        tvexpirydate.setText(expiry);

        String TaxableAmount= String.valueOf(Integer.parseInt(plan_admissiofee) + Integer.parseInt(plan_fee)- Integer.parseInt(plan_discount));
        String Final_Amount= String.valueOf(Integer.parseInt(TaxableAmount) + Integer.parseInt(plan_tax));
        String unpaidamount= String.valueOf(Integer.parseInt(Final_Amount)-Integer.parseInt(plan_paidamount));

        tvadmissionfee.setText(plan_admissiofee);
        tvfees.setText(plan_fee);
        tvdiscount.setText(plan_discount);
        tvtaxable.setText(TaxableAmount);
        tvtax.setText(plan_tax);
        tvfinalamount.setText(Final_Amount);
        tvpaid.setText(plan_paidamount);
        tvunpaid.setText(unpaidamount);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                share_invoice();
            }
        });

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             dialog.cancel();
               /* Intent intent=new Intent(Add_plan_to_member.this,MembersPlan.class);
                Bundle b=new Bundle();
                b.putSerializable("user", MembersOverallDAO);
                intent.putExtras(b);
                startActivity(intent);
                finish();*/
                create_downlad_file();
               /* try {
                    create_pdfs();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }*/
            }
        });

        watsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                Intent intent=new Intent(Add_plan_to_member.this,MembersPlan.class);
                Bundle b=new Bundle();
                b.putSerializable("user", MembersOverallDAO);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });



        dialog.show();
    }

    private void create_downlad_file() {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
           // Toast.
        }

//Create a directory for your PDF
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), "MyApp");
        if (!pdfDir.exists()){
            pdfDir.mkdir();
        }

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View content = inflater.inflate(R.layout.printable_invoice, null);
        TextView tvgymname=content.findViewById(R.id.gymname);
        TextView tvgymcontact=content.findViewById(R.id.gymcontact);
        TextView tvgymrecipt=content.findViewById(R.id.recipt_number);
        TextView tvmembername=content.findViewById(R.id.tvmembername);
        TextView tvmmembrid=content.findViewById(R.id.tvmemberid);
        TextView tvmemberphone=content.findViewById(R.id.tvmembermobile);
        TextView tvmemberjoindate=content.findViewById(R.id.tvjoinedate);
        CircleImageView profile=content.findViewById(R.id.ivprofile);

        TextView tvpurchasedate=content.findViewById(R.id.purchasedate);
        TextView tvplanname=content.findViewById(R.id.planname);
        TextView tvstartdate=content.findViewById(R.id.startdate);
        TextView tvexpirydate=content.findViewById(R.id.expiredate);

        TextView tvadmissionfee=content.findViewById(R.id.admissionee);
        TextView tvfees=content.findViewById(R.id.fees);
        TextView tvdiscount=content.findViewById(R.id.discount);
        TextView tvtaxable=content.findViewById(R.id.taxable);
        TextView tvtax=content.findViewById(R.id.taxamount);
        TextView tvfinalamount=content.findViewById(R.id.finalamount);
        TextView tvpaid=content.findViewById(R.id.paid_amount);
        TextView tvunpaid=content.findViewById(R.id.unpaid_amount);



        Random random=new Random();
        String receipt= String.valueOf(random.nextInt(10000));

        tvgymname.setText(preferenceManager.getGymName("gymname"));
        tvgymcontact.setText(preferenceManager.getGymContact("gymcontact"));
        tvgymrecipt.setText("Receipt No. "+receipt);

        tvmembername.setText(MembersOverallDAO.getName());
        tvmmembrid.setText("Member id : "+MembersOverallDAO.getMember_id());
        tvmemberphone.setText(MembersOverallDAO.getMobile());
        tvmemberjoindate.setText(MembersOverallDAO.getJoindate());

        Glide.with(Add_plan_to_member.this).load(MembersOverallDAO.getProfilepic())
                .thumbnail(0.5f)
                .into(profile);

        tvpurchasedate.setText(st_startdate);
        tvplanname.setText(selected);
        tvstartdate.setText(st_startdate);
        tvexpirydate.setText(expiry);

        String TaxableAmount= String.valueOf(Integer.parseInt(plan_admissiofee) + Integer.parseInt(plan_fee)- Integer.parseInt(plan_discount));
        String Final_Amount= String.valueOf(Integer.parseInt(TaxableAmount) + Integer.parseInt(plan_tax));
        String unpaidamount= String.valueOf(Integer.parseInt(Final_Amount)-Integer.parseInt(plan_paidamount));

        tvadmissionfee.setText(plan_admissiofee);
        tvfees.setText(plan_fee);
        tvdiscount.setText(plan_discount);
        tvtaxable.setText(TaxableAmount);
        tvtax.setText(plan_tax);
        tvfinalamount.setText(Final_Amount);
        tvpaid.setText(plan_paidamount);
        tvunpaid.setText(unpaidamount);



        PdfGenerator.getBuilder()
                .setContext(Add_plan_to_member.this)
                .fromViewSource()
                .fromView(content)
                .setFileName("Gym_member_invoice_"+MembersOverallDAO.getMember_id())
                .setFolderName("Invoice")
                .openPDFafterGeneration(true)
                .build(new PdfGeneratorListener() {
                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        super.onFailure(failureResponse);
                    }
                    @Override
                    public void onStartPDFGeneration() {

                    }
                    @Override
                    public void onFinishPDFGeneration() {

                    }
                    @Override
                    public void showLog(String log) {
                        super.showLog(log);
                    }

                    @Override
                    public void onSuccess(SuccessResponse response) {
                        super.onSuccess(response);
                        Toast.makeText(Add_plan_to_member.this,"pdf generated",Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void share_invoice() {
        File pdfFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        String path= "Gym_member_invoice_"+MembersOverallDAO.getMember_id()+".pdf";

        File myFile = new File(pdfFolder.getAbsolutePath() + File.separator + path);

        Intent intentShareFile = new Intent();
        intentShareFile.setAction(Intent.ACTION_SEND);
        if(myFile.exists()) {
            intentShareFile.setType("application/pdf");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(myFile));

            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing File from Webkul...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File from Webkul to purchase items...");

            startActivity(Intent.createChooser(intentShareFile, "Share File Webkul"));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}