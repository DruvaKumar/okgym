package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.BatchnamesDAO;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;

public class AddmemberActivity extends AppCompatActivity {
    TextInputEditText memberName,memberid, membermobile, memberemail, memberaddress, moredetails;
    TextView joindate, dob,  tvhaeder, tvsave;
    RadioGroup rgroup;
    RadioButton rbtnmale, rbtnfemale, gender;
    ImageView imgJoindate, imgDob, imgBack;
    CircleImageView profile;
    CardView cardAddphoto;
    Spinner spinner,memberbatch;
    String batch, membergender;
    String mNmae, mid, mmobile, memail, maddress, mdetails, mjoindate="", mdob="", mbatch, mgender;
    PreferenceManager preferenceManager;
    private  static  final  int CAMERA_REQUEST= 1888;
    private  static  final  int MY_CAMERA_CODE=100;
    String profilepath;
    private static final int PICK_IMAGE = 1;
    DatePicker datePicker;
    DatePickerDialog datePickerDialog;
    Uri picUri;
    Bitmap myBitmap;
    File profilepic;
    private static final int GALLERY = 1;
    private static Bitmap Image = null;
    private static Bitmap rotateImage = null;
    private static final int PERMISSION_REQUEST_CODE = 1;
    File profilepicture;
    ArrayList<String> BatchArray=new ArrayList<>();
    ArrayList<BatchnamesDAO>batchlist=new ArrayList<>();
    String batch_space,enquiryid="0";
    String selected="";
    //String[] batcharray = {"Select Batch", "Batch1", "Batch2", "Batch3", "Batch4"};

    JSONObject jsonObject;
    EnquiryDAO enquiryDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_addmember);

        preferenceManager = new PreferenceManager(getApplicationContext());

        int old_meberid=preferenceManager.getGymCount("count");
        int new_member=old_meberid+1;

        getbatches();

        memberName = findViewById(R.id.et_membername);
        memberid = findViewById(R.id.et_memid);
        membermobile = findViewById(R.id.et_mobile);
        memberemail = findViewById(R.id.et_email);
        memberaddress = findViewById(R.id.et_adress);

        joindate = findViewById(R.id.tvjoindate);
        dob = findViewById(R.id.tvdob);
        memberbatch = findViewById(R.id.spinner);
        moredetails = findViewById(R.id.tvmoredetails);
        spinner=findViewById(R.id.spinner);

        rgroup = findViewById(R.id.radiogroup);
        rbtnmale = findViewById(R.id.rbtb_male);
        rbtnfemale = findViewById(R.id.rbtb_female);

        imgJoindate = findViewById(R.id.img_joindate);
        //spinner = findViewById(R.id.img_batch);
        imgDob = findViewById(R.id.img_dob);
        profile = findViewById(R.id.img_profile);

        tvhaeder = findViewById(R.id.header);
        tvsave = findViewById(R.id.tv_save);
        imgBack = findViewById(R.id.btn_back);
        cardAddphoto = findViewById(R.id.cardview);

        tvsave.setVisibility(View.VISIBLE);

        tvhaeder.setText("Add Member");
        memberid.setText(String.valueOf(new_member));
        membergender="Male";

        Intent i=getIntent();
        Bundle b=i.getExtras();
        if(b!=null){
            enquiryDAO= (EnquiryDAO) b.getSerializable("user");
            memberName.setText(enquiryDAO.getMember_name());
            membermobile.setText(enquiryDAO.getMobile());
            memberemail.setText(enquiryDAO.getEmail());
            memberaddress.setText(enquiryDAO.getAddress());
            moredetails.setText(enquiryDAO.getDescription());
            enquiryid=enquiryDAO.getEnquiry_id();
        }


        if (!checkPermission()) {

            requestPermission();

        } else {

            // Snackbar.make(view, "Permission already granted.", Snackbar.LENGTH_LONG).show();

        }


        tvsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* startActivity(new Intent(getApplicationContext(),AddMemberDocumentsActivity.class));
                finish();*/
                Add_member_details_toserver();
            }
        });

        cardAddphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {

                    ActivityCompat.requestPermissions(AddmemberActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 200);

                }

                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent.createChooser(intent,"Select File"),GALLERY);

            }



        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgJoindate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(AddmemberActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                               /* joindate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);*/
                                Date date1 = null;
                                try {
                                    date1 = new SimpleDateFormat("dd-mm-yyyy").parse(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                mjoindate = new SimpleDateFormat("yyyy-mm-dd").format(date1);
                                joindate.setText(mjoindate);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }

        });

        imgDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(AddmemberActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                               /* joindate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);*/
                                Date date2 = null;
                                try {
                                    date2 = new SimpleDateFormat("dd-mm-yyyy").parse(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                mdob = new SimpleDateFormat("yyyy-mm-dd").format(date2);
                                dob.setText(mdob);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }

        });



        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(AddmemberActivity.this,batcharray[i],Toast.LENGTH_SHORT).show();
                String selectedCode=BatchArray.get(i);
                 selected = adapterView.getItemAtPosition(i).toString().trim();
               // getplandetails(selected);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       /* memberbatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getbatches();
            }
        });
*/
      /*  ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, batcharray);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(AddmemberActivity.this,batcharray[i],Toast.LENGTH_SHORT).show();
                batch = batcharray[i].toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        rgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                int selctedid = rgroup.getCheckedRadioButtonId();
                gender = findViewById(selctedid);
                membergender = gender.getText().toString().trim();
                //Toast.makeText(AddmemberActivity.this,gender.getText().toString(),Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void getbatches() {

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));

        Call<ArrayList<BatchnamesDAO>> call = RetrofitInstance.getInstance().getMyApi().get_batch_array(listDetails);
        call.enqueue(new Callback<ArrayList<BatchnamesDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<BatchnamesDAO>> call, Response<ArrayList<BatchnamesDAO>> response) {
                if (batchlist.size() > 0) {
                    batchlist.clear();
                }
                batchlist = (ArrayList<BatchnamesDAO>) response.body();


                String mmm= new Gson().toJson(batchlist);
                try {
                    JSONArray jsonArray=new JSONArray(mmm);
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        String plannamess=jsonObject.getString("batch_name");
                        batch_space= jsonObject.getString("remainder");


                        BatchArray.add(plannamess);
                        memberbatch.setAdapter(new ArrayAdapter<String>(AddmemberActivity.this, android.R.layout.simple_spinner_dropdown_item,BatchArray));


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<BatchnamesDAO>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void Add_member_details_toserver() {
        mNmae = memberName.getEditableText().toString().trim();
        mid = memberid.getEditableText().toString().trim();
        mmobile = membermobile.getEditableText().toString().trim();
        maddress = memberaddress.getEditableText().toString().trim();
        memail = memberemail.getEditableText().toString().trim();
        mgender = membergender;

        mbatch = batch;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        mdetails = moredetails.getEditableText().toString().trim();
        if(selected.equalsIgnoreCase("")){

             Toast.makeText(AddmemberActivity.this,"Please Create a Batch to proceed",Toast.LENGTH_LONG).show();
        }

       else if (mNmae.isEmpty() || mNmae.length() <= 2) {
            memberName.setError("Please enter Member Name");
        } else if (mmobile.isEmpty() || mmobile.length() < 10) {
            membermobile.setError("Please enter valid 10 digit mobile number");
        }
        else if (!memail.matches(emailPattern) ){
            memberemail.setError("Please enter valid Email Id");
        }
        else if (maddress.isEmpty() ) {
            memberaddress.setError("Please enter member's address");
        }
        else if (mjoindate.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Please select join date", Toast.LENGTH_SHORT).show();
           //joindate.setError("Please select join date");
        }
        else if (mdob.equalsIgnoreCase("") ) {
            Toast.makeText(getApplicationContext(), "Please Select Date of Birth", Toast.LENGTH_SHORT).show();
           // dob.setError("Please Selct Date of Birth");
        }
        else  if(profilepicture==null){
            Toast.makeText(getApplicationContext(), "Please add a photo", Toast.LENGTH_SHORT).show();
        }


       /* else if(batch_space.equalsIgnoreCase("0")){
            Toast.makeText(getApplicationContext(), "This Batch is already full", Toast.LENGTH_SHORT).show();

        }*/
       /* else if (mbatch.contains("Select Batch") ) {
            membermobile.setError("Plaese Select a Batch");
        }*/
        else if (PublicHelper.isConnectedToInternet(getApplicationContext())) {
            PublicHelper.showProgressDialog(AddmemberActivity.this,"");
            File file1 = new File(String.valueOf(profilepicture));
            // create RequestBody instance from file

            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
            MultipartBody.Part imageBody1 =
                    MultipartBody.Part.createFormData("profile", file1.getName(), requestFile1);

            RequestBody member_name = RequestBody.create(MediaType.parse("text/plain"), mNmae);
            RequestBody member_id = RequestBody.create(MediaType.parse("text/plain"), mid);
            RequestBody member_details = RequestBody.create(MediaType.parse("text/plain"), mdetails);
            RequestBody member_mobile = RequestBody.create(MediaType.parse("text/plain"), mmobile);
            RequestBody member_address = RequestBody.create(MediaType.parse("text/plain"), maddress);
            RequestBody member_email = RequestBody.create(MediaType.parse("text/plain"), memail);
            RequestBody member_gender = RequestBody.create(MediaType.parse("text/plain"), mgender);
            RequestBody member_joindate = RequestBody.create(MediaType.parse("text/plain"), mjoindate);
            RequestBody member_dob = RequestBody.create(MediaType.parse("text/plain"), mdob);
            RequestBody member_batch = RequestBody.create(MediaType.parse("text/plain"), selected);
            RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getUserid(Config.USERID));
            RequestBody enquiryId = RequestBody.create(MediaType.parse("text/plain"), enquiryid);

            //  LocaleHelper.showProgressDialog(getActivity(), getString(R.string.please));
            Call<Void> call = RetrofitInstance.getInstance().getMyApi().addnewmembers(imageBody1, member_name, member_id, member_details,
                    member_mobile, member_address, member_email, member_gender, member_joindate, member_dob, member_batch,userid,enquiryId);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();

                    preferenceManager.setMemberId(mid);
                    preferenceManager.setGymCount(Integer.parseInt(mid));
                     startActivity(new Intent(AddmemberActivity.this, AddMemberDocumentsActivity.class));
                     finish();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,requestCode,data);

        if(resultCode== Activity.RESULT_OK){
            Uri seletecdImageUri = data.getData();

            profilepath=getPath(seletecdImageUri);
            Log.v("TAG","path "+ profilepath);
            profilepicture=new File(profilepath);
            profile.setImageURI(seletecdImageUri);
        }
    }
    public String getPath(Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean storage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (storage && cameraAccepted) {
                    }
                    // Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    else{


                        //Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{READ_EXTERNAL_STORAGE, CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(AddmemberActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}