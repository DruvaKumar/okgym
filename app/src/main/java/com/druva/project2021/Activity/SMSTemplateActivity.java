package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterSMSTemplate;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.SMStemplatesDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SMSTemplateActivity extends AppCompatActivity {
    RecyclerView rvtemplate;
    FloatingActionButton float_addtemplate;
    ImageView img_back;
    TextView Header,save;
    PreferenceManager preferenceManager;
    
    ArrayList<SMStemplatesDAO> templatelist=new ArrayList<>();
    AdapterSMSTemplate adapterSMSTemplate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_smstemplate);
        preferenceManager=new PreferenceManager(getApplicationContext());

        rvtemplate=findViewById(R.id.rvtemplate);
        float_addtemplate=findViewById(R.id.floattemplate);
        Header=findViewById(R.id.header);
        img_back=findViewById(R.id.btn_back);
        save=findViewById(R.id.tv_save);

        save.setVisibility(View.GONE);
        Header.setText("SMS Template");

        rvtemplate.setLayoutManager(new LinearLayoutManager(SMSTemplateActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getsmstemplates(preferenceManager.getUserid(Config.USERID));
        rvtemplate.setItemAnimator(new DefaultItemAnimator());

        float_addtemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              addsmstemplatedialogue();

            }

            private void addsmstemplatedialogue() {
                Dialog dialog=new Dialog(SMSTemplateActivity.this);
                dialog.setContentView(R.layout.sms_template_layout);

                EditText tittle=dialog.findViewById(R.id.et_tittle);
                EditText desc=dialog.findViewById(R.id.et_description);
                TextView save=dialog.findViewById(R.id.btnsave);
                TextView cancecl=dialog.findViewById(R.id.btncancel);

                cancecl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });



                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String tittles=tittle.getEditableText().toString().trim();
                        String titiledesc=desc.getEditableText().toString().trim();
                        if(tittles.isEmpty()|| titiledesc.isEmpty()){
                            Toast.makeText(SMSTemplateActivity.this, "Please enter tittle & description", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            savetemplates(tittles, titiledesc);
                            dialog.cancel();

                        }
                    }
                });


                dialog.show();
            }
        });
    }

    private void getsmstemplates(String userid) {
        PublicHelper.showProgressDialog(SMSTemplateActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", userid);

        Call<ArrayList<SMStemplatesDAO>> call = RetrofitInstance.getInstance().getMyApi().get_all_templates(listDetails);
        call.enqueue(new Callback<ArrayList<SMStemplatesDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<SMStemplatesDAO>> call, Response<ArrayList<SMStemplatesDAO>> response) {
                if (templatelist.size() > 0) {
                    templatelist.clear();
                }
                templatelist = (ArrayList<SMStemplatesDAO>) response.body();
                adapterSMSTemplate = new AdapterSMSTemplate(templatelist, SMSTemplateActivity.this);
                rvtemplate.setAdapter(adapterSMSTemplate);
                PublicHelper.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ArrayList<SMStemplatesDAO>> call, Throwable t) {
              //  Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();

                PublicHelper.dismissProgressDialog();
            }
        });

    }
        


    private void savetemplates(String tittle , String description) {
       if (PublicHelper.isConnectedToInternet(getApplicationContext())) {
            HashMap<String, Object> listDetails = new HashMap<>();
            listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
            listDetails.put("tittle", tittle);
            listDetails.put("description", description);


            Call<Object> call = RetrofitInstance.getInstance().getMyApi().addtemplates(listDetails);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                    Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(SMSTemplateActivity.this,SMSTemplateActivity.class));
                    finish();
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

    }
    }
