package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.druva.project2021.Adapters.MembersAdapter;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersDAO;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembersProfile extends AppCompatActivity implements Serializable {
    CircleImageView profile;
    EditText name,joindate,mobile,email,address,gender,dob,batch;
    ImageView doc1 ,doc2,back;
    TextView saveprofile,editdocument,haeader,id;
    PreferenceManager preferenceManager;

    MembersOverallDAO membersDAO;
    Uri uri_profile, uri_doc1,uridoc2;
    File profilepic,document1,document2;
    String mNmae, mid, mmobile, memail, maddress, mdetails, mjoindate="", mdob="", mbatch, mgender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_members_profile);
        preferenceManager=new PreferenceManager(getApplicationContext());

        name=findViewById(R.id.name);
        id=findViewById(R.id.id);
        joindate=findViewById(R.id.joindate);
        mobile=findViewById(R.id.mobile);
        email=findViewById(R.id.email);
        address=findViewById(R.id.address);
        gender=findViewById(R.id.gender);
        dob=findViewById(R.id.dob);
        batch=findViewById(R.id.batch);
        doc1=findViewById(R.id.doc1);
        doc2=findViewById(R.id.doc2);
        saveprofile=findViewById(R.id.saveprofile);
        editdocument=findViewById(R.id.editdocuments);
        profile=findViewById(R.id.profile);


        back=findViewById(R.id.btn_back);
        haeader=findViewById(R.id.header);
        haeader.setText("Member Profile");

        Intent i=getIntent();
        Bundle b=i.getExtras();
        membersDAO= (MembersOverallDAO) b.getSerializable("user");

        name.setText(membersDAO.getName());
        id.setText(membersDAO.getMember_id());
        joindate.setText(membersDAO.getJoindate());
        mobile.setText(membersDAO.getMobile());
        email.setText(membersDAO.getEmail());
        address.setText(membersDAO.getAddress());
        gender.setText(membersDAO.getGender());
        dob.setText(membersDAO.getDob());
        batch.setText(membersDAO.getBatch());

        uri_profile= Uri.parse(membersDAO.getProfilepic());
        uri_doc1= Uri.parse(membersDAO.getDocument_1());
        uridoc2= Uri.parse(membersDAO.getDocument_2());

       /* profilepic=new File(uri_profile);
        document1=new File(uri_doc1);
        document2=new File(uridoc2);
*/
        Glide.with(MembersProfile.this).load(membersDAO.getProfilepic())
                .thumbnail(0.5f)
                .into(profile);
        Glide.with(MembersProfile.this).load(membersDAO.getDocument_1())
                .thumbnail(0.5f)
                .into(doc1);
        Glide.with(MembersProfile.this).load(membersDAO.getDocument_2())
                .thumbnail(0.5f)
                .into(doc2);

        saveprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatemembersdetails();

            }
        });
        editdocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferenceManager.setMemberId(membersDAO.getMember_id());
                startActivity(new Intent(MembersProfile.this, AddMemberDocumentsActivity.class));

            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });

        doc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        doc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void updatemembersdetails() {

        mNmae = name.getEditableText().toString().trim();
        mid = id.getText().toString().trim();
        mmobile = mobile.getEditableText().toString().trim();
        maddress = address.getEditableText().toString().trim();
        memail = email.getEditableText().toString().trim();
        mgender = gender.getEditableText().toString().trim();
        mjoindate=joindate.getEditableText().toString().trim();
        mdob=dob.getEditableText().toString().trim();

        mbatch = batch.getEditableText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


        /*if (profile==null){
            uri_profile= (Uri) profile.getTag();
        }*/

        if (mNmae.isEmpty() || mNmae.length() <= 2) {
            name.setError("Please enter Member Name");
        } else if (mmobile.isEmpty() || mmobile.length() < 10) {
            mobile.setError("Please enter valid 10 digit mobile number");
        } else if (!memail.matches(emailPattern)) {
            email.setError("Please enter valid Email Id");
        } else if (maddress.isEmpty()) {
            address.setError("Please enter member's address");
        } else if (mjoindate.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please set join date", Toast.LENGTH_SHORT).show();
            //joindate.setError("Please select join date");
        } else if (mgender.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please add mmebrs gender", Toast.LENGTH_SHORT).show();
            // dob.setError("Please Selct Date of Birth");
        }
        else if (mdob.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Set Date of Birth", Toast.LENGTH_SHORT).show();
            // dob.setError("Please Selct Date of Birth");
        }
        else if (mbatch.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please add batch name", Toast.LENGTH_SHORT).show();
            // dob.setError("Please Selct Date of Birth");
        }
        else if (PublicHelper.isConnectedToInternet(MembersProfile.this)) {



        /*File file1 = new File(String.valueOf(uri_profile));
        RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
        MultipartBody.Part imageBody1 =
                MultipartBody.Part.createFormData("profile", file1.getName(), requestFile1);

        File file2 = new File(String.valueOf(uri_doc1));
        RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
        MultipartBody.Part imageBody2 =
                MultipartBody.Part.createFormData("doc1", file2.getName(), requestFile2);

        File file3 = new File(String.valueOf(uridoc2));
        RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file3);
        MultipartBody.Part imageBody3 =
                MultipartBody.Part.createFormData("doc2", file3.getName(), requestFile3);*/
            PublicHelper.showProgressDialog(MembersProfile.this, "");

            RequestBody member_name = RequestBody.create(MediaType.parse("text/plain"), name.getEditableText().toString().trim());
            RequestBody member_id = RequestBody.create(MediaType.parse("text/plain"), id.getText().toString().trim());
            RequestBody member_mobile = RequestBody.create(MediaType.parse("text/plain"), mobile.getEditableText().toString().trim());
            RequestBody member_address = RequestBody.create(MediaType.parse("text/plain"), address.getEditableText().toString().trim());
            RequestBody member_email = RequestBody.create(MediaType.parse("text/plain"), email.getEditableText().toString().trim());
            RequestBody member_gender = RequestBody.create(MediaType.parse("text/plain"), gender.getEditableText().toString().trim());
            RequestBody member_joindate = RequestBody.create(MediaType.parse("text/plain"), joindate.getEditableText().toString().trim());
            RequestBody member_dob = RequestBody.create(MediaType.parse("text/plain"), dob.getEditableText().toString().trim());
            RequestBody member_batch = RequestBody.create(MediaType.parse("text/plain"), batch.getEditableText().toString().trim());
            RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getUserid(Config.USERID));


            Call<Void> call = RetrofitInstance.getInstance().getMyApi().update_member_details(member_name, member_id,
                    member_mobile, member_address, member_email, member_gender, member_joindate, member_dob, member_batch, userid);
            call.enqueue(new Callback<Void>() {

                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    PublicHelper.dismissProgressDialog();
                    //Log.v("TAG",response.toString());
                    startActivity(new Intent(getApplicationContext(), MembersActivity.class));
                    finish();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Something went wrong ", Toast.LENGTH_SHORT).show();


                }
            });

        }
    }
}