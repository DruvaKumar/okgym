package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.druva.project2021.BottomNavigation;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.druva.project2021.databinding.ProgressDialogLayoutBinding;
import com.druva.project2021.pojo.signPojo;
import com.druva.project2021.reftrofit.ApiInterface;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class PhonenumberActivity extends AppCompatActivity {

    TextInputEditText etPhonenumber;
    Button btnGetotp;
    String mobile, device;

    ApiInterface apiInterface;
    ArrayList<signPojo> loginData;
    Session session;
    PreferenceManager preferenceManager;
    ProgressBar progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_phonenumber);

        session=new Session(this);
        preferenceManager=new PreferenceManager(getApplicationContext());

        loginData = new ArrayList<>();
        etPhonenumber = findViewById(R.id.et_phone);
        btnGetotp = findViewById(R.id.btn_getotp);

        device = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        btnGetotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent intent=new Intent(PhonenumberActivity.this,OTPActivity.class);
                startActivity(intent);*/
                mobile = etPhonenumber.getEditableText().toString().trim();

                if (mobile.length() < 10) {
                    etPhonenumber.setError("Enter 10 digit mobile number");
                } else {
                    senddatatoserver();
                }
            }
        });
    }




    private void callAPItochecknumber() {

      /*  HashMap<String, Object> map = new HashMap<>();
        map.put("region",mobile);
       // map.put("device",device);

        Call<ArrayList<signPojo>> call = RetrofitInstance.getInstance().getMyApi().checkmobilenumber(map);
        call.enqueue(new Callback<ArrayList<signPojo>>() {
            @Override
            public void onResponse(Call<ArrayList<signPojo>> call, Response<ArrayList<signPojo>> response) {
                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
                Log.v("TAG",response.toString());
                *//*if (listChapterData.size() > 0) {
                    listChapterData.clear();
                }
                listChapterData = (ArrayList<signPojo>) response.body();
                adapterSubjectListAnalysis = new AdpaterLASubjectList(listChapterData, LA_SubjectListActivity.this);
                rv_subjectList.setAdapter(adapterSubjectListAnalysis);
                LocaleHelper.dismissProgressDialog();*//*
            }

            @Override
            public void onFailure(Call<ArrayList<signPojo>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();

                Log.v("TAG",t.toString());
               // LocaleHelper.dismissProgressDialog();
            }
        });*/
/*
        apiInterface.checkphonenumber(map).enqueue(new Callback<signPojo>() {
            @Override
            public void onResponse(Call<signPojo> call, Response<signPojo> response) {
                Log.v("TAG",response.toString());
                Toast.makeText(getApplicationContext(),"Success"+response.toString(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<signPojo> call, Throwable t) {
               // Toast.makeText(getApplicationContext(),"error"+t,Toast.LENGTH_SHORT).show();
                Log.v("TAG",t.toString());
            }
        });*/

    }

    private void senddatatoserver() {

        JSONObject postdetails = new JSONObject();
        try {

            postdetails.put("mobile",mobile);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (postdetails.length() > 0) {
            new getJson().execute(String.valueOf(postdetails));
        }
    }

    class getJson extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PublicHelper.showProgressDialog(PhonenumberActivity.this,"");
            }

        @Override
        protected String doInBackground(String... params) {


            String JsonResponse;
            String JsonDATA = params[0];


            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(Config.BASE_URL+Config.API_LOGIN);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);

                urlConnection.setRequestMethod(Config.METHOD_TYPE_POST);
                urlConnection.setRequestProperty(Config.CONTENT_TYPE, Config.CONTENT_JSON);
                urlConnection.setRequestProperty(Config.ACCEPT, Config.CONTENT_JSON);

                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                writer.write(JsonDATA);

                writer.close();

                InputStream inputStream = urlConnection.getInputStream();

                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputline;
                while ((inputline = reader.readLine()) != null)
                    buffer.append(inputline).append("\n");
                if (buffer.length() == 0) {
                    return null;
                }
                JsonResponse = buffer.toString();
                //Log.v("("otp response", JsonResponse);

                return JsonResponse;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        //Log.v("("TAG", "Error closing stream", e);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String JsonResponse) {
            super.onPostExecute(JsonResponse);
            if (JsonResponse == null) {
                Toast.makeText(getApplicationContext(),"Error ",Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

                  }
           else if(JsonResponse.contains("Success")){
                PublicHelper.dismissProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(JsonResponse);

                    String USERID=jsonObject.getString("user_id");
                    String message=jsonObject.getString("message");
                    String mobile=jsonObject.getString("mobile");

                    preferenceManager.setUserid(USERID);
                    preferenceManager.setMobile(mobile);

                    String name=jsonObject.getString("gym_name");
                    if(!name.equalsIgnoreCase("0")) {
                        String contact = jsonObject.getString("contact");
                        String email = jsonObject.getString("email");
                        String address = jsonObject.getString("address");
                        String website = jsonObject.getString("website");

                        preferenceManager.setGymName(name);
                        preferenceManager.setGymEmail(email);
                        preferenceManager.setGymContact(contact);
                        preferenceManager.setGymAddress(address);
                        preferenceManager.setGymWebsite(website);
                    }


                    if(message.equalsIgnoreCase("new_user")||message.equalsIgnoreCase("not_verified"))
                    {
                        Intent intent=new Intent(PhonenumberActivity.this,OTPActivity.class);
                        startActivity(intent);
                    }
                    else if(!name.equalsIgnoreCase("0"))
                    {
                        Intent intent=new Intent(PhonenumberActivity.this, BottomNavigation.class);
                        session.setLoggedin(true);
                        session.setprofile(true);
                        startActivity(intent);
                        finish();
                    }
                    else if(message.equalsIgnoreCase("old_user")) {
                        Intent intent=new Intent(PhonenumberActivity.this,CreateGymProfileActivity.class);
                        session.setLoggedin(true);
                        startActivity(intent);
                        finish();
                    }

                    else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
           else {
                PublicHelper.dismissProgressDialog();
                Toast.makeText(getApplicationContext(),""+JsonResponse,Toast.LENGTH_SHORT).show();
            }

        }

    }
}


