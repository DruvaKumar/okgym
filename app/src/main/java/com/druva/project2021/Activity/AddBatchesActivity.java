package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBatchesActivity extends AppCompatActivity {
    TextInputEditText etbatchname,etbatchlimit;

    TextView haeder,tvstarttime,tvendtime,tvsave;
    ImageView img_back,imgstart,imgend;
    TimePickerDialog timePickerDialog;
    LinearLayout lltimepicker;
    Button picktime;
    String batch_name,batch_limit,batch_start,batch_end;
    PreferenceManager preferenceManager;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_batches);

        preferenceManager=new PreferenceManager(getApplicationContext());
        session=new Session(this);

        etbatchname=findViewById(R.id.et_batchname);
        etbatchlimit=findViewById(R.id.et_batchlimit);
        tvstarttime=findViewById(R.id.et_start);
        tvendtime=findViewById(R.id.et_end);
        haeder=findViewById(R.id.header);
        imgstart=findViewById(R.id.imgstart);
        imgend=findViewById(R.id.imgend);
        tvsave=findViewById(R.id.tv_save);


        haeder=findViewById(R.id.header);
        img_back=findViewById(R.id.btn_back);

        lltimepicker=findViewById(R.id.lltimepicker);
        picktime=findViewById(R.id.done);

        tvsave.setVisibility(View.VISIBLE);

        tvsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivity(new Intent(getApplicationContext(),BatchActivity.class));
                finish();*/
                Add_batch_to_server();
            }
        });


        imgstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               final Calendar cal=Calendar.getInstance();
               int hour=cal.get(Calendar.HOUR_OF_DAY);
               int minute=cal.get(Calendar.MINUTE);

               timePickerDialog=new TimePickerDialog(AddBatchesActivity.this, new TimePickerDialog.OnTimeSetListener() {
                   @Override
                   public void onTimeSet(TimePicker timePicker, int i, int i1) {
                       String am_pm;
                       if(i>=12){
                           am_pm="PM";
                       }
                       else {
                           am_pm="AM";
                       }
                       tvstarttime.setText(i+":"+i1+" ");
                   }
               },hour,minute,true);timePickerDialog.show();

            }
        });

        imgend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int minute=cal.get(Calendar.MINUTE);

                timePickerDialog=new TimePickerDialog(AddBatchesActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        String am_pm;
                        if(i>=12){
                            am_pm="PM";
                        }
                        else {
                            am_pm="AM";
                        }
                        tvendtime.setText(i+":"+i1+" ");
                    }
                },hour,minute,true);timePickerDialog.show();

            }
        });

    }

    private void Add_batch_to_server() {
        batch_name = etbatchname.getEditableText().toString().trim();
        batch_limit = etbatchlimit.getEditableText().toString().trim();
        batch_start = tvstarttime.getEditableText().toString().trim();
        batch_end = tvendtime.getEditableText().toString().trim();


        if (batch_name.isEmpty() ) {
            etbatchname.setError("Please enter Batch Name");
        }
        else if (batch_limit.isEmpty() ) {
            etbatchlimit.setError("Please enter Batch Limit");
        }
        else if(batch_start.isEmpty()){
            tvstarttime.setError("Please set start time");
        }
        else if (batch_end.isEmpty() ) {
            tvendtime.setError("Please set end time");
        }

        else if (PublicHelper.isConnectedToInternet(getApplicationContext())) {
            PublicHelper.showProgressDialog(AddBatchesActivity.this,"");
            HashMap<String, Object> listDetails = new HashMap<>();
            listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
            listDetails.put("batch_name", batch_name);
            listDetails.put("batch_limit", batch_limit);
            listDetails.put("start_time", batch_start);
            listDetails.put("end_time", batch_end);

            Call<Object> call = RetrofitInstance.getInstance().getMyApi().addbatches(listDetails);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                    PublicHelper.dismissProgressDialog();

                    session.setbatchadded(true);

                    Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddBatchesActivity.this,BatchActivity.class));
                    finish();
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  PublicHelper.dismissProgressDialog();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

    }

}