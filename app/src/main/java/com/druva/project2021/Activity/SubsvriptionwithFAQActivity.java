package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterBatch;
import com.druva.project2021.Adapters.AdapterFAQ;
import com.druva.project2021.Adapters.AdapterSubscriptionsPlans;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.FAQ;
import com.druva.project2021.pojo.Subscription_plansDAO;
import com.druva.project2021.pojo.Subscription_plansDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubsvriptionwithFAQActivity extends AppCompatActivity {
    RecyclerView rvSubscription,rvFAQ;
    TextView imgcal,imgwatsapp,imgemail;
    ImageView btnBack;
    TextView tvDiscount,header;
    ArrayList<Subscription_plansDAO>subscriptionPlansDAO=new ArrayList<>();
    AdapterSubscriptionsPlans adapterSubscriptionsPlans;

    ArrayList<FAQ>faq=new ArrayList<>();
    AdapterFAQ adapterFAQ;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_subsvriptionwith_faqactivity);

        preferenceManager=new PreferenceManager(getApplicationContext());

        imgcal=findViewById(R.id.imgcall);
        imgwatsapp=findViewById(R.id.imgwatsapp);
        imgemail=findViewById(R.id.imgemail);
        tvDiscount=findViewById(R.id.tvdiscount);
        rvSubscription=findViewById(R.id.rvsubscriptions);
        rvFAQ=findViewById(R.id.rvfaq);

        header=findViewById(R.id.header);
        btnBack=findViewById(R.id.btn_back);

        header.setText("Subscriptions & FAQ");
        tvDiscount.setText(" Upto"+preferenceManager.getDiscount("discount")+"%"+"discount");

        rvSubscription.setLayoutManager(new LinearLayoutManager(SubsvriptionwithFAQActivity.this,
                LinearLayoutManager.VERTICAL, false));

        rvSubscription.setItemAnimator(new DefaultItemAnimator());
        getsubscriptionplans();


        rvFAQ.setLayoutManager(new LinearLayoutManager(SubsvriptionwithFAQActivity.this,
                LinearLayoutManager.VERTICAL, false));

        rvFAQ.setItemAnimator(new DefaultItemAnimator());
        getfaqs();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgcal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:"+preferenceManager.getGymContact("gymcontact")));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(SubsvriptionwithFAQActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions((Activity) SubsvriptionwithFAQActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                }else {     //have got permission
                    try{
                        SubsvriptionwithFAQActivity.this.startActivity(callIntent);  //call activity and make phone call
                    }
                    catch (android.content.ActivityNotFoundException ex){
                        Toast.makeText(SubsvriptionwithFAQActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void getsubscriptionplans() {
        PublicHelper.showProgressDialog(SubsvriptionwithFAQActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();

        Call<ArrayList<Subscription_plansDAO>> call = RetrofitInstance.getInstance().getMyApi().getsubscriptionplans(listDetails);
        call.enqueue(new Callback<ArrayList<Subscription_plansDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<Subscription_plansDAO>> call, Response<ArrayList<Subscription_plansDAO>> response) {
                if (subscriptionPlansDAO.size() > 0) {
                    subscriptionPlansDAO.clear();
                }
                subscriptionPlansDAO = (ArrayList<Subscription_plansDAO>) response.body();
                adapterSubscriptionsPlans = new AdapterSubscriptionsPlans(subscriptionPlansDAO, SubsvriptionwithFAQActivity.this);
                rvSubscription.setAdapter(adapterSubscriptionsPlans);
                PublicHelper.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ArrayList<Subscription_plansDAO>> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

            }
        });

    }

    private void getfaqs() {
       // PublicHelper.showProgressDialog(SubsvriptionwithFAQActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();

        Call<ArrayList<FAQ>> call = RetrofitInstance.getInstance().getMyApi().getfaq(listDetails);
        call.enqueue(new Callback<ArrayList<FAQ>>() {

            @Override
            public void onResponse(Call<ArrayList<FAQ>> call, Response<ArrayList<FAQ>> response) {
                if (faq.size() > 0) {
                    faq.clear();
                }
                faq = (ArrayList<FAQ>) response.body();
                adapterFAQ = new AdapterFAQ(faq, SubsvriptionwithFAQActivity.this);
                rvFAQ.setAdapter(adapterFAQ);
                PublicHelper.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ArrayList<FAQ>> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}