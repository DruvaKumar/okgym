package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPlansActivity extends AppCompatActivity {

    TextInputEditText etPlanname,etplanfees,etduration,etplandetails;
    RadioGroup group;
    RadioButton rbtn_days,rbtn_month,planduration;
    TextView haeder,tvsave;
    ImageView img_back;
    String plandays;
    String splansname,splanfee,splanday,splanduration,splandetails,splanmonths;
    PreferenceManager preferenceManager;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_plans);

        preferenceManager=new PreferenceManager(getApplicationContext());
        session=new Session(this);
        etPlanname=findViewById(R.id.et_planname);
        etplanfees=findViewById(R.id.et_fees);
        rbtn_days=findViewById(R.id.rbtnplandays);
        rbtn_month=findViewById(R.id.rbtnpmonth);
        group=findViewById(R.id.plangroup);
        etduration=findViewById(R.id.et_duration);
        etplandetails=findViewById(R.id.et_plandetails);
        tvsave=findViewById(R.id.tv_save);


        haeder=findViewById(R.id.header);
        img_back=findViewById(R.id.btn_back);

        tvsave.setVisibility(View.VISIBLE);

        tvsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivity(new Intent(getApplicationContext(),PlansActivity.class));
                finish();*/
                Addaplanstoserver();
            }
        });


        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                int selctedid=group.getCheckedRadioButtonId();
                planduration=findViewById(selctedid);
                plandays=planduration.getText().toString().trim();
                //Toast.makeText(AddmemberActivity.this,gender.getText().toString(),Toast.LENGTH_SHORT).show();
                if(plandays.equalsIgnoreCase("Days")){
                    plandays="1";
                    splanmonths="0";
                }
                else {
                    splanmonths="1";
                    plandays="0";
                }
            }
        });
    }

    private void Addaplanstoserver() {
        splansname = etPlanname.getEditableText().toString().trim();
        splanfee = etplanfees.getEditableText().toString().trim();
        splanday = plandays;
        splanduration = etduration.getEditableText().toString().trim();
        splandetails = etplandetails.getEditableText().toString().trim();


        if (splansname.isEmpty() || splansname.length() <= 2) {
            etPlanname.setError("Please enter Plan Name");
        }
        else if (splanfee.isEmpty() ) {
            etplanfees.setError("Please enter Plan fee");
        }
        else if(splanday.isEmpty()){
           Toast.makeText(AddPlansActivity.this,"Please select Days / Month",Toast.LENGTH_SHORT).show();
        }
        else if (splanduration.isEmpty() ) {
            etduration.setError("Please enter plan duration");
        }

        else if (splandetails.isEmpty() ) {
            etduration.setError("PLease enter plan details");
        }
         else if (PublicHelper.isConnectedToInternet(getApplicationContext())) {
            PublicHelper.showProgressDialog(AddPlansActivity.this,"");
            HashMap<String, Object> listDetails = new HashMap<>();
            listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));
            listDetails.put("plan_name", splansname);
            listDetails.put("feese", splanfee);
            listDetails.put("days", splanday);
            listDetails.put("months", splanmonths);
            listDetails.put("duration", splanduration);
            listDetails.put("plan_details", splandetails);

            Call<Object> call = RetrofitInstance.getInstance().getMyApi().addplans(listDetails);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    PublicHelper.dismissProgressDialog();

                    session.setplanadded(true);

                    Toast.makeText(getApplicationContext(), "Succesfully added", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddPlansActivity.this,PlansActivity.class));
                    finish();
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

    }
}