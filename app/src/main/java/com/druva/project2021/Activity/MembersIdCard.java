package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.gkemon.XMLtoPDF.PdfGenerator;
import com.gkemon.XMLtoPDF.PdfGeneratorListener;
import com.gkemon.XMLtoPDF.model.FailureResponse;
import com.gkemon.XMLtoPDF.model.SuccessResponse;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class MembersIdCard extends AppCompatActivity {

    TextView id,name,mobile,joined,gymname,gymcontact,gymaddress,header,download,share,chat;
    CircleImageView profile;
    ImageView memberqrcode,back;
    PreferenceManager preferenceManager;
    MembersOverallDAO MembersOverallDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.member_id_card);
        preferenceManager=new PreferenceManager(getApplicationContext());

        Intent i=getIntent();
        Bundle b=i.getExtras();
        MembersOverallDAO= (MembersOverallDAO) b.getSerializable("user");



        id=findViewById(R.id.tvmemid);
        name=findViewById(R.id.tvmembername);
        mobile=findViewById(R.id.tvmemmobile);
        joined=findViewById(R.id.tvjoindate);
        gymname=findViewById(R.id.gymname);
        gymcontact=findViewById(R.id.gymcontact);
        gymaddress=findViewById(R.id.gymaddress);
        profile=findViewById(R.id.ivprofile);
        memberqrcode=findViewById(R.id.qrcode);
        header=findViewById(R.id.header);
        back=findViewById(R.id.btn_back);

        download=findViewById(R.id.ivdownload);
        share=findViewById(R.id.ivshare);
        chat=findViewById(R.id.ivchat);
        header.setText("Member ID Card");

        name.setText(MembersOverallDAO.getName());
        mobile.setText(MembersOverallDAO.getMobile());
        joined.setText("Joined on :"+MembersOverallDAO.getJoindate());
        id.setText("Member ID : " + MembersOverallDAO.getMember_id());

        gymname.setText(preferenceManager.getGymName("gymname"));
        gymcontact.setText(preferenceManager.getGymContact("gymcontact"));
        gymaddress.setText(preferenceManager.getGymAddress("gymaddress"));

        Glide.with(MembersIdCard.this).load(MembersOverallDAO.getProfilepic())
                .thumbnail(0.5f)
                .into(profile);

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                download_card();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share_id_card();
            }
        });

        /*Glide.with(MembersIdCard.this).load(MembersOverallDAO.getQr_code())
                .thumbnail(0.5f)
                .into(memberqrcode);*/
        try {
            Bitmap bitmap = textToImage(MembersOverallDAO.getMember_id(), 500, 500);
            Log.v("TAG","qrcode is"+bitmap);
            memberqrcode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    private void share_id_card() {
        File pdfFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        String path= "Gym_member_"+MembersOverallDAO.getMember_id()+".pdf";

        File myFile = new File(pdfFolder.getAbsolutePath() + File.separator + path);

        Intent intentShareFile = new Intent();
        intentShareFile.setAction(Intent.ACTION_SEND);
        if(myFile.exists()) {
            intentShareFile.setType("application/pdf");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(myFile));

            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing File from Webkul...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File from Webkul to purchase items...");

            startActivity(Intent.createChooser(intentShareFile, "Share File Webkul"));
        }
    }

    private void download_card() {

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View content = inflater.inflate(R.layout.printable_id_card, null);

        id=content.findViewById(R.id.tvmemid);
        name=content.findViewById(R.id.tvmembername);
        mobile=content.findViewById(R.id.tvmemmobile);
        joined=content.findViewById(R.id.tvjoindate);
        gymname=content.findViewById(R.id.gymname);
        gymcontact=content.findViewById(R.id.gymcontact);
        gymaddress=content.findViewById(R.id.gymaddress);
        profile=content.findViewById(R.id.ivprofile);
        memberqrcode=content.findViewById(R.id.qrcode);
        header=content.findViewById(R.id.header);
        back=content.findViewById(R.id.btn_back);
        ImageView gym=content.findViewById(R.id.gname);


        name.setText(MembersOverallDAO.getName());
        mobile.setText(MembersOverallDAO.getMobile());
        joined.setText("Joined on :"+MembersOverallDAO.getJoindate());
        id.setText("Member ID : " + MembersOverallDAO.getMember_id());

        gymname.setText(preferenceManager.getGymName("gymname"));
        gymcontact.setText(preferenceManager.getGymContact("gymcontact"));
        gymaddress.setText(preferenceManager.getGymAddress("gymaddress"));

        Glide.with(MembersIdCard.this).load(MembersOverallDAO.getProfilepic())
                .thumbnail(0.5f)
                .into(profile);


        try {
            Bitmap bitmap = textToImage(MembersOverallDAO.getMember_id(), 500, 500);
            Log.v("TAG","qrcode is"+bitmap);
            memberqrcode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        PdfGenerator.getBuilder()
                .setContext(MembersIdCard.this)
                .fromViewSource()
                .fromView(content)
                .setFileName("Gym_member_"+MembersOverallDAO.getMember_id())
                .setFolderName("IDCards")
                .openPDFafterGeneration(true)
                .build(new PdfGeneratorListener() {
                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        super.onFailure(failureResponse);
                    }
                    @Override
                    public void onStartPDFGeneration() {

                    }
                    @Override
                    public void onFinishPDFGeneration() {

                    }
                    @Override
                    public void showLog(String log) {
                        super.showLog(log);
                    }

                    @Override
                    public void onSuccess(SuccessResponse response) {
                        super.onSuccess(response);
                    }
                });
    }


    private Bitmap textToImage(String text, int width, int height) throws WriterException, NullPointerException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.DATA_MATRIX.QR_CODE,
                    width, height, null);
        } catch (IllegalArgumentException Illegalargumentexception) {
            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        int colorWhite = 0xFFFFFFFF;
        int colorBlack = 0xFF000000;

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
}