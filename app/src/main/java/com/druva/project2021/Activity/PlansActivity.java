package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterPlans;
import com.druva.project2021.Adapters.MembersAdapter;
import com.druva.project2021.BottomNavigation;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.PlansDAO;
import com.druva.project2021.pojo.PlansDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.druva.project2021.ui.home.HomeFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlansActivity extends AppCompatActivity {

    RecyclerView rvplans;
    FloatingActionButton float_addplans;
    ImageView img_back;
    TextView Header,save;
    PreferenceManager preferenceManager;
    ArrayList<PlansDAO>planslist=new ArrayList<>();
    AdapterPlans adapterPlans;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_plans);
        preferenceManager=new PreferenceManager(getApplicationContext());

        rvplans=findViewById(R.id.rvPlans);
        float_addplans=findViewById(R.id.floatAddplan);
        Header=findViewById(R.id.header);
        img_back=findViewById(R.id.btn_back);
        save=findViewById(R.id.tv_save);

        Header.setText("Plans");

        rvplans.setLayoutManager(new LinearLayoutManager(PlansActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getplanslist(preferenceManager.getUserid(Config.USERID));
        rvplans.setItemAnimator(new DefaultItemAnimator());

        float_addplans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PlansActivity.this, AddPlansActivity.class);
                startActivity(intent);
                finish();

            }
        });


    }

    private void getplanslist(String userid) {
        PublicHelper.showProgressDialog(PlansActivity.this,"");

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("gym_id", userid);

        Call<ArrayList<PlansDAO>> call = RetrofitInstance.getInstance().getMyApi().get_all_plans(listDetails);
        call.enqueue(new Callback<ArrayList<PlansDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<PlansDAO>> call, Response<ArrayList<PlansDAO>> response) {
                if (planslist.size() > 0) {
                    planslist.clear();
                }
                planslist = (ArrayList<PlansDAO>) response.body();
                if (planslist.size() > 0) {
                    adapterPlans = new AdapterPlans(planslist, PlansActivity.this);
                    rvplans.setAdapter(adapterPlans);
                    PublicHelper.dismissProgressDialog();
                }
                else {
                    PublicHelper.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PlansDAO>> call, Throwable t) {
               // Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();

                PublicHelper.dismissProgressDialog();

            }
        });

    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        startActivity(new Intent(PlansActivity.this, BottomNavigation.class));
        finish();
    }
}
