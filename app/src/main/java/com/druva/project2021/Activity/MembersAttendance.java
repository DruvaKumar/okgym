package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterMembersAttendance;
import com.druva.project2021.Adapters.MembersAdapter;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.pojo.Member_AttendanceDAO;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembersAttendance extends AppCompatActivity {
    TextView header;
    ImageView back;
    RecyclerView rvmemberattendance;
    String memberid;
    PreferenceManager preferenceManager;
   // ArrayList<Member_AttendanceDAO>attendanceDAO;

    ArrayList<Member_AttendanceDAO> membersattedancelist=new ArrayList<>();
    AdapterMembersAttendance adapterMemberAttendance;

    MembersOverallDAO MembersOverallDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_members_attendance);

        preferenceManager=new PreferenceManager(getApplicationContext());


        Intent i=getIntent();
        Bundle b=i.getExtras();
        MembersOverallDAO= (MembersOverallDAO) b.getSerializable("user");

        header=findViewById(R.id.header);
        back=findViewById(R.id.btn_back);
        rvmemberattendance=findViewById(R.id.rvmemberattenance);


        header.setText("Member Name : "+MembersOverallDAO.getName());
        memberid=MembersOverallDAO.getMember_id();

        rvmemberattendance.setLayoutManager(new LinearLayoutManager(MembersAttendance.this,
                LinearLayoutManager.VERTICAL, false));

        rvmemberattendance.setItemAnimator(new DefaultItemAnimator());

        getattemdancereport(memberid);
    }

    private void getattemdancereport(String memberid) {
        PublicHelper.showProgressDialog(MembersAttendance.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));
        listDetails.put("member_id", memberid);

        Call<ArrayList<Member_AttendanceDAO>> call = RetrofitInstance.getInstance().getMyApi().getmemberattendance(listDetails);
        call.enqueue(new Callback<ArrayList<Member_AttendanceDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<Member_AttendanceDAO>> call, Response<ArrayList<Member_AttendanceDAO>> response) {
                if (membersattedancelist.size() > 0) {
                    membersattedancelist.clear();
                }
                Log.v("TAG","attendance "+response.body());

                membersattedancelist = (ArrayList<Member_AttendanceDAO>) response.body();
                adapterMemberAttendance = new AdapterMembersAttendance(membersattedancelist, MembersAttendance.this);
                rvmemberattendance.setAdapter(adapterMemberAttendance);
                PublicHelper.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ArrayList<Member_AttendanceDAO>> call, Throwable t) {
                PublicHelper.dismissProgressDialog();
                Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();


            }
        });
    }
}