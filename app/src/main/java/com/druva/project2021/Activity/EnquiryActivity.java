package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterEnquiry;
import com.druva.project2021.Adapters.AdapterPlans;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.pojo.EnquiryDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnquiryActivity extends AppCompatActivity {
    TextView tvlayoutName,totalenquiry,totaljoined;
    ImageView ivdropdown,ivorder,ivsearch;
    RecyclerView rvenquiry;
    FloatingActionButton float_addenquiry;
    ImageView img_back;
    //TextView Header,save;
    PreferenceManager preferenceManager;
    ArrayList<EnquiryDAO> enquirylist=new ArrayList<>();
    AdapterEnquiry adapterEnquiry;
    Session session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_enquiry);
        preferenceManager=new PreferenceManager(getApplicationContext());
        session=new Session(this);
        
        rvenquiry=findViewById(R.id.rvEnquiry);
        float_addenquiry=findViewById(R.id.floatAddenquiry);
        tvlayoutName=findViewById(R.id.tvHeading);
        ivdropdown=findViewById(R.id.imgdropdown);
        ivorder=findViewById(R.id.imgorder);
        ivsearch=findViewById(R.id.img_search);

        totalenquiry=findViewById(R.id.tvtotalenquiry);
        totaljoined=findViewById(R.id.tvtotaljoined);

        tvlayoutName.setText("Today");


        rvenquiry.setLayoutManager(new LinearLayoutManager(EnquiryActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getEnquirylist(preferenceManager.getUserid(Config.USERID));
        rvenquiry.setItemAnimator(new DefaultItemAnimator());

        float_addenquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(EnquiryActivity.this, AddEnquiryActivity.class);
                startActivity(intent);
                finish();*/
                if(session.isplanadded()) {
                    Intent intent = new Intent(EnquiryActivity.this, AddEnquiryActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(EnquiryActivity.this,"Please add a Plan to continue ",Toast.LENGTH_LONG).show();

                }
            }
        });
    }
private void getEnquirylist(String userid) {
    PublicHelper.showProgressDialog(EnquiryActivity.this,"");
    HashMap<String, Object> listDetails = new HashMap<>();
    listDetails.put("gym_id", userid);

    Call<ArrayList<EnquiryDAO>> call = RetrofitInstance.getInstance().getMyApi().get_all_enquiry(listDetails);
    call.enqueue(new Callback<ArrayList<EnquiryDAO>>() {

        @Override
        public void onResponse(Call<ArrayList<EnquiryDAO>> call, Response<ArrayList<EnquiryDAO>> response) {
            if (enquirylist.size() > 0) {
                enquirylist.clear();
            }

            enquirylist = (ArrayList<EnquiryDAO>) response.body();
            adapterEnquiry = new AdapterEnquiry(enquirylist, EnquiryActivity.this);
            rvenquiry.setAdapter(adapterEnquiry);
            totalenquiry.setText("Total Enquiry : "+enquirylist.size());
            totaljoined.setText("Total Joined : "+preferenceManager.getNewjoin("newjoin"));
            PublicHelper.dismissProgressDialog();
        }

        @Override
        public void onFailure(Call<ArrayList<EnquiryDAO>> call, Throwable t) {
          //  Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            PublicHelper.dismissProgressDialog();

        }
    });

}


}