package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.R;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class OTPActivity extends AppCompatActivity {
    TextView tvPhonenumber,tvTimer;
    TextInputEditText etOtp;
    ImageView Edit;
    Button submit;
    String otp;
    Session session;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpactivity);

        session=new Session(this);
        preferenceManager=new PreferenceManager(getApplicationContext());

        tvPhonenumber=findViewById(R.id.tv_phone);
        tvTimer=findViewById(R.id.tv_timer);
        Edit=findViewById(R.id.img_edit);
        submit=findViewById(R.id.btn_verify);
        etOtp=findViewById(R.id.et_otp);

        tvPhonenumber.setText(preferenceManager.getMobile("mobile"));

        starttimer();


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyotp();
            }
        });

    }

    private void starttimer() {
        new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long l) {

                NumberFormat nf=new DecimalFormat("00");
                long min=(l/60000) % 60;
                long sec=(l/1000)%60;

                tvTimer.setText(nf.format(min) + ":"+ nf.format(sec));

            }

            @Override
            public void onFinish() {
                tvTimer.setText("Resend");

            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void verifyotp() {

        otp=etOtp.getEditableText().toString().trim();

        JSONObject postdetails = new JSONObject();
        try {
            postdetails.put(Config.USERID,preferenceManager.getUserid(Config.USERID));
            postdetails.put("otp",otp);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (postdetails.length() > 0) {
            new sendotptoverify().execute(String.valueOf(postdetails));
        }
    }

    class sendotptoverify extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PublicHelper.showProgressDialog(OTPActivity.this,"");
        }

        @Override
        protected String doInBackground(String... params) {
            String JsonResponse;
            String JsonDATA = params[0];


            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(Config.BASE_URL+Config.API_OTP);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);

                urlConnection.setRequestMethod(Config.METHOD_TYPE_POST);
                urlConnection.setRequestProperty(Config.CONTENT_TYPE, Config.CONTENT_JSON);
                urlConnection.setRequestProperty(Config.ACCEPT, Config.CONTENT_JSON);

                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                writer.write(JsonDATA);

                writer.close();

                InputStream inputStream = urlConnection.getInputStream();

                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputline;
                while ((inputline = reader.readLine()) != null)
                    buffer.append(inputline).append("\n");
                if (buffer.length() == 0) {
                    return null;
                }
                JsonResponse = buffer.toString();
                //Log.v("("otp response", JsonResponse);

                return JsonResponse;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        //Log.v("("TAG", "Error closing stream", e);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String JsonResponse) {
            super.onPostExecute(JsonResponse);
            if (JsonResponse == null) {
                PublicHelper.dismissProgressDialog();
                Toast.makeText(getApplicationContext(),"Error Loading data.. ",Toast.LENGTH_SHORT).show();

            }
            else if(JsonResponse.contains("Success")){
                PublicHelper.dismissProgressDialog();
                Intent intent=new Intent(OTPActivity.this,CreateGymProfileActivity.class);
                session.setLoggedin(true);
                intent.putExtra("from", "phone");
                startActivity(intent);
                finish();
            }
            else if(JsonResponse.contains("Fail")){
                PublicHelper.dismissProgressDialog();
                Toast.makeText(getApplicationContext(),"Plaese Re-check your OTP",Toast.LENGTH_SHORT).show();
            }

        }

    }
}