package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.BottomNavigation;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.R;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMemberDocumentsActivity extends AppCompatActivity {
    ImageView imgdoc1, imgdoc2, imhback;
    TextView header, save, skip;
    File docfile1,docfile2;
    PreferenceManager preferenceManager;

    Uri picUri;
    Bitmap myBitmap;
    private static final int GALLERY = 1;
    private static final int GALLERY2 = 2;
    private static Bitmap Image = null;
    private static Bitmap rotateImage = null;
    private static final int PERMISSION_REQUEST_CODE = 1;
    String memberid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_member_documents);

        preferenceManager=new PreferenceManager(getApplicationContext());

        imgdoc1 = findViewById(R.id.imgdoc1);
        imgdoc2 = findViewById(R.id.imgdoc2);
        imhback = findViewById(R.id.btn_back);

        header = findViewById(R.id.header);
        save = findViewById(R.id.tv_save);
        skip = findViewById(R.id.skip);

        save.setVisibility(View.VISIBLE);

        header.setText("Add Documents");





        imgdoc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {

                    ActivityCompat.requestPermissions(AddMemberDocumentsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 200);

                }

                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent.createChooser(intent,"Select File"),GALLERY);


            }
        });

        imgdoc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {

                    ActivityCompat.requestPermissions(AddMemberDocumentsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 200);

                }

                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent.createChooser(intent,"Select File"),GALLERY2);


            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddMemberDocumentsActivity.this, BottomNavigation.class));
                finish();

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* startActivity(new Intent(AddMemberDocumentsActivity.this, BottomNavigation.class));
                finish();*/

                 if(docfile1==null || docfile2==null){
                    Toast.makeText(getApplicationContext(), "Please add both documents", Toast.LENGTH_SHORT).show();
                }
                 else {
                     Add_documnets_to_member_profile();
                 }
            }
        });

    }

    private void Add_documnets_to_member_profile() {

        if(PublicHelper.isConnectedToInternet(AddMemberDocumentsActivity.this)) {
            PublicHelper.showProgressDialog(AddMemberDocumentsActivity.this,"");
            File file1 = new File(String.valueOf(docfile1));

            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
            MultipartBody.Part imageBody1 =
                    MultipartBody.Part.createFormData("doc1", file1.getName(), requestFile1);

            File file2 = new File(String.valueOf(docfile2));

            RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
            MultipartBody.Part imageBody2 =
                    MultipartBody.Part.createFormData("doc2", file1.getName(), requestFile2);



            RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getUserid(Config.USERID));
          //  RequestBody member_id = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getMemberId("memberid"));
            RequestBody member_id = RequestBody.create(MediaType.parse("text/plain"), preferenceManager.getMemberId("memberid"));


            Call<Void> call = RetrofitInstance.getInstance().getMyApi().uploadmemberdocuments(imageBody1, imageBody2, userid,member_id);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Success" , Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddMemberDocumentsActivity.this, BottomNavigation.class));
                    finish();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    PublicHelper.dismissProgressDialog();
                    Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  }
            });
        }
        else {
            Toast.makeText(getApplicationContext(), "Please Connect to internet" , Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,requestCode,data);

        if(resultCode== Activity.RESULT_OK){
            if(requestCode==GALLERY) {
                Uri seletecdImageUri = data.getData();

                String profilepath = getPath(seletecdImageUri);
                Log.v("TAG", "path " + profilepath);
                docfile1 = new File(profilepath);
                imgdoc1.setImageURI(seletecdImageUri);
            }
            else if(requestCode==GALLERY2){
                Uri seletecdImageUri2 = data.getData();

                String profilepath2 = getPath(seletecdImageUri2);
                Log.v("TAG", "path " + profilepath2);
                docfile2 = new File(profilepath2);
                imgdoc2.setImageURI(seletecdImageUri2);

            }
        }
    }
    public String getPath(Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

}