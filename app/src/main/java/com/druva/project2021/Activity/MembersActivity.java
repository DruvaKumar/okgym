package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.MembersAdapter;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;


import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembersActivity extends AppCompatActivity {
    TextView tvlayoutName,nodata;
    ImageView ivdropdown,ivorder,ivsearch;
    RecyclerView rvLivememmbers;

    MembersAdapter membersAdapter;
    ArrayList<MembersOverallDAO> memberslist;
    PreferenceManager preferenceManager;

    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_live_members);
        
        preferenceManager=new PreferenceManager(getApplicationContext());
        memberslist = new ArrayList<>();

        tvlayoutName=findViewById(R.id.tvHeading);
        ivdropdown=findViewById(R.id.imgdropdown);
        ivorder=findViewById(R.id.imgorder);
        ivsearch=findViewById(R.id.img_search);
        rvLivememmbers=findViewById(R.id.rvlistdetails);
        nodata=findViewById(R.id.nodata);

        tvlayoutName.setText("Live Members");

        rvLivememmbers.setLayoutManager(new LinearLayoutManager(MembersActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getmembrslist(preferenceManager.getUserid(Config.USERID));
        rvLivememmbers.setItemAnimator(new DefaultItemAnimator());

    }

    private void getmembrslist(String userid) {
        PublicHelper.showProgressDialog(MembersActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("gym_id", userid);

        Call<ArrayList<MembersOverallDAO>> call = RetrofitInstance.getInstance().getMyApi().get_all_members(listDetails);
        call.enqueue(new Callback<ArrayList<MembersOverallDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<MembersOverallDAO>> call, Response<ArrayList<MembersOverallDAO>> response) {
                if (memberslist.size() > 0) {
                    memberslist.clear();
                }
                memberslist = (ArrayList<MembersOverallDAO>) response.body();
                membersAdapter = new MembersAdapter(memberslist, MembersActivity.this);
                rvLivememmbers.setAdapter(membersAdapter);
                if(memberslist.size()>0){
                    rvLivememmbers.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();

                }
                else {
                    nodata.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<MembersOverallDAO>> call, Throwable t) {
                nodata.setVisibility(View.VISIBLE);
                PublicHelper.dismissProgressDialog();

               // Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //here
        getmembrslist(preferenceManager.getUserid(Config.USERID));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}