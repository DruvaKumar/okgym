package com.druva.project2021.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterBatch;
import com.druva.project2021.Adapters.AdapterPlans;
import com.druva.project2021.BottomNavigation;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.BatchDAO;
import com.druva.project2021.pojo.BatchDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BatchActivity extends AppCompatActivity {

    RecyclerView rvbatches;
    FloatingActionButton float_addbatches;
    ImageView img_back;
    TextView Header,save;
    PreferenceManager preferenceManager;
    ArrayList<BatchDAO> batchlist=new ArrayList<>();
    AdapterBatch adapterBatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_batch);
        preferenceManager=new PreferenceManager(getApplicationContext());

        rvbatches=findViewById(R.id.rvBatch);
        float_addbatches=findViewById(R.id.floatAddbatch);
        Header=findViewById(R.id.header);
        img_back=findViewById(R.id.btn_back);
        save=findViewById(R.id.tv_save);

        save.setVisibility(View.GONE);
        Header.setText("Batch");
        float_addbatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BatchActivity.this, AddBatchesActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rvbatches.setLayoutManager(new LinearLayoutManager(BatchActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getbatchlist(preferenceManager.getUserid(Config.USERID));
        rvbatches.setItemAnimator(new DefaultItemAnimator());

    }

    private void getbatchlist(String userid) {
        PublicHelper.showProgressDialog(BatchActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("gym_id", userid);

        Call<ArrayList<BatchDAO>> call = RetrofitInstance.getInstance().getMyApi().get_all_batch(listDetails);
        call.enqueue(new Callback<ArrayList<BatchDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<BatchDAO>> call, Response<ArrayList<BatchDAO>> response) {
                if (batchlist.size() > 0) {
                    batchlist.clear();
                }
                batchlist = (ArrayList<BatchDAO>) response.body();
                if(batchlist.size()>0) {
                    adapterBatch = new AdapterBatch(batchlist, BatchActivity.this);
                    rvbatches.setAdapter(adapterBatch);
                    PublicHelper.dismissProgressDialog();
                }
                else {
                    PublicHelper.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BatchDAO>> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                PublicHelper.dismissProgressDialog();

            }
        });

    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        startActivity(new Intent(BatchActivity.this, BottomNavigation.class));
        finish();
    }
    }
