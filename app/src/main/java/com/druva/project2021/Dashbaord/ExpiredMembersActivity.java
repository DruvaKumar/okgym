package com.druva.project2021.Dashbaord;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.AdapterLiveMembersAdapter;
import com.druva.project2021.Adapters.MembersAdapter;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpiredMembersActivity extends AppCompatActivity {
    TextView tvlayoutName,nodata;
    ImageView ivdropdown,ivorder,ivsearch;
    RecyclerView rvExpiredmemmbers;

    ArrayList<MembersOverallDAO> memberslist=new ArrayList<>();
    MembersAdapter membersAdapter;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_live_members);

        tvlayoutName=findViewById(R.id.tvHeading);
        ivdropdown=findViewById(R.id.imgdropdown);
        ivorder=findViewById(R.id.imgorder);
        ivsearch=findViewById(R.id.img_search);
        rvExpiredmemmbers=findViewById(R.id.rvlistdetails);
        nodata=findViewById(R.id.nodata);


        tvlayoutName.setText("Expired Members");
        preferenceManager=new PreferenceManager(getApplicationContext());
        rvExpiredmemmbers.setLayoutManager(new LinearLayoutManager(ExpiredMembersActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getmembrslist(preferenceManager.getUserid(Config.USERID));
        rvExpiredmemmbers.setItemAnimator(new DefaultItemAnimator());

    }

    private void getmembrslist(String userid) {
        PublicHelper.showProgressDialog(ExpiredMembersActivity.this,"");
        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", userid);

        Call<ArrayList<MembersOverallDAO>> call = RetrofitInstance.getInstance().getMyApi().get_expired_members(listDetails);
        call.enqueue(new Callback<ArrayList<MembersOverallDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<MembersOverallDAO>> call, Response<ArrayList<MembersOverallDAO>> response) {
                if (memberslist.size() > 0) {
                    memberslist.clear();
                }
                memberslist = (ArrayList<MembersOverallDAO>) response.body();
                membersAdapter = new MembersAdapter(memberslist, ExpiredMembersActivity.this);
                rvExpiredmemmbers.setAdapter(membersAdapter);

                if(memberslist.size()>0) {
                    rvExpiredmemmbers.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
                else {
                    nodata.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MembersOverallDAO>> call, Throwable t) {
                nodata.setVisibility(View.VISIBLE);
                PublicHelper.dismissProgressDialog();
              // Toast.makeText(getApplicationContext(), "Something went wrong " , Toast.LENGTH_SHORT).show();
                  

            }
        });
    }
}