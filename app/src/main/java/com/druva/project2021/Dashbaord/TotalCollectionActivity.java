package com.druva.project2021.Dashbaord;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.project2021.Adapters.Adapter_total_collection;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.PublicHelper;
import com.druva.project2021.R;
import com.druva.project2021.pojo.MembersOverallDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TotalCollectionActivity extends AppCompatActivity {

    TextView tvDay,tvFilter,tvcollection,nodata;
    RecyclerView rvcollectionlist;
    PreferenceManager preferenceManager;
    Adapter_total_collection adapter_total_collection;

    ArrayList<MembersOverallDAO>membersOverallDAO=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_total_collection);
        preferenceManager=new PreferenceManager(getApplicationContext());

        tvDay=findViewById(R.id.tvday);
        tvFilter=findViewById(R.id.tvfilter);
        tvcollection=findViewById(R.id.tvtotalcollection);
        rvcollectionlist=findViewById(R.id.rvcollection);
        nodata=findViewById(R.id.nodata);

        rvcollectionlist.setLayoutManager(new LinearLayoutManager(TotalCollectionActivity.this,
                LinearLayoutManager.VERTICAL, false));
        getcollectionlist();
        rvcollectionlist.setItemAnimator(new DefaultItemAnimator());


    }

    private void getcollectionlist() {
        PublicHelper.showProgressDialog(TotalCollectionActivity.this,"");


        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put("userid", preferenceManager.getUserid(Config.USERID));

        Call<ArrayList<MembersOverallDAO>> call = RetrofitInstance.getInstance().getMyApi().get_collection_data(listDetails);
        call.enqueue(new Callback<ArrayList<MembersOverallDAO>>() {

            @Override
            public void onResponse(Call<ArrayList<MembersOverallDAO>> call, Response<ArrayList<MembersOverallDAO>> response) {
                if (membersOverallDAO.size() > 0) {
                    membersOverallDAO.clear();
                }
                membersOverallDAO = (ArrayList<MembersOverallDAO>) response.body();
                adapter_total_collection = new Adapter_total_collection(membersOverallDAO, TotalCollectionActivity.this);
                rvcollectionlist.setAdapter(adapter_total_collection);
                if(membersOverallDAO.size()>0) {
                    rvcollectionlist.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
                else {
                    nodata.setVisibility(View.VISIBLE);
                    PublicHelper.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MembersOverallDAO>> call, Throwable t) {
                nodata.setVisibility(View.VISIBLE);
                PublicHelper.dismissProgressDialog();
               // Toast.makeText(getApplicationContext(), "Something went wrong ", Toast.LENGTH_SHORT).show();

            }
        });
    }
}