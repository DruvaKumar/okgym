package com.druva.project2021.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.druva.project2021.Activity.AddmemberActivity;
import com.druva.project2021.Activity.AttendancActivity;
import com.druva.project2021.Activity.BatchActivity;
import com.druva.project2021.Activity.EnquiryActivity;
import com.druva.project2021.Activity.MembersActivity;
import com.druva.project2021.Activity.PlansActivity;
import com.druva.project2021.Activity.SMSTemplateActivity;
import com.druva.project2021.Activity.ScannerActivity;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.Helpers.Session;
import com.druva.project2021.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
private FragmentHomeBinding binding;
Session session;
PreferenceManager preferenceManager;

CardView cardscan,cardaddmembers,cardmembers,cardplans,cardattendance,cardtemplate,cardbatch,carduse,cardenquiry,cardreport;
TextView tvWatsnew,tvfeatures;
    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

    binding = FragmentHomeBinding.inflate(inflater, container, false);
    View root = binding.getRoot();
    session=new Session(getActivity());
    preferenceManager=new PreferenceManager(getActivity());

        cardscan=binding.cardscan;
        cardaddmembers=binding.cardaddmem;
        cardmembers=binding.cardmembers;
        cardplans=binding.cardplans;
        cardattendance=binding.cardattendance;
        cardtemplate=binding.cardtemplate;
        cardbatch=binding.cardbatch;
        carduse=binding.cardutube;
        cardenquiry=binding.cardenquiry;
        cardreport=binding.cardreports;
        tvWatsnew=binding.tvwhatsnew;
        tvfeatures=binding.tvfeatures;

      //  String featur= String.valueOf(Html.fromHtml(preferenceManager.getNewFeatures("features"),0));

       // tvfeatures.setText(HtmlCompat.fromHtml(preferenceManager.getNewFeatures("features"), 0));


    cardscan.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent(getActivity(),ScannerActivity.class);
            startActivity(intent);
        }
    });
        cardaddmembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if(!session.isbatchadded()){
                    Toast.makeText(getActivity(),"Please create a Batch and then add members",Toast.LENGTH_LONG).show();

                }
                 else if(!session.isplanadded()){
                    Toast.makeText(getActivity(),"Please add a Plan to continue ",Toast.LENGTH_LONG).show();
                }

                else {
                    Intent intent=new Intent(getActivity(), AddmemberActivity.class);
                    startActivity(intent);
                }
               /* Intent intent=new Intent(getActivity(), AddmemberActivity.class);
                startActivity(intent);*/

            }
        });
        cardmembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MembersActivity.class);
                startActivity(intent);
            }
        });
        cardplans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), PlansActivity.class);
                startActivity(intent);
            }
        });
        cardattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), AttendancActivity.class);
                startActivity(intent);
            }
        });
        cardtemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SMSTemplateActivity.class);
                startActivity(intent);
            }
        });
        cardbatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), BatchActivity.class);
                startActivity(intent);
            }
        });
        carduse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"YouTube",Toast.LENGTH_SHORT).show();
            }
        });
        cardenquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), EnquiryActivity.class);
                startActivity(intent);
            }
        });
        cardreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Coming soon",Toast.LENGTH_SHORT).show();
            }
        });

       /* final TextView textView = binding.textHome;
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/


        return root;
    }

@Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}