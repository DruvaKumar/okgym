package com.druva.project2021.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.druva.project2021.Dashbaord.BlockedmembersActivity;
import com.druva.project2021.Dashbaord.ExpiredMembersActivity;
import com.druva.project2021.Dashbaord.ExpireinfifteendaysActivity;
import com.druva.project2021.Dashbaord.ExpireinfivedayActivity;
import com.druva.project2021.Dashbaord.ExpireintendaysActivity;
import com.druva.project2021.Dashbaord.ExpiretodayActivity;
import com.druva.project2021.Dashbaord.TodaysBirthdayActivity;
import com.druva.project2021.Dashbaord.LiveMembersActivity;
import com.druva.project2021.Dashbaord.TotalCollectionActivity;
import com.druva.project2021.Dashbaord.UnpaidMembersActivity;
import com.druva.project2021.Helpers.Config;
import com.druva.project2021.Helpers.PreferenceManager;
import com.druva.project2021.databinding.FragmentDashboardBinding;
import com.druva.project2021.pojo.DashbaordDAO;
import com.druva.project2021.reftrofit.RetrofitInstance;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
private FragmentDashboardBinding binding;
CardView cardcollection,cardlivememb,cardexpiremem,cardexpiretoday,cardexpire1_5,card6_10,card11_15,
        cardunpaid,cardbloked,cardbirthday;

TextView tvcollection,tvlivemmebrs,tvexpired,tvexpiretoday,tvexpire1_5,tvexpired6_10,tvexpire11_15,
    tvunpaid,tvblocked,tvbirthday;

PreferenceManager preferenceManager;
    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);

        preferenceManager=new PreferenceManager(getActivity());

    binding = FragmentDashboardBinding.inflate(inflater, container, false);
    View root = binding.getRoot();

       /* final TextView textView = binding.textDashboard;
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        cardcollection=binding.cardcollection;
        cardlivememb=binding.cardlive;
        cardexpiremem=binding.cardexpire;
        cardexpiretoday=binding.cardexpiretoday;
        cardexpire1_5=binding.cardexpire15;
        card6_10=binding.cardexpire610;
        card11_15=binding.cardexpire1115;
        cardunpaid=binding.cardunpaid;
        cardbloked=binding.cardblocked;
        cardbirthday=binding.carddob;

        tvcollection=binding.tvcollection;
        tvlivemmebrs=binding.tvlivemember;
        tvexpired=binding.tvexpired;
        tvexpiretoday=binding.tvexpiretoday;
        tvexpire1_5=binding.tvexpire15;
        tvexpired6_10=binding.tvexpire610;
        tvexpire11_15=binding.tvexpire1115;
        tvunpaid=binding.tvunpaid;
        tvblocked=binding.tvblocked;
        tvbirthday=binding.tvbirthday;

        getDashbardData();

        cardcollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TotalCollectionActivity.class));
            }
        });
        cardlivememb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LiveMembersActivity.class));
            }
        });


        cardexpiremem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ExpiredMembersActivity.class));
            }
        });
        cardexpiretoday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ExpiretodayActivity.class));
            }
        });
        cardexpire1_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ExpireinfivedayActivity.class));
            }
        });


        card6_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ExpireintendaysActivity.class));
            }
        });

        card11_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ExpireinfifteendaysActivity.class));
            }
        });

        cardunpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), UnpaidMembersActivity.class));
            }
        });

        cardbloked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), BlockedmembersActivity.class));
            }
        });
        cardbirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TodaysBirthdayActivity.class));
            }
        });

        return root;
    }

    private void getDashbardData() {

        HashMap<String, Object> listDetails = new HashMap<>();
        listDetails.put(Config.USERID, preferenceManager.getUserid(Config.USERID));

        Call<DashbaordDAO> call = RetrofitInstance.getInstance().getMyApi().get_dashboard_data(listDetails);
        call.enqueue(new Callback<DashbaordDAO>() {
            @Override
            public void onResponse(Call<DashbaordDAO> call, Response<DashbaordDAO> response) {

              //  Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
               DashbaordDAO dashbaordDAO=response.body();

               if(dashbaordDAO.getCollection()==null){
                   tvcollection.setText("\u20B9" + "0" );
               }
               else {
                   tvcollection.setText("\u20B9" + "" + dashbaordDAO.getCollection());
               }
                tvlivemmebrs.setText(dashbaordDAO.getLive());
                tvexpired.setText(dashbaordDAO.getExpired());
                tvexpiretoday.setText(dashbaordDAO.getLastday());
                tvexpire1_5.setText(dashbaordDAO.getExpire_in_5());
                tvexpired6_10.setText(dashbaordDAO.getExpire_in_10());
                tvexpire11_15.setText(dashbaordDAO.getExpire_in_15());
                tvunpaid.setText(dashbaordDAO.getUnpaid());
                tvblocked.setText(dashbaordDAO.getBlocked());
                tvbirthday.setText(dashbaordDAO.getBirthday());

            }

            @Override
            public void onFailure(Call<DashbaordDAO> call, Throwable t) {
                Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}