package com.druva.project2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.druva.project2021.Helpers.PublicHelper;

public class WebviewActivity extends AppCompatActivity {
    WebView webView;
    TextView header;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_webview);

        PublicHelper.showProgressDialog(WebviewActivity.this,"");
        webView=findViewById(R.id.webview);
        header=findViewById(R.id.header);
        back=findViewById(R.id.btn_back);

        Intent i=getIntent();
        String haeding=i.getStringExtra("heading");
        String url=i.getStringExtra("url");

        header.setText(haeding);
        Load_url(url);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
    private void Load_url(String url) {
     try {
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new MyWebChromeClient(url));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setAllowContentAccess(false);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setVerticalScrollBarEnabled(false);

        webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        webView.setBackgroundColor(Color.TRANSPARENT);

        webView.loadUrl(url);
        PublicHelper.dismissProgressDialog();
    } catch (Exception e) {
        Log.w("TAG", "setUpNavigationView", e);
    }
}

private class MyWebChromeClient extends WebChromeClient {
    private String urlname;

    public MyWebChromeClient(String urlname) {
        this.urlname = urlname;
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        try {

        } catch (Exception e) {
            Log.w("onProgressChanged", e);
        }
    }

}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}